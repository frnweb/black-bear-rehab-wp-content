<form action="https://foundationsrecoverynetwork.us5.list-manage.com/subscribe/post?u=a517fa2b2970f6fb110a5a1dc&amp;id=3675106f6e" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate sl_form--email" target="_blank" novalidate>
	<div class="sl_form__multiselect">
		<div id="sl_form__multiselect__field" class="sl_form__multiselect__field" tabindex="0">
			<span id="sl_who-input">I am a...</span>
		</div>
		<div id="sl_form__multiselect__options" class="mc-field-group input-group">
	    	<label for="mce-group[22101]-22101-0"><input type="checkbox" value="1" name="group[22101][1]" id="mce-group[22101]-22101-0">I am seeking information for myself</label>
			<label for="mce-group[22101]-22101-1"><input type="checkbox" value="2" name="group[22101][2]" id="mce-group[22101]-22101-1">I am seeking information for a loved one</label>
			<label for="mce-group[22101]-22101-2"><input type="checkbox" value="4" name="group[22101][4]" id="mce-group[22101]-22101-2">I am an alumni or in recovery</label>
			<label for="mce-group[22101]-22101-3"><input type="checkbox" value="8" name="group[22101][8]" id="mce-group[22101]-22101-3">I am an industry professional</label>
		</div>
	</div>
	<input onFocus="hideCheckboxes()" type="email" value="" placeholder="Email" name="EMAIL" class="required email sl_form__input" id="mce-EMAIL">

	<!-- Passing Source URL -->
	<input style="display: none;" type="text" value="<?php echo get_permalink() ?>" name="URL" class="" id="mce-URL">

	<!-- Passing Facility Group -->
	<div style="display: none;" class="mc-field-group input-group">
		<input type="checkbox" value="256" name="group[22145][256]" id="mce-group[22145]-22145-0" checked><label for="mce-group[22145]-22145-0">Black Bear Lodge</label>
		<input type="checkbox" value="512" name="group[22145][512]" id="mce-group[22145]-22145-1"><label for="mce-group[22145]-22145-1">Talbott Recovery</label>
		<input type="checkbox" value="1024" name="group[22145][1024]" id="mce-group[22145]-22145-2"><label for="mce-group[22145]-22145-2">Skywood Recovery</label>
		<input type="checkbox" value="2048" name="group[22145][2048]" id="mce-group[22145]-22145-3"><label for="mce-group[22145]-22145-3">The Oaks at La Paloma</label>
		<input type="checkbox" value="4096" name="group[22145][4096]" id="mce-group[22145]-22145-4"><label for="mce-group[22145]-22145-4">Michael's House</label>
	</div>

	<!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
	<div style="position: absolute; left: -5000px;" aria-hidden="true">
		<input type="text" name="b_a517fa2b2970f6fb110a5a1dc_3675106f6e" tabindex="-1" value="">
	</div>
	<input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="sl_button">
</form>
<div id="mce-responses">
	<div class="response" id="mce-error-response" style="display:none"></div>
	<div class="response" id="mce-success-response" style="display:none"></div>
</div>    
<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[3]='ADDRESS';ftypes[3]='address';fnames[15]='MMERGE15';ftypes[15]='zip';fnames[4]='PHONE';ftypes[4]='phone';fnames[5]='BIRTHDAY';ftypes[5]='birthday';fnames[6]='ACCT_OWNER';ftypes[6]='text';fnames[7]='MMERGE7';ftypes[7]='text';fnames[9]='MMERGE9';ftypes[9]='text';fnames[10]='MMERGE10';ftypes[10]='text';fnames[11]='MMERGE11';ftypes[11]='text';fnames[12]='MMERGE12';ftypes[12]='text';fnames[14]='MCMPGN';ftypes[14]='text';fnames[8]='URL';ftypes[8]='text';fnames[13]='MMERGE13';ftypes[13]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
<!--End mc_embed_signup-->
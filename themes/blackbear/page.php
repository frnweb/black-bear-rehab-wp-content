<?php get_header(); ?>
<header id="page-id">
<div class="tier-content-block">
	<div class="text-block icon-pinecone-lrg-lt">
		<h1><?php the_title(); ?></h1>
		<?php get_template_part('library/includes/breadcrumbs'); ?>
	</div><!-- end text-block -->
	<?php if(get_post_meta($post->ID,'ranklab_page_summary', true)) { ?>
		<!-- <div class="text-block page-message">
			//<h2><?php echo get_post_meta($post->ID,'ranklab_page_summary', true); ?></h2>
		</div> --><!-- end text-block -->
	<?php } ?>
</div><!-- end tier-content-block-->
</header>
<div class="main clearfix inner-page">
<div class="tier-content-block">
<section role="main" class="left-content">
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>	
<article>
	<?php
	if ( has_post_thumbnail() ) {
		the_post_thumbnail( 'large' );
	}
	?>
	<?php the_content(); ?>
</article>
<?php endwhile; endif; ?>
</section>
<?php get_sidebar(); ?>
</div><!-- end tier-content-block-->
</div> <!-- #main -->
<?php get_footer(); ?>
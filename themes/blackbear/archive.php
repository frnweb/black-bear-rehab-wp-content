<?php get_header(); ?>
<header id="page-id">
<div class="tier-content-block">
	<div class="text-block icon-pinecone-lrg-lt">
		<?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
		<?php /* If this is a category archive */ if (is_category()) { ?>
			<h1>Archive for the &#8216;<?php single_cat_title(); ?>&#8217; Category</h1>
		<?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>
			<h1>Posts Tagged &#8216;<?php single_tag_title(); ?>&#8217;</h1>
		<?php /* If this is a daily archive */ } elseif (is_day()) { ?>
			<h1>Archive for <?php the_time('F jS, Y'); ?></h1>
		<?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
			<h1>Archive for <?php the_time('F, Y'); ?></h1>
		<?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
			<h1 class="pagetitle">Archive for <?php the_time('Y'); ?></h1>
		<?php /* If this is an author archive */ } elseif (is_author()) { ?>
			<h1 class="pagetitle">Author Archive</h1>
		<?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
			<h1 class="pagetitle">Blog Archives</h1>
		<?php } ?>
		<?php get_template_part('library/includes/breadcrumbs'); ?>
	</div><!-- end text-block -->
	<?php if(get_post_meta($post->ID,'ranklab_page_summary', true)) { ?>
		<!-- <div class="text-block page-message">
			<h2><?php echo get_post_meta($post->ID,'ranklab_page_summary', true); ?></h2>
		</div> --><!-- end text-block -->
	<?php } ?>
</div><!-- end tier-content-block-->
</header>
<div class="main clearfix inner-page">
<div class="tier-content-block">
<section role="main" class="left-content">

<?php if (have_posts()) :  while  (have_posts()) : the_post(); ?>
	<article class="blog">
		<div class="side-block">
			<div class="postmeta icon-pc">
				<!--<span><strong><?php //the_author(); ?></strong></span>-->
				<span><?php the_time('F jS Y') ?></span>
				<span>Posted In: <?php the_category(', ') ?></span>
				<span><?php //comments_popup_link('No Comments &#187;', '1 Comment &#187;', '% Comments &#187;'); ?></span>
			</div><!-- end meta -->
			<!-- <button class="btn btn-brown icon-pinecone-sm-lt" onclick="location.href='<?php the_permalink();?>'"  >Read</button> -->
		</div><!-- end side-block -->
		<div class="text-block">
			<h2><a href="<?php the_permalink();?>"><?php the_title();?></a></h2>
			<?php the_excerpt();?>
		</div><!-- end text-block -->
	</article><!-- end blog -->	
<?php endwhile; endif; ?>
<?php ranklab_pagination();?>

</section>
<?php get_sidebar(); ?>
</div><!-- end tier-content-block-->
</div> <!-- #main -->
<?php get_footer(); ?>
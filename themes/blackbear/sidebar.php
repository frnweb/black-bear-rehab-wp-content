<aside role="complementary">
<?php echo do_shortcode('[frn_social]'); ?> 
<?php 
if(has_sidebar('default')) { 
	if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Default Sidebar')) : endif;
} elseif (has_sidebar('about')) {	
	if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('About Sidebar')) : endif;
} elseif (has_sidebar('programs')) {	
	if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Programs Sidebar')) : endif;
} elseif (has_sidebar('location')) {	
	if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Location Sidebar')) : endif;
} elseif (has_sidebar('mental-health')) {	
	if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Mental Health Sidebar')) : endif;
} elseif (has_sidebar('admissions')) {	
	if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Admissions Sidebar')) : endif;
} elseif (has_sidebar('treatment')) {	
	if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Treatment Sidebar')) : endif;
} elseif (has_sidebar('resources')) {	
	if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Resources Sidebar')) : endif;
} elseif (has_sidebar('substance-abuse')) {	
	if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Substance Abuse Sidebar')) : endif;
} elseif (has_sidebar('contact')) {	
	if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Contact Sidebar')) : endif;
} elseif (is_home() || is_single() || is_archive()) {	
	if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Blog Sidebar')) : endif;
} else {
	if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Default Sidebar')) : endif;
}
?>

</aside>
<?php
//Maintenance Mode
function activate_maintenance_mode() {
   if ( !(current_user_can( 'administrator' ) ||  current_user_can(
'super admin' ) ||  current_user_can( 'editor' ))) {
	include(TEMPLATEPATH.'/maintenance.php');
   }
}
$value = stripslashes(get_option('ranklab_mm_active'));

if($value == 'Yes') {
	add_action('get_header', 'activate_maintenance_mode');
} else {
	
}


//Sidebar Taxonomy
function has_sidebar( $sidebar, $_post = null ) {
	if ( empty( $sidebar ) )
		return false;

	if ( $_post )
		$_post = get_post( $_post );
	else
		$_post =& $GLOBALS['post'];

	if ( !$_post )
		return false;

	$r = is_object_in_term( $_post->ID, 'sidebar', $sidebar );

	if ( is_wp_error( $r ) )
		return false;

	return $r;
}



//Remove Defualt Gallery Style
add_filter( 'use_default_gallery_style', '__return_false' );

//Define Gallery Width
if ( ! isset( $content_width ) )
    $content_width = 1000;

?>
<?php

// Shortcodes

	// ROW
		function frn_foundation_row ( $atts, $content = null ) {
			return '<div class="sl_row row">' . do_shortcode ( $content ) . '</div>';
		}
		add_shortcode ('row', 'frn_foundation_row' );
	///ROW

	// COLUMN
		function frn_foundation_col ( $atts, $content = null ) {
			$specs = shortcode_atts( array(
				'size'		=> '',
				), $atts );
			return '<div class="' . esc_attr($specs['size']) . ' sl_columns columns">' . do_shortcode ( $content ) . '</div>';
		}
		add_shortcode ('col', 'frn_foundation_col' );
	///COLUMN

	// BLOCKGRID LIST ITEM
		function frn_foundation_blockgridli ( $atts, $content = null ) {
			return '<div class="sl_column column column-block">' .  do_shortcode ( $content ) . '</div>';
		}

		add_shortcode ('item', 'frn_foundation_blockgridli' );
	///BLOCKGRID LIST ITEM

	// BUTTON
		function frn_foundation_buttons ( $atts, $content = null ) {
			$specs = shortcode_atts( array(
				'url'	=> '#',
				'style'	=> '', 
				'target'	=> '_self'
				), $atts );
			if(in_array('read-next', $specs, true)){
				return '<div class="sl_read-next--button"><h4>Read This Next:</h4><a href="' . esc_attr($specs['url'] ) . '" class="sl_button button secondary ' . esc_attr($specs['style'] ) . '" target="'. esc_attr($specs['target'] ) .'">' . $content . '</a></div>';
			} else {
			return '<a href="' . esc_attr($specs['url'] ) . '" class="sl_read-next--link sl_button button ' . esc_attr($specs['style'] ) . '" target="'. esc_attr($specs['target'] ) .'">' . $content . '</a>';
			};
		}

		add_shortcode ('button', 'frn_foundation_buttons' );
	///BUTTON

	//READ NEXT
	function frn_read_next ( $atts, $content = null ) {
		$specs = shortcode_atts( array(
			'url'	=> '#',
			'title' => '',
			'style'	=> '', 
			'target'	=> '_self'
			), $atts );

			return '<div class="sl_read-next ' . esc_attr($specs['style'] ) . ' "><span>&gt;&gt;&gt; Read This Next:</span> <a class="sl_read-next--link" title="' . esc_attr($specs['title'] ) . '" href="' . esc_attr($specs['url'] ) . '" target="'. esc_attr($specs['target'] ) .'">' . $content . '</a></div>';
	}

	add_shortcode ('read-next', 'frn_read_next' );

	// FLEXVIDEO
		function frn_foundation_flexvideo ( $atts, $content = null ) {
			return '<div class="flex-video">' . $content . '</div>';
		}
		add_shortcode ('flexvideo', 'frn_foundation_flexvideo' );
	///FLEXVIDEO

	// TABS
		function frn_foundation_tabs( $atts, $content = null ) {
			$specs = shortcode_atts( array(
				'style'		=> ''
			), $atts );
			if(in_array('alternate', $specs, true)){
				return '<div class="sl_content-tabs content-tabs alternate' . esc_attr($specs['style'] ) . '">' . do_shortcode ( $content ) . '</div>';
			} else {
			return '<div class="sl_content-tabs content-tabs primary">' . do_shortcode ( $content ) . '</div>';
		};
		}

		add_shortcode ('tabs', 'frn_foundation_tabs' );
	///TABS

	// TAB MENU
		function frn_foundation_tabs_menu( $atts, $content = null ) {
			$specs = shortcode_atts( array(
				'style'		=> ''
			), $atts );
			static $i = 0;
			$i++;
			if(in_array('alternate', $specs, true)){
				return '<div class="menu-wrapper"><ul class="tab-menu alternate' . esc_attr($specs['style'] ) . '">' . do_shortcode ( $content ) . '</ul></div>';
			} else {
			return '<div class="menu-wrapper"><ul class="tab-menu primary">' . do_shortcode ( $content ) . '</ul></div>';
		};
		}

		add_shortcode ('tab-menu', 'frn_foundation_tabs_menu' );
	///TAB MENU

	// TAB TITLE
		function frn_foundation_tabs_title ( $atts, $content = null ) {

		    $specs = shortcode_atts( array(
		        'class'     => ''
		    ), $atts );

		    static $i = 0;
		    if ( $i == 0 ) {
		        $class = 'class="current"';
		    } else {
		        $class = NULL;
		    }
		    $i++;
		    
		    return '<li class="tab' . $i . '"><a href="#"' . $class . ' '.esc_attr($specs['class'] ). '">' .
		    do_shortcode ( $content ) . '</a></li>';
		}

		add_shortcode ('tab-title', 'frn_foundation_tabs_title' );
	///TAB TITLE

	// TAB CONTENT
		function frn_foundation_tabs_content( $atts, $content = null ) {
			static $i = 0;
			$i++;
			return '<div class="tab-content-wrapper"' . $i . '">' . do_shortcode ( $content ) . '</div>';
		}
		add_shortcode ('tab-content', 'frn_foundation_tabs_content' );
	///TAB CONTENT

	// TAB PANEL
		function frn_foundation_tabs_panel ($atts, $content = null ) {
			static $i = 0;
			if ( $i == 0 ) {
				$style = NULL;
			} else {
				$style = 'style="display:none;"';
			}
			$i++;
			return '<div class="dynamic tab' . $i . '" ' . $style . '>' . do_shortcode( $content ) . '</div>';
		}
		add_shortcode ('tab-panel', 'frn_foundation_tabs_panel' );
	///TAB PANEL

	// ACCORDION
		function frn_foundation_accordion( $atts, $content = null ) {
			$specs = shortcode_atts( array(
				'style'		=> ''
			), $atts );
			return '<div class="sl_accordion accordion">' . do_shortcode ( $content ) . '</div>';
		}
		add_shortcode ('accordion', 'frn_foundation_accordion' );
	///ACCORDION

	// ACCORDION ITEM
		function frn_foundation_tabs_accordion_item ( $atts, $content = null ) {
			$specs = shortcode_atts( array(
				'title'		=> ''
				), $atts );
			static $i = 0;
		/*	if ( $i == 0 ) {
				$class = 'active';
			} else {
				$class = NULL;
			}*/
			$i++;
			$value = '<h3>' . esc_attr($specs['title'] ) . '<span class="plus">+</span></h3><div>' . do_shortcode( $content ) . '</div>';

			return $value;
		}

		add_shortcode ('accordion-item', 'frn_foundation_tabs_accordion_item' );
	///ACCORDION ITEM


///THESE ARE SOME SHORTCODES THAT ARE INDEPENDENT OF THE FOUNDATION FRAMEWORK

	// DIVIDER
		function line_divider() {

			return '<div class="sl_divider divider clearfix"></div>';

		}
		add_shortcode( 'divider', 'line_divider' );
	///DIVIDER

	// CALLOUT
		function callout_box ( $atts, $content = null ) {
			$specs = shortcode_atts( array(
				'style'		=> ''
				), $atts );
			return '<div class="sl_callout callout ' . esc_attr($specs['style'] ) . '">' . do_shortcode ( $content ) . '</div>';
		}
		add_shortcode ('callout', 'callout_box' );
	///CALLOUT

	// CALLOUT CARD
		function callout_card ( $atts, $content = null ) {
			$specs = shortcode_atts( array(
				'style'		=> '',
				'img'		=> ''
				), $atts );
			return '<div class="sl_callout-card callout-card ' . esc_attr($specs['style'] ) . '"><div class="card-media" style="background-image: url('. esc_attr($specs['img'] ) .'"></div><div class="card-content">' . do_shortcode ( $content ) . '</div></div>';
		}
		add_shortcode ('callout-card', 'callout_card' );
	///CALLOUT CARD

	// LARGE TEXT
		function large_text ( $atts, $content = null ) {
			return '<div class="sl_callout-text callout-text">' . do_shortcode ( $content ) . '</div>';
		}
		add_shortcode ('large_text', 'large_text' );
	///LARGE TEXT


	// MAP
		function map_container ( $atts, $content = null ) {
			return '<div class="map-container">' . $content . '</div>';
		}
		add_shortcode ('map', 'map_container' );
	///MAP

	// CONTACT BOX
		function contact_box_section ( $atts, $content = null ) {
			return '<div class="sl_callout callout equalizer contact-box" data-equalizer-watch>' . do_shortcode ( $content ) . '</div>';
		}
		add_shortcode ('contact_box', 'contact_box_section' );
	///CONTACT BOX

	// CONTAINER
		function div_container ( $atts, $content = null ) {
		    $specs = shortcode_atts( array(
		        'class'     => '',
		        ), $atts );
		    return '<div class="sl_container container ' . esc_attr($specs['class']) . ' ">' .  do_shortcode ( $content ) . '</div>';
		}

		add_shortcode ('container', 'div_container' );
	///CONTAINER

	//EMAIL US
		function email_us ($atts, $content = null) {
			$specs = shortcode_atts( array(
				'text'		=> 'Subscribe to our email list!',
				'domain'	=> 'Black Bear Post'
				), $atts );
			return '<div class="module email"><div class="email__card"><h3>' . esc_attr($specs['text']) . '</h3><form class="form form--email" target="_blank" action="http://cloud.frn-mail.com/subscribe" method="GET"><input placeholder="Email Address" id="input" class="form__input" type="text" name="email"><input name="domain" type="hidden" value="' . esc_attr($specs['domain']) . '"><button type="submit" value="Sign Me Up" class="button secondary">Sign Me Up</button></form></div></div>';
		}
		add_shortcode ('email', 'email_us' );
	//EMAIL US

	//YOUTUBE EMBED IFRAME
	function bbl_embed( $atts, $content = null ) {
		$specs = shortcode_atts( array(
			'src'   => ''
        ), $atts );
        return '<iframe width="560" height="315" src="" data-src=' . esc_attr($specs['src'] ) . ' frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
	}
	add_shortcode ('embed', 'bbl_embed' );
	//EMBED

	// EMAIL
	function sl_email ($atts) {
	    $specs = shortcode_atts( array(
	        'heading'     => 'Subscribe to our email list',
			'campaign'     => '',
			'not_content' => '',
			), $atts );

	    // $content = ob_get_clean();

	    static $i = 0;
		$i++;

		ob_start ();
		?><div class="sl_card sl_card--email">
			<!-- Begin Constant Contact Inline Form Code -->
			<div class="ctct-inline-form" data-form-id="ff259981-2b5c-42bf-9209-e8790f7742d9"></div>
			<!-- End Constant Contact Inline Form Code -->
	    </div><?php

		$content = ob_get_clean();
	
		return do_shortcode($content);

	}
	add_shortcode( 'email', 'sl_email' );
	//END EMAIL


//disables wp texturize on registered shortcodes
function frn_shortcode_exclude( $shortcodes ) {
    $shortcodes[] = 'row';
    $shortcodes[] = 'column';
    $shortcodes[] = 'blockgrid';
    $shortcodes[] = 'item';
    $shortcodes[] = 'button';
    $shortcodes[] = 'flexvideo';
    $shortcodes[] = 'tabs';
    $shortcodes[] = 'tab-title';
    $shortcodes[] = 'tab-menu';
    $shortcodes[] = 'tab-content';
    $shortcodes[] = 'tab-panel';
    $shortcodes[] = 'accordion';
    $shortcodes[] = 'accordion-item';
    $shortcodes[] = 'email';
    $shortcodes[] = 'modal';
	$shortcodes[] = 'divider';
	$shortcodes[] = 'map';
	$shortcodes[] = 'contact_box';
	$shortcodes[] = 'facility_photo_slider';
	$shortcodes[] = 'frn_privacy_url';
    $shortcodes[] = 'frn_footer';
    $shortcodes[] = 'lhn_inpage';
    $shortcodes[] = 'frn_phone';
    $shortcodes[] = 'frn_social';
    $shortcodes[] = 'frn_boxes';

    return $shortcodes;
}

add_filter ('no_texturize_shortcodes', 'frn_shortcode_exclude' );

// This function reformats autop inside shortcodes. To turn off auto p inside shortcode, wrap shortcode in [shortcode_unautop]

// Example: return '[shortcode_unautop]'. do_shortcode($content) .'[/shortcode_unautop]';

function reformat_auto_p_tags($content) {
    $new_content = '';
    $pattern_full = '{(\[shortcode_unautop\].*?\[/shortcode_unautop\])}is';
    $pattern_contents = '{\[shortcode_unautop\](.*?)\[/shortcode_unautop\]}is';
    $pieces = preg_split($pattern_full, $content, -1, PREG_SPLIT_DELIM_CAPTURE);
    foreach ($pieces as $piece) {
        if (preg_match($pattern_contents, $piece, $matches)) {
            $new_content .= $matches[1];
        } else {
            $new_content .= wptexturize(wpautop($piece));
        }
    }

    return $new_content;
}

remove_filter('the_content', 'wpautop');
remove_filter('the_content', 'wptexturize');

add_filter('the_content', 'reformat_auto_p_tags', 99);
add_filter('widget_text', 'reformat_auto_p_tags', 99);

?>

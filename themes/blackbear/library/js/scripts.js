jQuery(document).ready(function($) {
	//Alternating Rows
	$("#subpage .sub_item:even").addClass("alt");
	
	/*
	//Responsive Nav
	$("#nav").addClass("js").before('<div id="menu">&#9776;</div>');
	$("#menu").click(function(){
		$("#nav").toggle();
		});
		$(window).resize(function(){
		if(window.innerWidth > 768) {
		$("#nav").removeAttr("style");
		}
	});
	*/
	
	//Content Formatting
	//Accordion
	$('div.accordion> div').hide();  
	  $('div.accordion> h3').click(function() {
	  	$(this).toggleClass('active');
	    $(this).next('div').slideToggle('fast');
	});
	
	//Tabs
	$('.tab-menu.primary li a').click(function(event) {
	    event.preventDefault();
	    $('.content-tabs.primary .dynamic').hide();
	    $('.tab-menu.primary li a').removeClass('current');
	    var parent = $(event.target).parent();
	    $('.dynamic.' + parent.attr('class')).fadeIn();
	    parent.find('a').addClass('current');
	});

	$('.tab-menu.alternate li a').click(function(event) {
	    event.preventDefault();
	    $('.content-tabs.alternate .dynamic').hide();
	    $('.tab-menu.secondary li a').removeClass('current');
	    var parent = $(event.target).parent();
	    $('.dynamic.' + parent.attr('class')).fadeIn();
	    parent.find('a').addClass('current');
	});
	
	//Equal Columns
	var max_height = 0;
	$("div.third").each(function(){
	    if ($(this).height() > max_height) { max_height = $(this).height(); }
	});
	$("div.third").height(max_height);
	
	//Stellar
	//if (document.documentElement.clientWidth < 800) {
		$.stellar({
			horizontalScrolling: false,
			verticalOffset: 40
		});
	// }
	
	
// 	//Responsive Nav
	$('#responsive-menu-button').sidr({
	name: 'sidr-main',
	source: '#nav, #top-nav'
	});

	var vidDefer = document.getElementsByTagName('iframe');
	
	// Remove empty P tags created by WP inside of Accordion and Orbit
	$('.accordion p:empty, .orbit p:empty').remove();

	$("p").find("iframe").unwrap();

	for (var i=0; i<vidDefer.length; i++) {
		if(vidDefer[i].getAttribute('data-src')) {
		vidDefer[i].setAttribute('src',vidDefer[i].getAttribute('data-src'));
		} 
	}

	if(!$('iframe[src*="youtube.com"], iframe[src*="vimeo.com"]').parents("div").hasClass("sl_reveal")) {
		$('iframe[src*="youtube.com"], iframe[src*="vimeo.com"]').each(function() {
			if ( $(this).innerWidth() / $(this).innerHeight() > 1.5 ) {
				$(this).wrap("<div class='widescreen responsive-embed'/>");
			} else {
				$(this).wrap("<div class='responsive-embed'/>");
			}
		})
	}
});

//Mailchimp Email Form scripts. The markup can be found in /parts/contetn-emailform.php
jQuery(document).ready(function($) {
    var expanded = false;

    //Checks if dropdown is open and displays or hides accordingly
    function showCheckboxes(emailNumber) {
      var checkboxes = document.getElementById("sl_form__multiselect__options--" + emailNumber);

      console.log("show", checkboxes);
      if (!expanded) {
        checkboxes.style.display = "block";
        expanded = true;
      } else {
        checkboxes.style.display = "none";
        expanded = false;
      }
    };

    //Just hides the dropdown if it's open
    function hideCheckboxes(emailNumber) {
      var checkboxes = document.getElementById("sl_form__multiselect__options--" + emailNumber);

        checkboxes.style.display = "none";
        expanded = false;
    };

    $(".sl_form__multiselect__field").focus(function(e) {
      var emailNumber = e.target.id.split("--").pop().toString();
      var optionDropdown = document.getElementById("sl_form__multiselect__field--" + emailNumber);
      var emailField = document.getElementById("mce-EMAIL--" + emailNumber);

      showCheckboxes(emailNumber);

      if (document.activeElement === optionDropdown){
        $(optionDropdown).blur();
      }

      $(emailField).on('focus', function() {
       hideCheckboxes(emailNumber);
      })

      var selectOptions = '#sl_form__multiselect__options--' + emailNumber + ' input:checkbox'

      console.log("logs", selectOptions);

    $(selectOptions).on("change", function(){
      var whoOptions = [];
      $(selectOptions).each(function(){
       if($(this).is(":checked")){
         whoOptions.push(this.parentNode.textContent)
         document.getElementById("sl_who-input--" + emailNumber).innerHTML = whoOptions.concat();
         document.getElementById("sl_who-input--" + emailNumber).style.color = "#152435";
       }

       //If more than one option is selected
       if(whoOptions.length > 1){
        document.getElementById("sl_who-input--" + emailNumber).innerHTML = "Multiple Selected";
        document.getElementById("sl_who-input--" + emailNumber).style.color = "#152435";
       }

       //If less than one option is selected, reformat to initial state
       if(whoOptions.length < 1 || whoOptions == undefined){
        document.getElementById("sl_who-input--" + emailNumber).innerHTML = "I am a...";
        document.getElementById("sl_who-input--" + emailNumber).style.color = "inherit";
       }
      })   
    });
    });
});
//END MAILCHIMP JS


jQuery(document).ready(function($) {
if (jQuery.cookie('modal_shown') == null) {
	jQuery.cookie('modal_shown', 'yes', { expires: 7, path: '/' });
	setTimeout(function(){
		jQuery('#mobile_overlay').delay(5000).addClass('visible');
		jQuery('#close_btn').click(function(){
		jQuery('#mobile_overlay').removeClass('visible');
			  });
}, 4000);
}

//add scroll logic for phone banner
window.onscroll = function() {myFunction()};

var phoneNav = document.getElementById("phone-bg");
var sticky = phoneNav.offsetTop;

function myFunction() {
if (window.pageYOffset > sticky) {
phoneNav.style.position = "fixed";
} else {
phoneNav.style.position = "absolute";
}
}

    //add close for COVID banner without Foundation support
    var closebtn = document.getElementById('close-btn')
    var banner = document.getElementById('sl_notification-bar');

	closebtn.onclick = function(event) {
        banner.style.display = "none";
	}
});
<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
<meta charset="<?php bloginfo('charset'); ?>">
<title><?php wp_title(''); ?></title>

<!-- dns prefetch -->
<link href="//www.google-analytics.com" rel="dns-prefetch">

<!-- meta -->
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1.0">


<!-- icons -->
<link href="<?php echo get_template_directory_uri(); ?>/style/images/icons/favicon.ico" rel="shortcut icon">
<link href="<?php echo get_template_directory_uri(); ?>/style/images/icons/touch.png" rel="apple-touch-icon-precomposed">
	
<!-- css + javascript -->
<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/style/css/normalize.css" />
<!--
<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/style/css/lightbox.css" /> -->
<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/style/css/jquery.sidr.dark.css" />
<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/style/css/formatting.css" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>?v=2.1" />
<script src="<?php echo get_template_directory_uri(); ?>/library/js/modernizr.min.js"></script>

<?php wp_head(); ?>
<script>
	//disables scroll zoom on google map 
	jQuery("body").ready(function($) {
		$('.map-container')
			.click(function(){
				$(this).find('iframe').addClass('clicked')})
		.mouseleave(function(){
				$(this).find('iframe').removeClass('clicked')});
	});
</script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PV84BHT');</script>
<!-- End Google Tag Manager -->

<!-- Hotjar Tracking Code for https://blackbearrehab.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:172474,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>
<!-- End Hotjar Tracking Code -->

<!-- Cloudflare Verify -->
<meta name="cf-2fa-verify" content="cfaf6a579407433">
<!-- End Cloudflare Verify -->

</head>
<body <?php body_class(); ?>>
<!-- Begin Constant Contact Active Forms -->
<script> var _ctct_m = "4cea1f45733694e0b64412df8237c051"; </script>
<script id="signupScript" src="//static.ctctcdn.com/js/signup-form-widget/current/signup-form-widget.min.js" async defer></script>
<!-- End Constant Contact Active Forms -->
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PV84BHT"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!-- header -->
<div class="header-container">

<div data-closable class="sl_notification-bar" id="sl_notification-bar" style="background-color:#71c2b3;color:#ffffff; padding: 8px 24px;">
	<style type="text/css">
		.sl_notification-bar .sl_inner { width: 100%; max-width: 1080px; margin: 0 auto; font-size: 18px; text-align: center;font-weight:400;}
		.sl_notification-bar .sl_button { display: inline-block; padding: 6px 10px; font-size: 15px; border-radius: 3px; color: #fff!important; text-shadow: 0 1px 1px rgba(0, 0, 0, 0.2); background-color:#8c6239; letter-spacing: 0; font-weight: 400; margin-left: 8px;}
		.sl_notification-bar .close-button { position: absolute; color: #8a8a8a; right: 1rem; top: 0.5rem; font-size: 2em; line-height: 1; cursor: pointer; }
	</style>
	<div class="sl_inner">
		<span>Committed to Safety: Latest information on COVID-19 Precautions</span><a href="/important-covid-19-update-from-black-bear-lodge/" class="sl_button">Learn More</a>
	</div>
	<div id="close-btn" class="close-button" aria-label="Close alert" style="color: #fff;" type="button" data-close>
		<span aria-hidden="true">&times;</span>
	</div>
</div>

<header class="clearfix">
	<div id="mobile-header">
		<a id="responsive-menu-button" href="#sidr-main" onClick="ga('send', 'event', 'Hamburger Menus', 'Menu Opens & Closes'); ">Menu</a>
	</div>
	<div id="top-right-nav">
		<nav id="top-nav">
			<ul>
				<li class="top-nav-item"><a href="/resources/">Resources</a></li>
				<li class="top-nav-item"><a href="/inspiration/">Inspiration</a></li>
				<li class="top-nav-item"><a href="/blog/">Blog</a></li>
			</ul>
		</nav>
	</div><!-- end top-nav -->
	<div id="phone-bg" class="phone-bg"><span class="phone-icon-sm"><?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Header"]'); ?></span></div>
    <!-- logo -->
	<div class="logo">
		<a href="<?php echo home_url(); ?>" title="">
			<img src="<?php echo get_template_directory_uri(); ?>/style/images/BBL-logo-dark.png" alt="Logo" title="" class="logo">
		</a>
	</div>
	<!-- /logo -->
	
	<div id="nav">
		<nav id="main-nav" role="navigation"><?php wp_nav_menu( array( 'theme_location' => 'main-nav' ) ); ?></nav>
	</div><!-- end nav -->
</header>
</div><!-- end header-container -->
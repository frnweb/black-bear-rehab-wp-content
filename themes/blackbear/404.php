<?php get_header(); ?>
<header id="page-id">
<div class="tier-content-block">
	<div class="text-block icon-pinecone-lrg-lt">
		<h1>Sorry, we couldn't find it</h1>
		<?php get_template_part('library/includes/breadcrumbs'); ?>
	</div><!-- end text-block -->
	<?php if(get_post_meta($post->ID,'ranklab_page_summary', true)) { ?>
		<!-- <div class="text-block page-message">
			<h2><?php echo get_post_meta($post->ID,'ranklab_page_summary', true); ?></h2>
			<br />
		</div><!-- end text-block --> -->
	<?php } ?>
</div><!-- end tier-content-block-->
</header>
<div class="main clearfix inner-page">
<div class="tier-content-block">
<section role="main" class="left-content">
	
<article>
	<p><?php _e( 'It looks like something may be wrong with the web address you were using. We are available 24/7 if you have questions about '.get_bloginfo('name').', drug abuse, or what a person can do to overcome addiction. '.do_shortcode('[frn_phone]')); ?></p>
	<p><?php //echo stripslashes(get_option('ranklab_404')); ?></p>

<?php echo do_shortcode('[frn_related html="searchbox"]'); ?>
</article>

</section>
<?php get_sidebar(); ?>
</div><!-- end tier-content-block-->
</div> <!-- #main -->
<?php get_footer(); ?>
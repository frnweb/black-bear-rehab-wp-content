<?php
/*
	Template Name: Blog
*/
?>
<?php get_header(); ?>
<header id="page-id">
<div class="tier-content-block">
	<div class="text-block icon-pinecone-lrg-lt">
		<h1><?php the_title(); ?></h1>
		<?php get_template_part('library/includes/breadcrumbs'); ?>
	</div><!-- end text-block -->
	<?php if(get_post_meta($post->ID,'ranklab_page_summary', true)) { ?>
		<!-- <div class="text-block page-message">
			<h2><?php echo get_post_meta($post->ID,'ranklab_page_summary', true); ?></h2>
		</div> --><!-- end text-block -->
	<?php } ?>
</div><!-- end tier-content-block-->
</header>
<div class="main clearfix inner-page">
<div class="tier-content-block">
<section role="main" class="left-content">

<?php query_posts('post_type=post&posts_per_page=5&paged='.$paged);?>
<?php if (have_posts()) :  while  (have_posts()) : the_post(); ?>
	<article class="blog">
		<div class="side-block">
			<div class="postmeta icon-pc">
				<!--<span><strong><?php //the_author(); ?></strong></span>-->
				<span><?php the_time('F jS Y') ?></span>
				<span>Posted In: <?php the_category(', ') ?></span>
				<span><?php //comments_popup_link('No Comments &#187;', '1 Comment &#187;', '% Comments &#187;'); ?></span>
			</div><!-- end meta -->
			<!-- <button class="btn btn-brown icon-pinecone-sm-lt" onclick="location.href='<?php the_permalink();?>'"  >Read</button> -->
		</div><!-- end side-block -->
		<div class="text-block">
			<h2><a href="<?php the_permalink();?>"><?php the_title();?></a></h2>
			<?php the_excerpt();?>
		</div><!-- end text-block -->
	</article><!-- end blog -->	
<?php endwhile; ?>
<?php ranklab_pagination();?>
<?php endif; ?>
</section>
<?php get_sidebar(); ?>
</div><!-- end tier-content-block-->
</div> <!-- #main -->
<?php get_footer(); ?>
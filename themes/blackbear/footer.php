<!-- Mailchimp Email Form -->
    <div class="sl_footer__email">
        <div class="sl_inner">
            <?php echo do_shortcode('[email]'); ?>
        </div>
    </div>
<!-- End Mailchimp Email Form -->

<div class="footer-container">
    <footer class="clearfix">
    <div class="sl_inner">
    <div class="sl_row row">	
    	<div class="footer-side-left columns sl_columns">
    		<ul class="facility-info">
    			<li class="icon-sm-phone"><?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Footer (Discover Path)"]'); ?></li>
    			<li class="icon-sm-email"><a class="lhn_btn_email" onclick="frn_open_lhnwindow('email'); return false;" href="#">Email Us</a></li>
				<li class="icon-sm-users"><a href="/about/">About Us</a></li>
				<li class="icon-pinecone-lrg-lt"><a href="https://jobs.uhsinc.com/frn-residential-treatment-careers/jobs?page=1&brand=Black+Bear+Lodge">Careers</a></li>
    			
    		</ul>
		<ul class="facility-address">
			<li class="icon-sm-location"><a href="https://www.google.com/maps/place/Black+Bear+Lodge/@34.6583268,-83.7221458,16z/data=!4m2!3m1!1s0x885f485504f17c83:0x124b3e62316d2e35" target="_blank">310 Black Bear Ridge<br />Sautee Nacoochee, GA 30571<br /><span style="text-decoration:underline;">Map it</span></a></li>
		</ul>
    	</div><!-- end one-third columns sl_columns -->

        <div class="footer-center columns sl_columns">
            <!-- logo -->
            <div class="logo-footer">
                <a href="<?php echo home_url(); ?>">
                    <img src="<?php echo get_template_directory_uri(); ?>/style/images/BBL-logo-light.png" alt="Logo" class="logo-footer">
                </a>
            </div>
            <!-- /logo -->
            <h2>Discover a New Path</h2>
            <a href="/admissions/"><button class="btn btn-green icon-pinecone-sm-drk">Start Admissions</button></a><br clear="all"><br />
            Accredited by:<br />
            <div class="footer-accreditations">
            <a href="https://www.jointcommission.org/about_us/about_the_joint_commission_main.aspx"><img src="/wp-content/uploads/joint-commission-logo.png" align="center" style="vertical-align: top;" alt="The Joint Commission: Accreditation non-profit for health care organizations and programs"></a>
            <div style="display: inline-block"><script src="https://static.legitscript.com/seals/3417924.js"></script></div>
            <a class="naatp-logo" href="https://www.naatp.org/resources/addiction-industry-directory/1449/black-bear-lodge" target="_blank"><img src="https://www.naatp.org//civicrm/file?reset=1&id=4087&eid=201&fcs=1f26e81149c63e3ea4639b16e1ccf9e6a66df075bd944bb7ddb8b7024507c576_1585039261_87600"></a>

            </div>
            <div class="footer-bottom"><p>Black Bear Lodge is dedicated to providing the highest level of quality care to our patients. The Joint Commission’s gold seal of approval on our website shows that we have demonstrated compliance to the most stringent standards of performance, and we take pride in our accreditation. The Joint Commission standards deal with organization quality, safety-of-care issues and the safety of the environment in which care is provided. If you have concerns about your care, we would like to hear from you. Please contact us at <?php echo do_shortcode('[frn_phone number="678-251-3100"]'); ?>. If you do not feel that your concerns have been addressed adequately, you may contact The Joint Commission at: Division of Accreditation Operations, Office of Quality Monitoring, The Joint Commission, One Renaissance Boulevard, Oakbrook Terrace, IL 60181, Telephone: <?php echo do_shortcode('[frn_phone number="800-994-6610"]'); ?></p></div>

        </div><!-- end one-third columns sl_columns -->    
    	
		<div class="footer-side-right columns sl_columns">
    		<ul class="footer-social">
    			<a target="_blank" href="https://www.facebook.com/BlackBearRecovery"><li class="icon-sm-facebook">Facebook</li></a>
    			<a target="_blank" href="https://twitter.com/BlackBearRehab"><li class="icon-sm-twitter">Twitter</li></a>
				<a target="_blank" href="https://www.linkedin.com/company/black-bear-lodge-treatment-center/"><li class="icon-sm-linkedin">LinkedIn</li></a>
    		</ul>
    	</div><!-- end one-third columns sl_columns -->
    </div><!-- end sl_row row-->
    <?php wp_footer(); ?>
    </div><!-- end sl_inner -->
    </footer>
</div><!-- end footer-container -->
<div id="bottom-bar">
	<div id="bottom-bar-left">
		<div class="bottom-c">
		    <div class="message-c">
		    	<?php if(get_post_meta($post->ID,'ranklab_footer_rotator_large', true)) { ?>
		    		<h2><?php echo get_post_meta($post->ID,'ranklab_footer_rotator_large', true); ?></h2>
		    		<p><?php echo get_post_meta($post->ID,'ranklab_footer_rotator_sub', true); ?></p>		
		    	<?php } else {  ?>
		        	<h2><?php echo stripslashes(get_option('ranklab_rotating_large')); ?></h2>
					<p><?php echo stripslashes(get_option('ranklab_rotating_sub')); ?></p>
				<?php } ?>
		    </div>
		    <div class="phone-c">
		        <h2 class="phone-text"><?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Footer"]'); ?></h2>
		    </div>
		</div>
	</div><!-- end bottom-bar-left -->
	<div id="bottom-bar-right"> 
		 <a href="/contact/"><button class="btn-small btn-brown">Contact Us</button></a>
	</div><!-- end bottom-bar-right -->
</div><!-- end bottom-bar -->
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/library/js/jquery.cycle2.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/library/js/jquery.cycle2.scrollVert.min.js"></script>
<?php /*<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/library/js/lightbox.js"></script> */ ?>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/library/js/jquery.stellar.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/library/js/jquery.sidr.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/library/js/scripts.js?v=1.02"></script>
<script src="<?php echo get_template_directory_uri(); ?>/library/js/jquery.cookie.js"></script><!-- added this from old MH site for cookie -->


  <!-- this is the mobile popover from the old site -->
   <?php global $frn_mobile; ?>
   
    <?php if($frn_mobile == "Smartphone") {?>
   <div id="mobile_overlay" class="">
    <div id="mobileCTA">
		<div id="close_btn"><img src="<?php echo get_template_directory_uri(); ?>/style/images/close-icon.svg" /></div>
        <h3 class="text-center">Speak to a Professional</h3>
        <div class="text-center orange phone-cta"><?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Mobile Lightbox"]'); ?></div>        
        <p>We care about your unique situation and needs. At Black Bear Lodge, our goal is to provide lifetime recovery solutions.</p>
    </div><!-- end mobile CTA -->
</div><!-- end mobile_overlay -->

<?php } ?><!-- end frn function on for mobile detection on popover -->

</body>
</html>
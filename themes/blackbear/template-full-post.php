<?php
/*
Template Name: Full Post
Template Post Type: post
*/
?>
<?php get_header(); ?>
<header id="page-id">
<div class="tier-content-block">
	<div class="text-block">
		<h1><?php the_title(); ?></h1>
		<?php get_template_part('library/includes/breadcrumbs'); ?>
	</div><!-- end text-block -->
	<?php if(get_post_meta($post->ID,'ranklab_page_summary', true)) { ?>
		<!-- <div class="text-block page-message">
			<h2><?php echo get_post_meta($post->ID,'ranklab_page_summary', true); ?></h2>
		</div> --><!-- end text-block -->
	<?php } ?>
</div><!-- end tier-content-block-->
</header>
<div class="main clearfix inner-page">
<div class="tier-content-block">
<section role="main" class="full-content">
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>	
<article>
	<?php
	if ( has_post_thumbnail() ) {
		the_post_thumbnail( 'large' );
	}
	?>
	<?php the_content(); ?>
	<!-- post details -->
<div class="box green"><em>Articles posted here are primarily educational and may not directly reflect the offerings at Black Bear Lodge. For more specific information on programs at Black Bear Lodge, contact us today.</em></div>		
	<div class="postmeta-block">
	<div class="postmeta icon-pc-s">
		<p class="date"><strong>Date:</strong> <?php the_time('F j, Y'); ?> <?php the_time('g:i a'); ?></p>
		<p class="tags"><?php the_tags( __( 'Tags: ' ), ', ', '<br>'); // Separated by commas with a line break at the end ?></p>			
		<p class="categories"><strong>Categorized in:</strong> <?php _e( '' ); the_category(', '); // Separated by commas ?></p>
		<!--<p class="author"><strong>This post was written by:</strong> <?php //_e( ' ' ); the_author(); ?></p>-->
	</div><!-- end postmeta -->
	</div><!-- end postmeta block -->
	<!-- /post details -->
	<?php comments_template(); ?>
</article>
<?php endwhile; endif; ?>
</section>
</div><!-- end tier-content-block-->
</div> <!-- #main -->
<?php get_footer(); ?>
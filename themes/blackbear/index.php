<?php get_header(); ?>
<div class="main clearfix">
<section class="tier main-promo">
	<div class="text-block">
		<h1><span class="drk-brown">Discover</span> a New Path</h1>
		<p class="sub-text drk-brown">A peaceful retreat for addiction and mental health treatment</p>
		<a href="/residential-drug-rehab/"><button class="btn btn-brown icon-pinecone-sm-lt">How We Can Help</button></a>
	</div><!-- end text-block -->
	<!--
	<div id="brochure"><a href="http://www.foundationsrecoverynetwork.com/wp-content/uploads/Patient-Centered-Care.pdf" target="_blank"><img src="http://oursecuredev.com/ranklab/blackbear/wp-content/uploads/2013/12/brochure.png"></a></div>
	-->
</section><!-- main-promo -->

<section class="tier announcment bgimage">
	<div class="tier-content-block">
		<div class="sl_callout secondary" style="text-align: center; border-radius: 3px; max-width: 800px; margin: 0 auto;">
			<h2>A lifetime of recovery is just one phone call away.</h2>
			<p class="sub-text">Take a step towards a happier, healthier life.</p>
			<div class="test-video"><iframe width="100%" height="100%" src="https://www.youtube.com/embed/aIKxULuzvw8" frameborder="0" allowfullscreen></iframe></div>
		</div><!-- end sl_callout -->
	</div><!-- end tier-content-block-->
</section><!-- callout -->

<section class="tier video bgimage">
	<div class="tier-content-block" style="padding-top: 5%; padding-bottom: 0%;">
		<div class="one-half">
			<h2>Evidence-Based Treatment</h2>
			<p class="sub-text">The highly trained staff at Black Bear Lodge has years of experience treating addiction and mental health with the most effective methods backed by results.</p>
			<a href="/programs/"><button class="btn btn-green icon-pinecone-sm-drk ">Our Programs</button></a>
		</div>
		<div class="one-half">
			<div class="test-video"><iframe width="100%" height="100%" src="https://www.youtube.com/embed/TDU0UGgDNKw" frameborder="0" allowfullscreen></iframe></div>
		</div><!-- end one-half -->
	</div><!-- end tier-content-block-->
</section><!-- video -->

<section class="tier about">
	<div class="tier-content-block" style="padding-top: 5%; padding-bottom: 0%;">
		<div class="one-half icon-pinecone-lrg-lt">
			<div class="about-content">
				<h2>About Black Bear Lodge</h2>
				<p class="sub-text">Located in northern Georgia, Black Bear Lodge serves patients from across the state and around the world.</p>
				<p class="about-para">Black Bear Lodge is a place of solace from the chaos that substance abuse and mental health issues can bring into a person’s life. Call us today to get the help you need to take the first step toward recovery.</p>

				<a href="/location/"><button class="btn btn-green icon-pinecone-sm-drk">See Facility Photos</button></a>
			</div>
			<div class="test-video about-video"><iframe width="100%" height="100%" src="https://www.youtube.com/embed/1_HmbKcYQj0" frameborder="0" allowfullscreen></iframe></div>

		</div><!-- end one-half -->
		<div class="one-half">
			<div class="reputation--container">
				<h2 class="reputation-title"> Recent Reviews </h2>
				<a class="reputation-widget" target="_blank" href="https://widgets.reputation.com/widgets/610985068dd068152fa2c833/run?tk=52d91720521" data-tk="52d91720521" data-widget-id="610985068dd068152fa2c833" env="" region="us">Reputation Reviews</a>
				<script>!function(d,s,c){var js,fjs=d.getElementsByTagName(s)[0];js=d.createElement(s);js.className=c;js.src="https://widgets.reputation.com/src/client/widgets/widgets.js?v=1592944030435";fjs.parentNode.insertBefore(js,fjs);}(document,"script","reputation-wjs");</script>
			</div>
		</div><!-- end one-half -->
	</div><!-- end tier-content-block-->
</section><!-- about -->

<section class="tier stat bgimage">
	<div class="tier-content-block" style="padding-top: 5%; padding-bottom: 0%;" >
		<div class="one-half">
			<div class="text-block">
			<h2>Each year 23.5 million people need treatment for illicit drug or alcohol abuse problems.</h2>
			<p class="sub-text lighter-brown">"But only 2.6 million of those persons will be treated at a rehab facility."<sup><a target="_blank" href="http://www.drugabuse.gov/publications/drugfacts/treatment-statistics">1</a></sup></p>
			<p>When drug or alcohol dependence occurs, it not only affects the patient, but also impacts everyone in that person's life, including those closest to him or her. However, with intensive professional treatment that is tailor-made to the needs of the individual patient, the support of loved ones, and long-term aftercare services, success in recovery is possible. </p><p>It's common for patients diagnosed with a substance abuse issue to also have the symptoms that define a co-occurring mental health disorder. When co-occurring disorders are present, Dual Diagnosis treatment that addresses both disorders intensively and simultaneously is recommended for optimum success in recovery.</p>
			<a href="/success/"><button class="btn btn-green icon-pinecone-sm-drk">Our Success</button></a>
			</div>
		</div><!-- end one-half -->
		<div class="one-half">
			<div class="round-stat">
				<div class="stat-info">
					<div class="stat-item stat-big">
						<h2>23.5</h2>
						<p>Million people with addiction in the US</p>
					</div><!-- end stat-item -->
					<div class="stat-item stat-medium green">
						<h2 class="green">2.6</h2>
						<p>Million receive treatment</p>
					</div><!-- end stat-item -->
					<div class="stat-item stat-small">
						<p>That's barely</p>
						<h2>11%</h2>
					</div> <!-- end stat-item -->
				</div><!-- end stat-info -->
			</div><!-- end round-stat -->
		</div><!-- end one-half -->
	</div><!-- end tier-content-block-->
</section><!-- stat -->

<section class="tier insurance bgimage">
	<div class="tier-content-block" style="padding-top: 5%; padding-bottom: 0%;">
		<div class="one-half icon-insurance">
			<p class="sub-text yellow">KNOW YOUR OPTIONS</p>
			<h2 class="green">WHEN IT COMES TO PAYING FOR TREATMENT</h2>
		</div><!-- end one-half -->
		<div class="one-half">
			<p class="sub-text yellow">We accept health care insurance from most providers. Determine your coverage for free by asking us to run an <a href="/insurance/">insurance verification.</a></p>
		</div><!-- end one-half -->
	</div><!-- end tier-content-block-->
</section><!-- insurance -->

</div> <!-- #main -->
<?php get_footer(); ?>
<?php
/*
Template Name: FRN Formatting
*/
?>

<?php get_header(); ?>
<header id="page-id">
<div class="tier-content-block">
	<div class="text-block icon-pinecone-lrg-lt">
		<h1><?php the_title(); ?></h1>
		<?php get_template_part('library/includes/breadcrumbs'); ?>
	</div><!-- end text-block -->
	<?php if(get_post_meta($post->ID,'ranklab_page_summary', true)) { ?>
		<div class="text-block page-message">
			<h2><?php echo get_post_meta($post->ID,'ranklab_page_summary', true); ?></h2>
		</div><!-- end text-block -->
	<?php } ?>
</div><!-- end tier-content-block-->
</header>
<div class="main clearfix inner-page">
<div class="tier-content-block">
<section role="main" class="full-content">

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>	

<?php 
// This is where all of the sample formatting stuff goes
// Please note the code in the <textarea> requires no formatting/spacing tabs.
?>

<style>code{background: none; border: none;}textarea{overflow: hidden;}</style>
<br>

<section>
<div class="row">
	<div class="one-half">
		<h1>h1. This is a very large header.</h1>
		<h2>h2. This is a large header.</h2>
		<h3>h3. This is a medium header.</h3>
		<h4>h4. This is a moderate header.</h4>
		<h5>h5. This is a small header.</h5>
		<h6>h6. This is a tiny header.</h6>
	</div><!-- /.one-half-->
	<div class="one-half">
		<code class="clearfix">
			<textarea style="height: 200px;">
<h1>h1. This is a very large header.</h1>
<h2>h2. This is a large header.</h2>
<h3>h3. This is a medium header.</h3>
<h4>h4. This is a moderate header.</h4>
<h5>h5. This is a small header.</h5>
<h6>h6. This is a tiny header.</h6>
			</textarea>
		</code>
	</div><!-- /.one-half -->
</div><!-- /.row -->

<div class="sl_divider divider"></div>

<div class="row">
	<div class="one-half">

<div class="sl_divider divider"></div>

	</div><!-- /.one-half -->
	<div class="one-half">
		<code class="clearfix">
			<textarea style="height: 30px;">
[divider]
			</textarea>
		</code>
	</div><!-- /.one-half -->
</div><!-- /.row -->

<div class="sl_divider divider"></div>

<div class="row">
	<div class="one-half columns">
		<br>
		<blockquote>“Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam eleifend turpis dui. Fusce id interdum dui, ac semper risus. Curabitur sed fermentum lorem. Vestibulum vitae efficitur augue, nectw.” <cite>Noah benShea</cite></blockquote>
		<blockquote class="secondary">“Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam eleifend turpis dui. Fusce id interdum dui, ac semper risus. Curabitur sed fermentum lorem. Vestibulum vitae efficitur augue, nectw.” <cite>Noah benShea</cite></blockquote>
	</div><!-- /.one-half -->
	<div class="one-half columns">
		<code class="clearfix">
			<textarea style="height: 350px;">
<blockquote>“Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam eleifend turpis dui. Fusce id interdum dui, ac semper risus. Curabitur sed fermentum lorem. Vestibulum vitae efficitur augue, nectw.”
<cite>Noah benShea</cite></blockquote>
<blockquote class="secondary">“Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam eleifend turpis dui. Fusce id interdum dui, ac semper risus. Curabitur sed fermentum lorem. Vestibulum vitae efficitur augue, nectw.” <cite>Noah benShea</cite></blockquote>
			</textarea>
		</code>
	</div><!-- /.one-half -->
</div><!-- /.row -->

<div class="sl_divider divider"></div>

<div class="row">
	<div class=one-half columns">
		<br>
		<h4>Un-ordered Lists</h4>
		<ul>
		 	<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam beatae consequatur quam.</li>
		 	<li>Deserunt, recusandae veritatis illum perspiciatis illo quae odio laboriosam laudantium consequuntur eveniet? Magni, tempore, animi.</li>
		 	<li>Tempora, laboriosam, aspernatur, quisquam, recusandae pariatur hic dignissimos a inventore magni cupiditate quis eius. Nobis.</li>
		</ul>
		<ul class="secondary">
		 	<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam beatae consequatur quam.</li>
		 	<li>Deserunt, recusandae veritatis illum perspiciatis illo quae odio laboriosam laudantium consequuntur eveniet? Magni, tempore, animi.</li>
		 	<li>Tempora, laboriosam, aspernatur, quisquam, recusandae pariatur hic dignissimos a inventore magni cupiditate quis eius. Nobis.</li>
		</ul>
		<ul class="disc">
		 	<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam beatae consequatur quam.</li>
		 	<li>Deserunt, recusandae veritatis illum perspiciatis illo quae odio laboriosam laudantium consequuntur eveniet? Magni, tempore, animi.</li>
		 	<li>Tempora, laboriosam, aspernatur, quisquam, recusandae pariatur hic dignissimos a inventore magni cupiditate quis eius. Nobis.</li>
		</ul>
	</div><!-- /.one-half columns -->
	<div class="one-half columns">
		<code class="clearfix">
			<textarea style="height: 275px;">
<h4>Un-ordered Lists</h4>
<ul>
<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam beatae consequatur quam.</li>
<li>Deserunt, recusandae veritatis illum perspiciatis illo quae odio laboriosam laudantium consequuntur eveniet? Magni, tempore, animi.</li>
<li>Tempora, laboriosam, aspernatur, quisquam, recusandae pariatur hic dignissimos a inventore magni cupiditate quis eius. Nobis.</li>
</ul>
<ul class="secondary">
<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam beatae consequatur quam.</li>
<li>Deserunt, recusandae veritatis illum perspiciatis illo quae odio laboriosam laudantium consequuntur eveniet? Magni, tempore, animi.</li>
<li>Tempora, laboriosam, aspernatur, quisquam, recusandae pariatur hic dignissimos a inventore magni cupiditate quis eius. Nobis.</li>
</ul>
<ul class="disc">
<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam beatae consequatur quam.</li>
<li>Deserunt, recusandae veritatis illum perspiciatis illo quae odio laboriosam laudantium consequuntur eveniet? Magni, tempore, animi.</li>
<li>Tempora, laboriosam, aspernatur, quisquam, recusandae pariatur hic dignissimos a inventore magni cupiditate quis eius. Nobis.</li>
</ul>
			</textarea>
		</code>
	</div><!-- /.one-half columns -->
</div><!-- /.row -->

<div class="sl_divider divider"></div>

<div class="row">
	<div class="one-half columns">
		<br>
		<h4>Ordered Lists</h4>
		<ol>
		 	<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam beatae consequatur quam.</li>
		 	<li>Deserunt, recusandae veritatis illum perspiciatis illo quae odio laboriosam laudantium consequuntur eveniet? Magni, tempore, animi.</li>
		 	<li>Tempora, laboriosam, aspernatur, quisquam, recusandae pariatur hic dignissimos a inventore magni cupiditate quis eius. Nobis.</li>
		</ol>
	</div><!-- /.one-half columns -->
	<div class="one-half columns">
		<code class="clearfix">
			<textarea style="height: 275px;">
<h4>Ordered Lists</h4>
<ol>
<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam beatae consequatur quam.</li>
<li>Deserunt, recusandae veritatis illum perspiciatis illo quae odio laboriosam laudantium consequuntur eveniet? Magni, tempore, animi.</li>
<li>Tempora, laboriosam, aspernatur, quisquam, recusandae pariatur hic dignissimos a inventore magni cupiditate quis eius. Nobis.</li>
</ol>
			</textarea>
		</code>
	</div><!-- /.one-half columns -->
</div><!-- /.row -->

<div class="sl_divider divider"></div>

<div class="row">
	<div class="one-half columns">
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
		<div class="sl_callout callout no-box">
			Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.
		</div>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
		<div class="sl_callout callout no-box-large">
			Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.
		</div>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
	</div><!-- /.col columns -->
	<div class="one-half columns">
		<code class="clearfix">
			<textarea style="height: 700px;">
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
[callout style="no-box"]
Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.
[/callout]
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
[callout style="no-box-large"]
Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.
[/callout]
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
			</textarea>
		</code>
	</div><!-- /.col columns -->
</div><!-- /.row -->

<div class="sl_divider divider"></div>


<div class="row">
	<div class="one-half columns">
		<div class="sl_callout callout">
			<h4>H4. This is the Default Callout</h4>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
		</div>
		<div class="sl_callout callout secondary">
			<h4>H4. This is the Secondary Callout</h4>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
		</div>
		<div class="sl_callout callout dark">
			<h4>H4. This is the Dark Callout</h4>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
		</div>
		<div class="sl_callout callout border">
			<h4>H4. This is a Border Callout</h4>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
		</div>
		<div class="sl_callout-card callout-card">
			<div class="card-media" style="background-image: url(<?php bloginfo('template_directory'); ?>/style/images/blackbear-facility.jpg)"></div>
			<div class="card-content">
				<h4>This is the Default Callout Card</h4>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
			</div>
		</div>
		<div class="sl_callout-card callout-card dark">
			<div class="card-media" style="background-image: url(<?php bloginfo('template_directory'); ?>/style/images/blackbear-facility.jpg)"></div>
			<div class="card-content">
				<h4>This is a Secondary Callout Card</h4>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
			</div>
		</div>
		<div class="sl_callout-card callout-card border">
			<div class="card-media" style="background-image: url(<?php bloginfo('template_directory'); ?>/style/images/blackbear-facility.jpg)"></div>
			<div class="card-content">
				<h4>This is a Border Callout Card</h4>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
			</div>
		</div>
	</div><!-- /.col columns -->
	<div class="one-half columns">
		<code class="clearfix">
			<textarea style="height: 1000px;">
[callout]
<h4>H4. This is the Default Callout</h4>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
[/callout]
[callout style="secondary"]
<h4>H4. This is the Secondary Callout</h4>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
[/callout]
[callout style="dark"]
<h4>H4. This is the Dark Callout</h4>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
[/callout]
[callout style="border"]
<h4>H4. This is a Border Callout</h4>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
[/callout]
[callout-card img="/wp-content/themes/oaks/style/images/blackbear-facility.jpg"]
<h4>This is the Default Callout Card</h4>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
[/callout-card]
[callout-card img="/wp-content/themes/oaks/style/images/blackbear-facility.jpg" style="dark"]
<h4>This is a Secondary Callout Card</h4>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
[/callout-card]
[callout-card img="/wp-content/themes/oaks/style/images/blackbear-facility.jpg" style="border"]
<h4>This is a Border Callout Card</h4>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
[/callout-card]
			</textarea>
		</code>
	</div><!-- /.col columns -->
</div><!-- /.row -->

<div class="sl_divider divider"></div>

<div class="row">
	<div class="one-half columns">
<p><a href="#" class="sl_button button " target="_self">Button</a> <a href="#" class="sl_button button secondary" target="_self">Button</a> <a href="#" class="sl_button button dark" target="_self">Button</a> <a href="#" class="sl_button button border" target="_self">Button</a></p>
<div class="sl_read-next--button">
	<h4>Read This Next:</h4>
	<p><a href="#" class="sl_button button read-next" target="_self">Long Article Title</a></p>
</div>
	</div><!-- /.col columns -->
	<div class="one-half columns">
		<code class="clearfix">
			<textarea style="height: 150px;">
[button url="#"]Button 1[/button]
[button url="#" style="button--secondary"]Button 2[/button]
[button url="#"]Button[/button]
[button url="#" style="secondary"]Button[/button]
[button url="#" style="dark"]Button[/button]
[button url="#" style="border"]Button[/button]
[button url="#" style="read-next"]Long Article Title[/button]
			</textarea>
		</code>
	</div><!-- /.col columns -->
</div><!-- /.row -->

<div class="sl_divider divider"></div>

<div class="row">
	<div class="one-half columns">
		<?php echo do_shortcode('[read-next url="#" title="Link title goes here"]This is the preview text...[/read-next]')?>
		<?php echo do_shortcode('[read-next style="large" url="#" title="Link title goes here"]This is the preview text...[/read-next]')?>
	</div><!-- /.col columns -->

	<div class="one-half columns">
			<textarea style="height: 100px;">
[read-next url="#" title="Link title goes here"]This is the preview text...[/read-next]
[read-next style="large" url="#" title="Link title goes here"]This is the preview text...[/read-next]
			</textarea>
	</div><!-- /.col columns -->

</div><!-- /.row -->

<div class="sl_divider divider"></div>

<div class="row">
	<div class="one-half columns">
		<p>If you want to use two sets of tabs on a page. Make sure the second one has the style of "alternate" for the [tabs] and the [tab-menu]. Don't use more than two sets of tabs on a page.</p>
		<div class="sl_content-tabs content-tabs primary">
			<div class="menu-wrapper">
				<ul class="tab-menu primary">
					<li class="tab1"><a href="#" class="current">Tab 1</a></li>
					<li class="tab2"><a href="#">Tab 2</a></li>
					<li class="tab3"><a href="#">Tab 3</a></li>
				</ul>
			</div>
			<div class="tab-content-wrapper">
				<div class="tab1 dynamic">
					<h4>Tab 1</h4>
					<p>testing testing testing</p>
				</div>
				<div class="tab2 dynamic" style="display: none;">
					<h4>Tab 2</h4>
					<p>testing testing testing</p>
				</div>
				<div class="tab3 dynamic" style="display: none;">
					<h4>Tab 3</h4>
					<p>testing testing testing</p>
				</div>
			</div>
		</div>

		<div class="sl_content-tabs content-tabs alternate secondary">
			<div class="menu-wrapper">
				<ul class="tab-menu alternate">
					<li class="tab4"><a href="#" class="current">Tab 1</a></li>
					<li class="tab5"><a href="#">Tab 2</a></li>
					<li class="tab6"><a href="#">Tab 3</a></li>
				</ul>
			</div>
			<div class="tab-content-wrapper">
				<div class="tab4 dynamic">
					<h4>Tab 1</h4>
					<p>testing testing testing</p>
				</div>
				<div class="tab5 dynamic" style="display: none;">
					<h4>Tab 2</h4>
					<p>testing testing testing</p>
				</div>
				<div class="tab6 dynamic" style="display: none;">
					<h4>Tab 3</h4>
					<p>testing testing testing</p>
				</div>
			</div>
		</div>

	</div><!-- /.one-half columns -->
	<div class="one-half columns">
		<code class="clearfix">
			<textarea style="height: 570px;">
[tabs]
[tab-menu]
[tab-title]Tab 1[/tab-title]
[tab-title]Tab 2[/tab-title]
[tab-title]Tab 3[/tab-title]
[/tab-menu]

[tab-content]
[tab-panel]
<h2>Tab 1</h2>
testing testing testing
[/tab-panel]

[tab-panel]
<h2>Tab 2</h2>
testing testing testing
[/tab-panel]

[tab-panel]
<h2>Tab 3</h2>
testing testing testing
[/tab-panel]

[/tab-content]
[/tabs]

[tabs style="alternate secondary"]
[tab-menus style="alternate"]
[tab-title]Tab 1[/tab-title]
[tab-title]Tab 2[/tab-title]
[tab-title]Tab 3[/tab-title]
[/tab-menu]

[tab-content]
[tab-panel]
<h2>Tab 1</h2>
testing testing testing
[/tab-panel]

[tab-panel]
<h2>Tab 2</h2>
testing testing testing
[/tab-panel]

[tab-panel]
<h2>Tab 3</h2>
testing testing testing
[/tab-panel]

[/tab-content]
[/tabs]
			</textarea>
		</code>
	</div><!-- /.one-half columns -->
</div><!-- /.row -->

<div class="sl_divider divider"></div>

<div class="row">
	
	<div class="one-half columns">
		<div class="sl_accordion accordion">
			<h3>Accordion Title 1<span class="plus">+</span></h3>
			<div>
				<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante.</p>
			</div>
			<h3>Accordion Title 2<span class="plus">+</span></h3>
			<div>
				<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante.</p>
			</div>
		</div>
		<div class="sl_accordion accordion secondary">
			<h3>Accordion Title 1<span class="plus">+</span></h3>
			<div>
				<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante.</p>
			</div>
			<h3>Accordion Title 2<span class="plus">+</span></h3>
			<div>
				<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante.</p>
			</div>
		</div>
	</div><!-- /.col columns -->
	<div class="one-half columns">
		<code class="clearfix">
			<textarea style="height: 400px; overflow-y: scroll;">
[accordion]
[accordion-item title="Accordion Title 1"]
Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante
[/accordion-item]
accordion-item title="Accordion Title 1"]
Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante
[/accordion-item]
[/accordion]
[accordion style="secondary"]
[accordion-item title="Accordion Title 1"]
Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante
[/accordion-item]
accordion-item title="Accordion Title 1"]
Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante
[/accordion-item]
[/accordion]
			</textarea>
		</code>
	</div><!-- /.col columns -->
</div><!-- /.row -->

<div class="sl_divider divider"></div>

<div class="sl_row row">
	<div class="three-fifth sl_columns columns">
		<?php echo do_shortcode('[email not_content="true"]'); ?>
		<?php echo do_shortcode('[email heading="You can customize this" campaign="specific_campaign" not_content="true"]'); ?>
	</div><!-- /.col columns -->

	<div class="two-fifth sl_columns columns">
		<code class="clearfix">
			<textarea style="height: 300px;">
[email]
[email heading="You can customize this" campaign="specific_campaign"]
			</textarea>
		</code>
	</div><!-- /.col columns -->

</div><!-- /.row -->

<div class="sl_divider divider"></div>

<h1>Columns</h1>
<div class="row">
	<div class="one-half columns">
	<h4>One Half</h4>
	Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
	</div>	<!-- /.one-half -->

	<div class="one-half columns">
	<h4>One Half</h4>
	Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
	</div>	<!-- /.one-half -->
</div><!-- /.row -->

<div class="row">
	<div class="one-third columns">
	<h4>One Third</h4>
	Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
	</div>	<!-- /.one-third -->

	<div class="one-third columns">
	<h4>One Third</h4>
	Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
	</div>	<!-- /.one-third -->

	<div class="one-third columns">
	<h4>One Third</h4>
	Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
	</div>	<!-- /.one-third -->
</div><!-- /.row -->

<div class="row">
	<div class="one-fourth columns">
	<h4>One Fourth</h4>
	Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
	</div>	<!-- /.one-fourth -->

	<div class="one-fourth columns">
	<h4>One Fourth</h4>
	Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
	</div>	<!-- /.one-fourth -->

	<div class="one-fourth columns">
	<h4>One Fourth</h4>
	Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
	</div>	<!-- /.one-fourth -->

	<div class="one-fourth columns">
	<h4>One Fourth</h4>
	Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
	</div>	<!-- /.one-fourth -->
</div><!-- /.row -->

<div class="row">
	<div class="one-fifth columns">
	<h4>One Fifth</h4>
	Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
	</div>	<!-- /.one-fifth -->

	<div class="one-fifth columns">
	<h4>One Fifth</h4>
	Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
	</div>	<!-- /.one-fifth -->

	<div class="one-fifth columns">
	<h4>One Fifth</h4>
	Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
	</div>	<!-- /.one-fifth -->

	<div class="one-fifth columns">
	<h4>One Fifth</h4>
	Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
	</div>	<!-- /.one-fifth -->

	<div class="one-fifth columns">
	<h4>One Fifth</h4>
	Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
	</div>	<!-- /.one-fifth -->
</div><!-- /.row -->

<div class="row">
	<div class="one-sixth columns">
	<h4>One Sixth</h4>
	Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
	</div>	<!-- /.one-sixth -->

	<div class="one-sixth columns">
	<h4>One Sixth</h4>
	Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
	</div>	<!-- /.one-sixth -->

	<div class="one-sixth columns">
	<h4>One Sixth</h4>
	Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
	</div>	<!-- /.one-sixth -->

	<div class="one-sixth columns">
	<h4>One Sixth</h4>
	Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
	</div>	<!-- /.one-sixth -->

	<div class="one-sixth columns">
	<h4>One Sixth</h4>
	Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
	</div>	<!-- /.one-sixth -->

	<div class="one-sixth columns">
	<h4>One Sixth</h4>
	Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
	</div>	<!-- /.one-sixth -->
</div><!-- /.row -->


<div class="row">
	<div class="columns">
			<code class="clearfix">
			<textarea style="height: 500px; overflow-y: scroll;">
[row]
[col size="one-half"]
<h4>One Half</h4>
Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
[/col]

[col size="one-half"]
<h4>One Half</h4>
Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
[/col]
[/row]

[row]
[col size="one-third"]
<h4>One Third</h4>
Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
[/col]

[col size="one-third"]
<h4>One Third</h4>
Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
[/col]

[col size="one-third"]
<h4>One Third</h4>
Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
[/col]
[/row]

[row]
[col size="one-fourth"]
<h4>One Fourth</h4>
Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
[/col]

[col size="one-fourth"]
<h4>One Fourth</h4>
Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
[/col]

[col size="one-fourth"]
<h4>One Fourth</h4>
Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
[/col]

[col size="one-fourth"]
<h4>One Fourth</h4>
Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
[/col]
[/row]

[row]
[col size="one-fifth"]
<h4>One Fifth</h4>
Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
[/col]

[col size="one-fifth"]
<h4>One Fifth</h4>
Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
[/col]

[col size="one-fifth"]
<h4>One Fifth</h4>
Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
[/col]

[col size="one-fifth"]
<h4>One Fifth</h4>
Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
[/col]

[col size="one-fifth"]
<h4>One Fifth</h4>
Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
[/col]
[/row]

[row]
[col size="one-sixth"]
<h4>One Sixth</h4>
Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
[/col]

[col size="one-sixth"]
<h4>One Sixth</h4>
Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
[/col]

[col size="one-sixth"]
<h4>One Sixth</h4>
Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
[/col]

[col size="one-sixth"]
<h4>One Sixth</h4>
Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
[/col]

[col size="one-sixth"]
<h4>One Sixth</h4>
Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
[/col]

[col size="one-sixth"]
<h4>One Sixth</h4>
Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
[/col]
[/row]
		</textarea>
		</code>
	</div><!-- /.col columns -->
</div><!-- /.row -->

</section>

<?php endwhile; endif; ?>
</div><!-- end content -->
</div><!-- end wrapper div -->
</div><!-- end inside div -->
					
					
					
					
					
			
    
				<?php //get_sidebar(); // sidebar 1 ?>
    
		

<?php get_footer(); ?>
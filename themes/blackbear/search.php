<?php get_header(); ?>
<header id="page-id">
<div class="tier-content-block">
	<div class="text-block icon-pinecone-lrg-lt">
		<h1>Search Results</h1>
	</div><!-- end text-block -->
</div><!-- end tier-content-block-->
</header>
<div class="main clearfix inner-page">
<div class="tier-content-block">
<section role="main" class="left-content">
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>	
<article>
	<h2><a href="<?php the_permalink();?>"><?php the_title();?></h2>
</article>
<?php endwhile; endif; ?>
</section>
<?php get_sidebar(); ?>
</div><!-- end tier-content-block-->
</div> <!-- #main -->
<?php get_footer(); ?>

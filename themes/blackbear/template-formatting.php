<?php
/*
Template Name: Formatting
*/
?>
<?php get_header(); ?>
<header id="page-id">
<div class="tier-content-block">
	<div class="text-block icon-pinecone-lrg-lt">
		<h1><?php the_title(); ?></h1>
		<?php get_template_part('library/includes/breadcrumbs'); ?>
	</div><!-- end text-block -->
	<?php if(get_post_meta($post->ID,'ranklab_page_summary', true)) { ?>
		<!-- <div class="text-block page-message">
			<h2><?php echo get_post_meta($post->ID,'ranklab_page_summary', true); ?></h2>
		</div> --><!-- end text-block -->
	<?php } ?>
</div><!-- end tier-content-block-->
</header>
<div class="main clearfix inner-page">
<div class="tier-content-block">
<section role="main" class="full-content">

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>	

<?php the_content(); ?>
		
<h2>Table of Contents</h2>
<div class="toc">
<h2>Find Fast</h2>
<a href="">Link Title Here</a>
<a href="">Link Title Here</a>
<a href="">Link Title Here</a>
</div><!-- end toc -->

<code><textarea style="height:120px;">
<div class="toc">
<h2>Find Fast</h2>
<a rel="nofollow" href="#">Link Title Here</a>
<a rel="nofollow" href="#">Link Title Here</a>
<a rel="nofollow" href="#">Link Title Here</a>
</div><!-- end toc -->
<a name=""></a> 
</textarea></code>



<h2>Boxes</h2>
<div class="box">
<h3>Lorem Ipsum Dolor Sit Amit</h3>
<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas <a href="#">Home</a> semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
</div>

<div class="box gray">
<h3>Lorem Ipsum Dolor Sit Amit</h3>
<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas <a href="#">Home</a> semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
</div>

<div class="box green">
<h3>Lorem Ipsum Dolor Sit Amit</h3>
<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas <a href="#">Home</a> semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
</div>

<code><textarea style="height:120px;">
<div class="box">
<h3>Lorem Ipsum Dolor Sit Amit</h3>
<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas <a href="#">Home</a> semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
</div>

<div class="box gray">
<h3>Lorem Ipsum Dolor Sit Amit</h3>
<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas <a href="#">Home</a> semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
</div>

<div class="box lblue">
<h3>Lorem Ipsum Dolor Sit Amit</h3>
<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas <a href="#">Home</a> semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
</div>

</textarea></code>



<h2>Sub Text Paragraph Option</h2>
<p class="sub-text">This is a Sub Text Paragraph habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>

<p>This is a Regular Paragraph habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>

<code><textarea style="height:120px;">
<p class="sub-text">Sub Text Paragraph Option</p>
</textarea></code>


<h2>Lists</h2>

<h3>Basic</h3>
<ul>
<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
</ul>

<code><textarea style="height:120px;">
<ul>
<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
</ul>
</textarea></code>

<h2>Row</h2>
<div class="row">
<div class="one-sixth"><h3>one-sixth</h3><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p></div><!-- end one-fourth -->
<div class="five-sixth"><h3>five-sixth</h3><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p></div><!-- end one fourth -->
</div>
<code><textarea rows="4">
<div class="row">
<div class="one-sixth">Content Goes Here</div><!-- end one-sixth-->
<div class="five-sixth">Content Goes Here</div><!-- end one-sixth -->
</div>
</textarea></code>

<h2>One Half Column</h2>
<div class="one-half"><h3>one-half</h3><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p></div><!-- end one-half -->
<div class="one-half"><h3>one-half</h3><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p></div><!-- end one-half -->

<code><textarea>
<div class="one-half">Content Goes Here</div><!-- end one-half -->
<div class="one-half">Content Goes Here</div><!-- end one-half -->
</textarea></code>



<h2>One Third</h2>
<div class="one-third"><h3>one-third</h3><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p></div><!-- end one-third -->
<div class="one-third"><h3>one-third</h3><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p></div><!-- end one-third -->
<div class="one-third"><h3>one-third</h3><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p></div><!-- end one-third -->

<code><textarea>
<div class="one-third">Content Goes Here</div><!-- end one-third -->
<div class="one-third">Content Goes Here</div><!-- end one-third -->
<div class="one-third">Content Goes Here</div><!-- end one-third -->
</textarea></code>



<h2>Two Third</h2>
<div class="two-third"><h3>two-third</h3><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.  Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p></div><!-- end two-third -->
<div class="one-third"><h3>one-third</h3><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p></div><!-- end one-third -->

<code><textarea>
<div class="two-third">Content Goes Here</div><!-- end two-third -->
<div class="one-third">Content Goes Here</div><!-- end one-third -->
</textarea></code>

<h2>One Fourth Columns</h2>
<div class="one-fourth"><h3>one-fourth</h3><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p></div><!-- end one-fourth -->
<div class="one-fourth"><h3>one-fourth</h3><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p></div><!-- end one-fourth -->
<div class="one-fourth"><h3>one-fourth</h3><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p></div><!-- end one-fourth -->
<div class="one-fourth"><h3>one-fourth</h3><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p></div><!-- end one-fourth -->

<code><textarea>
<div class="one-fourth">Content Goes Here</div><!-- end one-fourth -->
<div class="one-fourth">Content Goes Here</div><!-- end one-fourth -->
<div class="one-fourth">Content Goes Here</div><!-- end one-fourth -->
<div class="one-fourth">Content Goes Here</div><!-- end one-fourth -->
</textarea></code>


<h2>Three Fourth Column</h2>
<div class="three-fourth"><h3>three-fourth</h3><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p></div><!-- end one-fourth -->
<div class="one-fourth"><h3>one-fourth</h3><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p></div><!-- end one-fourth -->

<code><textarea>
<div class="three-fourth">Content Goes Here</div><!-- end three-fourth -->
<div class="one-fourth">Content Goes Here</div><!-- end one-fourth -->
</textarea></code>

<h2>One Fifth Columns</h2>
<div class="one-fifth"><h3>one-fifth</h3><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p></div><!-- end one-fifth -->
<div class="one-fifth"><h3>one-fifth</h3><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p></div><!-- end one-fifth -->
<div class="one-fifth"><h3>one-fifth</h3><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p></div><!-- end one-fifth -->
<div class="one-fifth"><h3>one-fifth</h3><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p></div><!-- end one-fifth -->
<div class="one-fifth"><h3>one-fifth</h3><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p></div><!-- end one-fifth -->

<code><textarea>
<div class="one-fifth">Content Goes Here</div><!-- end one-fifth -->
<div class="one-fifth">Content Goes Here</div><!-- end one-fifth-->
<div class="one-fifth">Content Goes Here</div><!-- end one-fifth -->
<div class="one-fifth">Content Goes Here</div><!-- end one-fifth -->
<div class="one-fifth">Content Goes Here</div><!-- end one-fifth -->
</textarea></code>

<h2>Two &AMP; Three Fifth Columns</h2>
<div class="two-fifth"><h3>two-fifth</h3><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p></div><!-- end two-fifth -->
<div class="three-fifth"><h3>three-fifth</h3><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p></div><!-- end three-fifth -->

<code><textarea>
<div class="two-fifth">Content Goes Here</div><!-- end two-fifth -->
<div class="three-fifth">Content Goes Here</div><!-- end three-fifth -->
</textarea></code>

<h2>Four Fifth Columns</h2>
<div class="one-fifth"><h3>one-fifth</h3><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p></div><!-- end two-fifth -->
<div class="four-fifth"><h3>four-fifth</h3><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p></div><!-- end three-fifth -->

<code><textarea>
<div class="one-fifth">Content Goes Here</div><!-- end two-fifth -->
<div class="four-fifth">Content Goes Here</div><!-- end three-fifth -->
</textarea></code>
		


<h2>One Sixth Columns</h2>
<div class="one-sixth"><h3>one-sixth</h3><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p></div><!-- end one-sixth -->
<div class="one-sixth"><h3>one-sixth</h3><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p></div><!-- end one-sixth -->
<div class="one-sixth"><h3>one-sixth</h3><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p></div><!-- end one-sixth -->
<div class="one-sixth"><h3>one-sixth</h3><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p></div><!-- end one-sixth -->
<div class="one-sixth"><h3>one-sixth</h3><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p></div><!-- end one-sixth -->
<div class="one-sixth"><h3>one-sixth</h3><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p></div><!-- end one-sixth-->

<code><textarea>
<div class="one-sixth">Content Goes Here</div><!-- end one-sixth-->
<div class="one-sixth">Content Goes Here</div><!-- end one-sixth -->
<div class="one-sixth">Content Goes Here</div><!-- end one-sixth-->
<div class="one-sixth">Content Goes Here</div><!-- end one-sixth -->
<div class="one-sixth">Content Goes Here</div><!-- end one-sixth -->
<div class="one-sixth">Content Goes Here</div><!-- end one-sixth -->
</textarea></code>
<h2>Five Sixth Columns</h2>
<div class="one-sixth"><h3>one-sixth</h3><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p></div><!-- end one-sixth -->
<div class="five-sixth"><h3>five-sixth</h3><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p></div><!-- end five-sixth -->

<code><textarea>
<div class="one-sixth">Content Goes Here</div><!-- end one-sixth-->
<div class="five-sixth">Content Goes Here</div><!-- end five-sixth -->

</textarea></code>



		
		

<h2>Accordion</h2>
<div class="accordion">		
<h3>Feature Title Here<span class="plus">+</span></h3>
<div>
	<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi.</p>
</div>

<h3>Feature Title Here<span class="plus">+</span></h3>
<div>
	<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi.</p>
</div>
</div><!-- end accordion div -->

<code><textarea style="height:300px;">
<div class="accordion">		
<h3>Feature Title Here<span class="plus">+</span></h3>
<div>
	<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi.</p>
</div>

<h3>Feature Title Here<span class="plus">+</span></h3>
<div>
	<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi.</p>
</div>
</div><!-- end accordion div -->
</textarea></code>
		
		
<h2>Tabs</h2>
<div class="content-tabs">
<div class="menu-wrapper">
  <ul class="tab-menu">
    <li class="one"><a href="#" class="current">Tab One</a></li>
	<li class="two"><a href="#" class="">Tab Two</a></li>
	<li class="three"><a href="#" class="">Tab Three</a></li>
  </ul>
</div><!-- end menu-wrapper -->
<div class="tab-content-wrapper">
	<div class="one dynamic" >
	   <h3>Lorem Ipsum Dolor Sit Amit</h3>
	   <p>tab one content</p>
	</div><!-- end one dynamic -->
	<div class="two dynamic" style="display:none;">
	   tab two content
	</div><!-- end one dynamic -->
	<div class="three dynamic" style="display:none;">
	   tab three content
	</div><!-- end one dynamic -->
</div><!-- end tab-content-wrapper -->
</div><!-- end tabs -->

<code><textarea style="height:350px;">
<div class="content-tabs">
<div class="menu-wrapper">
  <ul class="tab-menu">
    <li class="one"><a href="#" class="current">Tab One</a></li>
	<li class="two"><a href="#" class="">Tab Two</a></li>
	<li class="three"><a href="#" class="">Tab Three</a></li>
  </ul>
</div><!-- end menu-wrapper -->
<div class="tab-content-wrapper">
	<div class="one dynamic" >
		<h3>Lorem Ipsum Dolor Sit Amit</h3>
	   	<p>tab one content</p>
	</div><!-- end one dynamic -->
	<div class="two dynamic" style="display:none;">
	   tab two content
	</div><!-- end one dynamic -->
	<div class="three dynamic" style="display:none;">
	   tab three content
	</div><!-- end one dynamic -->
</div><!-- end tab-content-wrapper -->
</div><!-- end tabs -->
</textarea></code>




<h2>People</h2>
<div class="one-half">
	<div class="person-block"><span class="man-brw"></span></div>
	<div class="person-block"><span class="man-l-brw"></span></div>
	<div class="person-block"><span class="man-green"></span></div>
</div><!-- end one-half -->

<div class="one-half">
	<div class="person-block"><span class="woman-brw"></span></div>
	<div class="person-block"><span class="woman-l-brw"></span></div>
	<div class="person-block"><span class="woman-green"></span></div>
</div><!-- end one-half -->

<code><textarea style="height:350px;">
	<div class="person-block"><span class="man-brw"></span></div>
	<div class="person-block"><span class="man-l-brw"></span></div>
	<div class="person-block"><span class="man-green"></span></div>
	
	<div class="person-block"><span class="woman-brw"></span></div>
	<div class="person-block"><span class="woman-l-brw"></span></div>
	<div class="person-block"><span class="woman-green"></span></div>
</textarea></code>


<h2>Arrows</h2>
<div class="one-third">
	<div class="arrow-block"><span class="arrow-brw-1"></span></div>
	<div class="arrow-block"><span class="arrow-brw-2"></span></div>
	<div class="arrow-block"><span class="arrow-brw-3"></span></div>
</div><!-- end one-half -->

<div class="one-third">
	<div class="arrow-block"><span class="arrow-l-brw-1"></span></div>
	<div class="arrow-block"><span class="arrow-l-brw-2"></span></div>
	<div class="arrow-block"><span class="arrow-l-brw-3"></span></div>
</div><!-- end one-half -->

<div class="one-third">
	<div class="arrow-block"><span class="arrow-grn-1"></span></div>
	<div class="arrow-block"><span class="arrow-grn-2"></span></div>
	<div class="arrow-block"><span class="arrow-grn-3"></span></div>
</div><!-- end one-half -->

<code><textarea style="height:350px;">
<div class="one-third">
	<div class="arrow-block"><span class="arrow-brw-1"></span></div>
	<div class="arrow-block"><span class="arrow-brw-2"></span></div>
	<div class="arrow-block"><span class="arrow-brw-3"></span></div>
</div><!-- end one-half -->

<div class="one-third">
	<div class="arrow-block"><span class="arrow-l-brw-1"></span></div>
	<div class="arrow-block"><span class="arrow-l-brw-2"></span></div>
	<div class="arrow-block"><span class="arrow-l-brw-3"></span></div>
</div><!-- end one-half -->

<div class="one-third">
	<div class="arrow-block"><span class="arrow-grn-1"></span></div>
	<div class="arrow-block"><span class="arrow-grn-2"></span></div>
	<div class="arrow-block"><span class="arrow-grn-3"></span></div>
</div><!-- end one-half -->
</textarea></code>		




<h2>Pie Charts</h2>
<div class="two-third">
<div class="pc-block"><span class="pc-brw-1-8"></span></div>
<div class="pc-block"><span class="pc-brw-2-8"></span></div>
<div class="pc-block"><span class="pc-brw-3-8"></span></div>
<div class="pc-block"><span class="pc-brw-4-8"></span></div>
<div class="pc-block"><span class="pc-brw-5-8"></span></div>
<div class="pc-block"><span class="pc-brw-6-8"></span></div>
<div class="pc-block"><span class="pc-brw-7-8"></span></div>
</div><!-- end one-third -->
<div class="two-third">
<div class="pc-block"><span class="pc-grn-1-8"></span></div>
<div class="pc-block"><span class="pc-grn-2-8"></span></div>
<div class="pc-block"><span class="pc-grn-3-8"></span></div>
<div class="pc-block"><span class="pc-grn-4-8"></span></div>
<div class="pc-block"><span class="pc-grn-5-8"></span></div>
<div class="pc-block"><span class="pc-grn-6-8"></span></div>
<div class="pc-block"><span class="pc-grn-7-8"></span></div>
</div><!-- end one-third -->

<code><textarea style="height:350px;">
<div class="pc-block"><span class="pc-brw-1-8"></span></div>
<div class="pc-block"><span class="pc-brw-2-8"></span></div>
<div class="pc-block"><span class="pc-brw-3-8"></span></div>
<div class="pc-block"><span class="pc-brw-4-8"></span></div>
<div class="pc-block"><span class="pc-brw-5-8"></span></div>
<div class="pc-block"><span class="pc-brw-6-8"></span></div>
<div class="pc-block"><span class="pc-brw-7-8"></span></div>

<div class="pc-block"><span class="pc-grn-1-8"></span></div>
<div class="pc-block"><span class="pc-grn-2-8"></span></div>
<div class="pc-block"><span class="pc-grn-3-8"></span></div>
<div class="pc-block"><span class="pc-grn-4-8"></span></div>
<div class="pc-block"><span class="pc-grn-5-8"></span></div>
<div class="pc-block"><span class="pc-grn-6-8"></span></div>
<div class="pc-block"><span class="pc-grn-7-8"></span></div>
</textarea></code>



<h2>Info Box</h2>
<div class="info-box-green">
	<div class="i-icon-block"></div>
	<div class="text-block">
		<h2>Lorem Ipsum Dolor Sit Amit</h2>
		<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
	</div>
</div><!-- end info-box -->

<div class="info-box-brown">
	<div class="i-icon-block"></div>
	<div class="text-block">
		<h2>Lorem Ipsum Dolor Sit Amit</h2>
		<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
	</div>
</div><!-- end info-box -->

<code style="margin-top:50px; width:90%; "><textarea style="height:350px;">
<div class="info-box-green">
	<div class="i-icon-block"></div>
	<div class="text-block">
		<h2>Lorem Ipsum Dolor Sit Amit</h2>
		<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
	</div>
</div><!-- end info-box -->

<div class="info-box-brown">
	<div class="i-icon-block"></div>
	<div class="text-block">
		<h2>Lorem Ipsum Dolor Sit Amit</h2>
		<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
	</div>
</div><!-- end info-box -->
</textarea></code>



<h2>Circles</h2>
<div class="circle">
	<div class="circle-content"><p>Lorem Ipsum Dolor Sit Amit</p></div>
</div><!-- end circle -->
<div class="circle l-brown">
	<div class="circle-content"><p>Lorem Ipsum Dolor Sit Amit</p></div>
</div><!-- end circle -->
<div class="circle green">
	<div class="circle-content"><p>Lorem Ipsum Dolor Sit Amit Et Netus</p></div>
</div><!-- end circle -->

<code><textarea style="height:200px;">
<div class="circle">
	<div class="circle-content"><p>Lorem Ipsum Dolor Sit Amit</p></div>
</div><!-- end circle -->
<div class="circle l-brown">
	<div class="circle-content"><p>Lorem Ipsum Dolor Sit Amit</p></div>
</div><!-- end circle -->
<div class="circle green">
	<div class="circle-content"><p>Lorem Ipsum Dolor Sit Amit Et Netus</p></div>
</div><!-- end circle -->
</textarea></code>



<h2>CTA's</h2>
<a href=""><img src="/wp-content/uploads/2013/11/tour-the-lodge.png" /></a>
<a href=""><img src="/wp-content/uploads/2013/11/tour-the-lodge-trees.png" /></a>
<a href=""><img src="/wp-content/uploads/2013/11/tour-the-lodge-circle.png" /></a><br /><br /><br />

<a href=""><img src="/wp-content/uploads/2013/11/admissions-wood-sign.png" /></a>
<a href=""><img src="/wp-content/uploads/2013/11/admissions-trees.png" /></a>
<a href=""><img src="/wp-content/uploads/2013/11/admissions-light.png" /></a>
<a href=""><img src="/wp-content/uploads/2013/11/admissions-skyline.jpg" /></a>
<a href=""><img src="/wp-content/uploads/2013/11/admissions-pine.png" /></a>
<a href=""><img src="/wp-content/uploads/2013/11/admissions-round.png" /></a>
<br /><br /><br />

<a href=""><img src="/wp-content/uploads/2013/11/insurance-accepted.png" /></a><br /><br /><br />

<a href=""><img src="/wp-content/uploads/2013/11/why-works-green.png" /></a>
<a href=""><img src="/wp-content/uploads/2013/11/why-works-brown.png" /></a>
<a href=""><img src="/wp-content/uploads/2013/11/why-works-wood.png" /></a>
<br /><br /><br />


<code><textarea style="height:400px;">
<a href=""><img src="/wp-content/uploads/2013/11/tour-the-lodge.png" /></a>
<a href=""><img src="/wp-content/uploads/2013/11/tour-the-lodge-trees.png" /></a>
<a href=""><img src="/wp-content/uploads/2013/11/tour-the-lodge-circle.png" />


<a href=""><img src="/wp-content/uploads/2013/11/admissions-wood-sign.png" /></a>
<a href=""><img src="/wp-content/uploads/2013/11/admissions-trees.png" /></a>
<a href=""><img src="/wp-content/uploads/2013/11/admissions-light.png" /></a>
<a href=""><img src="/wp-content/uploads/2013/11/admissions-skyline.jpg" /></a>
<a href=""><img src="/wp-content/uploads/2013/11/admissions-pine.png" /></a>
<a href=""><img src="/wp-content/uploads/2013/11/admissions-round.png" /></a>


<a href=""><img src="/wp-content/uploads/2013/11/insurance-accepted.png" /></a><br /><br /><br />


<a href=""><img src="/wp-content/uploads/2013/11/why-works-green.png" /></a>
<a href=""><img src="/wp-content/uploads/2013/11/why-works-brown.png" /></a>
<a href=""><img src="/wp-content/uploads/2013/11/why-works-wood.png" /></a>

</textarea></code>
		



<?php endwhile; endif; ?>
</section>
</div><!-- end tier-content-block-->
</div> <!-- #main -->
<?php get_footer(); ?>
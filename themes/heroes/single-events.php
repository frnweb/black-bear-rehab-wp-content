<?php get_header(); ?>
<div id="main">
	<div id="events">
	
		<h1>News &amp; Events</h1>

		<?php while (have_posts()) : the_post(); $eventID = get_the_ID(); ?>
		<div class="event">
			<div class="content">
				<?php 
				if (has_post_thumbnail()): 
					the_post_thumbnail( array(250,250), array('class' => 'content-thumb') ); 
				else: 
					echo '<img class="attachment-250x250 wp-post-image content-thumb" width="250" height="250" alt="heroes-thumb" src="/wp-content/uploads/heroes-thumb.png">';
				endif; 
				?>				
				<?php
				$terms = wp_get_post_terms( get_the_ID(), 'event_category');
				$eventID = get_the_ID();
				?>
				<h1><a href="<?php the_permalink();?>"><?php the_title(); ?></a></h1>
				<?php if(get_post_meta($post->ID,'hir_event_day', true)) { ?>
					<p class="meta">Date of Event: <span><?php echo get_post_meta($post->ID,'hir_event_day', true); ?></span></p>			
				<?php } ?>
				<p class="meta" style="margin-bottom:15px;">News Type: <span><?php echo $terms[0]->name; ?></span></p>
				<?php the_content(); ?>
				<!--<p><a href="<?php the_permalink(); ?>">Read more</a></p>-->
			</div>
		</div>
		<?php endwhile; ?>
		
		<?php
		$args=array(
		  'post__not_in' => array($eventID),
		  'post_type' => 'events',
		  'post_status' => 'publish',
		  'posts_per_page' => 4,
		  'orderby' => 'date',
		  'order' => 'DESC'
		);		
		query_posts($args);?>
		<p class="more">Other News &amp Events</p>
		<section class="event-list">
			<?php while (have_posts()) : the_post(); ?>
			<div class="event-item clearfix">
				<aside>
					<div class="thumb">
						<?php 
						if (has_post_thumbnail()): 
							the_post_thumbnail( array(100,100) ); 
						else: 
							echo '<img class="attachment-250x250 wp-post-image" width="100" height="100" alt="heroes-thumb" src="/wp-content/uploads/heroes-thumb.png">';
						endif; 
						?>
					</div>
				</aside>
				<article>
					<div class="content">
						<?php
						$terms = wp_get_post_terms( get_the_ID(), 'event_category');
						?>
						<h1><a href="<?php the_permalink();?>"><?php the_title(); ?></a></h1>
						<?php if(get_post_meta($post->ID,'hir_event_day', true)) { ?>
						<p class="meta">Date of Event: <span><?php echo get_post_meta($post->ID,'hir_event_day', true); ?></span></p>			
					<?php } ?>
						<p class="meta">News Type: <span><?php echo $terms[0]->name; ?></span></p>
						<p class="excerpt"><?php echo get_post_meta($post->ID,'hir_event_excerpt', true); ?></p>
						<!--<p><a href="<?php the_permalink(); ?>">Read more</a></p>-->
					</div>
				</article>
			</div>
			<?php endwhile; ?>
		</section>

	</div>
	
	<div id="event-sidebar">
		<?php get_sidebar('1'); ?>
	</div>
</div>
<?php get_footer(); ?>
<?php get_header(); ?>

<div id="main">
	<div id="error_page">
		<div class="form-block">

		<h1 class="center">We Cannot Find That Page</h1>
		<p>&nbsp;</p>
		<p>We're sorry but there is something wrong with the link you clicked or we no longer have that page on the site.</p>

		<p>Please check the web address below for any characters that look incorrect, such as extra spaces or any combinations of punctuation or symbols (e.g. %20 or &#38;nbsp;) and remove them from the web address in the address bar above.</p>

		<p>Our coordinators are available 24 hours a day via phone. If the address still does not work, you can <a href="/help/">email us</a> or call (<strong><span id="frn_phones" ga_phone_location="Phone Clicks in 404 Page" style="white-space:nowrap;">888.371.5712</span></strong>) and we'll be glad to answer any questions you have .</p>

		<p style="margin-bottom:10px;"><a href="<?php echo "http://" . $_SERVER['HTTP_HOST']  . $_SERVER['REQUEST_URI']; ?>"><?php echo "http://" . $_SERVER['HTTP_HOST']  . $_SERVER['REQUEST_URI']; ?></a></p>

		<p>Thank you!</p>

		<p><?php bloginfo('name'); ?><br />
		<i>[Part of the <a href="http://www.foundationsrecoverynetwork.com" rel="nofollow">Foundations Recovery Network</a>]</i></p>


		</div>
	</div>
	<div id="page-sidebar" style="padding:15px 8px 0 8px;">
		<?php get_sidebar('3'); ?>
	</div>

</div>

<?php get_footer(); ?>
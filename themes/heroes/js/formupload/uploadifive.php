<?php
/*
UploadiFive
Copyright (c) 2012 Reactive Apps, Ronnie Garcia
*/

// Set the uplaod directory
if ($_POST["utype"]=='video') {
	$uploadDir = '/web_content/videos/';
	$fileTypes = array('mov', 'mp4', 'wmv', 'png'); /*********** REMOVE PNG *************/
}
else {
	$uploadDir = '/web_content/avatars/';
	$fileTypes = array('jpg', 'jpeg', 'gif', 'png');
}


$verifyToken = md5('unique_salt' . $_POST['timestamp']);

if (!empty($_FILES) && $_POST['token'] == $verifyToken) {
	$timestamp = time();
	$fileunique = md5('unique_salt' . $timestamp);
	$tempFile   = $_FILES['Filedata']['tmp_name'];
	$uploadDir  = $_SERVER['DOCUMENT_ROOT'] . $uploadDir;
	$targetFile = $uploadDir . $_POST['token'].'_'.$_FILES['Filedata']['name'];

	// Validate the filetype
	$fileParts = pathinfo($_FILES['Filedata']['name']);
	if (in_array(strtolower($fileParts['extension']), $fileTypes)) {

		// Save the file
		move_uploaded_file($tempFile, $targetFile);
		echo 1;

	} else {

		// The file type wasn't allowed
		echo 'Invalid file type.';

	}
}
?>
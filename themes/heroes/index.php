<?php get_header(); 
/* global $cfs; */
//wp_reset_query(); 
//query_posts( 'posts_per_page=5' );
query_posts('posts_per_page=5&paged='.$paged);
?>

<div id="main">
	<div class="twocolumns twocolumns-2">
		<div id="content">
			<div class="two-cols">
				<div class="post-col">
				<?php if (have_posts()) : ?>
					<?php while (have_posts()) : the_post(); ?>
						<div class="post">
							<div class="author-box">
								<?php $user_info = get_userdata($post->post_author); ?>
								<!--<a href="<?php echo $user_info-> user_url; ?>" class="img-box">--><?php echo get_avatar($post->post_author, 102); ?><!--</a>-->
								
								<div class="about">
									<p><!--<a href="<?php echo $user_info-> user_url; ?>">--><?php echo $user_info->user_firstname . ' ' . $user_info->user_lastname; ?><!--</a>--></p>
									<!--<a href="<?php echo $user_info-> user_url; ?>" class="link-more">More About <?php echo $user_info->user_firstname; ?> &gt;</a>-->
								</div>
							</div>
							<div class="post-content">
								<h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
								<?php if(get_post_meta(get_the_ID(), 'subtitle', true)){ ?><h3><?php echo get_post_meta(get_the_ID(), 'subtitle', true);?></h3><?php } ?>
								<span class="info">Posted on <strong><?php the_time('F jS'); ?></strong> by <strong><?php echo get_the_author(); ?></strong> | currently <?php comments_popup_link('No comments', '1 comment', '% comments'); ?></span>
								<?php //$content = get_the_content(); $content = substr($content, 0, 500); echo $content; ?>
								<? /*<?php echo $cfs->get('excerpt', get_the_ID()); ?> (<a href="<?php the_permalink(); ?>">more</a>) */ ?>
								<?php the_excerpt();?>
								<div class="comments" style="display:none;">
									<span class="number"><?php comments_popup_link('Comments (0)', 'Comments (1)', 'Comments (%)'); ?> </span>
									<?php //the_tags('<strong>Tags:</strong><ul class="tag-list"><li>', ',</li><li>', '</li></ul>'); ?>
								</div>
								<div class="social-box" style="display:none;">
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=721022864576316";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
									<ul class="social-plagin">
										<li><a href="<?php the_permalink()?>#respond"><img src="<?php bloginfo('template_url'); ?>/images/btn-comment.gif" width="98" height="23" alt="image description" /></a></li>
										<li><a href="http://twitter.com/share" class="twitter-share-button" data-count="none">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script></li>
										<li><div class="fb-share-button" data-href="<?php the_permalink()?>">" data-type="button"></div></li>
									</ul>
									<div class="rating-box">
										<span>Rate:</span>
									</div>
									<?php if(function_exists('the_ratings')) { the_ratings(); } ?>
								</div>
							</div>
						</div>
					<?php endwhile; ?>
					
				<?php else: ?>
					<h2>Not Found</h2>
					<p>Sorry, but you are looking for something that isn't here.</p>
				<?php endif; ?>
				</div>
			</div>
			<div class="navigation ev2">
						<?php ranklab_pagination();?>
					</div>
		</div>
		<?php get_sidebar('3'); ?>
	</div>
</div>

<?php get_footer(); ?>

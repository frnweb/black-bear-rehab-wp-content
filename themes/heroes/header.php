<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
	<head profile="http://gmpg.org/xfn/11">
		<title><?php 
			wp_title('');
			?></title>
		<meta property="fb:app_id" content="721022864576316" />
		<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
		<?php //commented out since plugin uses video thumbnails for images
		if($hide!="no") { ?>
		<meta property="og:image" content="http://heroesinrecovery.com/wp-content/uploads/break-the-stigma-200-og.jpg" />
		<meta property="og:title" content="Heroes in Recovery" />
		<meta property="og:description" content="Help break the stigma associated with addiction and share your story!" />
		<meta property="og:site_name" content="Heroes in Recovery" />
		<?php } ?>

		<?php wp_head(); ?>		

		
		<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_url'); ?>/all.css" />
		<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_url'); ?>/style.css" />
		<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_url'); ?>/opacity.css" />
		<link href="http://fonts.googleapis.com/css?family=EB+Garamond" rel="stylesheet" type="text/css" />
		<?php //if(is_home()) echo '<meta name="og:image" content="http://www.heroesinrecovery.com/wp-content/uploads/featured-banner-share2.jpg">'; ?>

		<?php if(is_singular() && !is_singular( 'stories' ) && get_the_author_meta( 'ID' )!=28) { 
			$dom = new DOMDocument;
			$dom->loadHTML(get_avatar($post->post_author, 250, 'none', "test"));
			$img = $dom->getElementsByTagName('img')->item(0);
			$img_url = $img->getAttribute('src'); 
			
			?><meta property="og:image" content="<?=$img_url; ?>" />
		<?php }
		else { ?>
		<meta name="og:image" content="http://www.heroesinrecovery.com/wp-content/uploads/featured-banner-share2.jpg">
		<?php }

		if ( is_singular() ) wp_enqueue_script( 'theme-comment-reply', get_bloginfo('template_url')."/js/comment-reply.js" ); ?>
		<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/input-type-file.js"></script>
		<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/custom.js"></script>
		<? /* <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/image-rotator.js"></script> */ ?>
		<!--[if lt IE 7]>
			<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/ie.css" media="all" />
			<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/ie-hover-pack.js"></script>
		<![endif]-->
		<script type="text/javascript" src="/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
		<script type="text/javascript" src="/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
		<link rel="stylesheet" type="text/css" href="/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
		<script type="text/javascript">
			jQuery(document).ready(function() {
				jQuery("a.hp-video").fancybox({
					'transitionIn'	:	'elastic',
					'transitionOut'	:	'elastic',
					'speedIn'		:	600, 
					'speedOut'		:	470, 
					'overlayShow'	:	false
				});
			});
		</script>
		<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.cycle.all.js"></script>
		<?php

			//http://malsup.github.com/jquery.cycle.all.js

			global $current_user;
			get_currentuserinfo();
		?>  
		<script type="text/javascript">                           
			jQuery("#submitbtn").click(function() {  
				jQuery('#result').html('<img src="<?php bloginfo('template_url'); ?>/images/loader.gif" class="loader" />').fadeIn();  
				var input_data = $('#wp_login_form').serialize();  
				jQuery.ajax({  
				type: "POST",  
				url:  "<?php echo "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>",  
				data: input_data,  
				success: function(msg){  
					jQuery('.loader').remove();  
					jQuery('<div>').html(msg).appendTo('div#result').hide().fadeIn('slow');  
				}  
				});  
				return false; 
			});  
			jQuery(document).ready(function() {
				jQuery('a.share-main').hover(
					function() {
						jQuery('.share-drop').show();
					},
					function() {
						if (jQuery('.share-drop').is(':visible')) {
							setTimeout(function() {  
								if (navigator.appName == 'Microsoft Internet Explorer') {
									if (jQuery('.share-drop:hover').length == 0) { jQuery('.share-drop').hide(); }
								}
								else {
									if (!jQuery('.share-drop').is(':hover')) { jQuery('.share-drop').hide(); }
								}
							}, 50);
						}
					}
				);
			});
		</script>
		
		<?php if (is_page('4450')) {?>
			<!-- Facebook Conversion Code for Heroes Registration NYC -->
			<script>(function() {
			var _fbq = window._fbq || (window._fbq = []);
			if (!_fbq.loaded) {
			var fbds = document.createElement('script');
			fbds.async = true;
			fbds.src = '//connect.facebook.net/en_US/fbds.js';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(fbds, s);
			_fbq.loaded = true;
			}
			})();
			window._fbq = window._fbq || [];
			window._fbq.push(['track', '6015295985499', {'value':'0.01','currency':'USD'}]);
			</script>
			<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6015295985499&amp;cd[value]=0.01&amp;cd[currency]=USD&amp;noscript=1" /></noscript>
	
		<?php } ?>


	</head>
	
	<body>
		<div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=721022864576316";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
		<!--wrapper-->
		<div id="wrapper">
			<div class="wrapper-holder">
				<div class="wrapper-frame">
					<div class="w1">
						<div id="header">
							<h1 class="logo"><a href="<?php bloginfo('url'); ?>"><?php bloginfo('name'); ?></a></h1>
							<!--<?php get_search_form(); ?>-->
							<div class="section">
								<? /*
								<?php $id = 11; $logo_stories = $wpdb->get_results( $wpdb->prepare( "SELECT cfs.post_id, pm.meta_value FROM wp_cfs_values cfs INNER JOIN wp_postmeta pm ON cfs.meta_id = pm.meta_id WHERE cfs.field_id = %d AND (pm.meta_value <> 'None' AND pm.meta_value <> '') ORDER BY pm.meta_id DESC;", $id ) ); ?>
							
								<?php
								function sortary($a, $b) { 
									$letters = array('H', 'E', 'R', 'O', 'EE', 'S'); 
									return array_search($a->meta_value, $letters) - array_search($b->meta_value, $letters); 
								}
								usort($logo_stories, "sortary");
								?>
								
								<ul class="heroes-nav">
									<? foreach ($logo_stories as $letter): ?>
									<li<?=is_page($letter->post_id) ? ' class="active"' : '';?>><a href="<?php echo get_permalink($letter->post_id); ?>" class="btn-<?php echo strtolower($letter->meta_value); ?>"><?php echo $letter->meta_value; ?></a></li>
									<? endforeach; ?>
								</ul>
								*/ ?>
								<ul class="heroes-nav">
								<?php  
								$hir_query1 = new WP_Query( 'post_type=stories&meta_value=h&posts_per_page=1');
								while ( $hir_query1->have_posts() ) : $hir_query1->the_post();
									echo '<li><a href="'.get_permalink().'" class="btn-h">H</a></li>';
								endwhile; wp_reset_postdata();
								$hir_query2 = new WP_Query( 'post_type=stories&meta_value=e&posts_per_page=1');
								while ( $hir_query2->have_posts() ) : $hir_query2->the_post();  
									echo '<li><a href="'.get_permalink().'" class="btn-e">E</a></li>';
								endwhile; wp_reset_postdata();
								$hir_query3 = new WP_Query( 'post_type=stories&meta_value=r&posts_per_page=1');
								while ( $hir_query3->have_posts() ) : $hir_query3->the_post();  
									echo '<li><a href="'.get_permalink().'" class="btn-r">R</a></li>';
								endwhile; wp_reset_postdata();
								$hir_query4 = new WP_Query( 'post_type=stories&meta_value=o&posts_per_page=1');
								while ( $hir_query4->have_posts() ) : $hir_query4->the_post();  
									echo '<li><a href="'.get_permalink().'" class="btn-o">O</a></li>';
								endwhile; wp_reset_postdata();
								$hir_query5 = new WP_Query( 'post_type=stories&meta_value=ee&posts_per_page=1');
								while ( $hir_query5->have_posts() ) : $hir_query5->the_post();  
									echo '<li><a href="'.get_permalink().'" class="btn-ee">E</a></li>';
								endwhile; wp_reset_postdata();
								$hir_query6 = new WP_Query( 'post_type=stories&meta_value=s&posts_per_page=1');
								while ( $hir_query6->have_posts() ) : $hir_query6->the_post();  
									echo '<li><a href="'.get_permalink().'" class="btn-s">S</a></li>';
								endwhile; wp_reset_postdata(); 
								?>
								</ul>
							</div>
							<div class="panel">
								<ul class="social">
									<li><a href="https://www.facebook.com/HeroesinRecovery" target="_blank" class="btn-facebook" title="Follow us on Facebook" data-exticon="no" >facebook</a></li>
									<li><a href="<?php bloginfo('rss2_url'); ?>" class="btn-rss" data-exticon="no">rss</a></li>
									<li><a href="http://twitter.com/#!/HeroesNRecovery" class="btn-twitter" target="_blank" data-exticon="no">twitter</a></li>
									<li><a href="http://www.youtube.com/user/heroesinrecovery" target="_blank" class="btn-youtube" title="Follow us on YouTube" data-exticon="no">youtube</a></li>
								</ul>
								<strong class="txt-recovery">IN RECOVERY tm</strong>
								<div class="story-count">
									<span><?php $stories = wp_count_posts('stories'); echo $stories->publish; ?></span> stories and growing
								</div>

							</div>
							<?php 
							wp_nav_menu(array(
								'theme_location' => 'main',
								'container' => false,
								'menu_id' => 'nav',
								'menu_class' => '',
								'walker' => new Custom_Walker_Nav_Menu
							));
							?>
							<div class="share-it">
								<a class="share-main" href="/share/" title="Share a Story"></a>
							</div>
						</div>
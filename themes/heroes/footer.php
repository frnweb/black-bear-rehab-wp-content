						<div id="footer">
							<ul class="item-list">
								<li>&copy; <?php print date('Y'); ?> Heroes In Recovery</li>
								<li>Encouraged by <?php bloginfo('name'); ?></li>
								<li><?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Footer"]'); ?></li>
								<!--
								<li><a href="#">Site Map</a></li>
								
								<li><a href="<?php echo get_permalink(TermsofUsePageID); ?>">Terms of Use</a></li>
								<li><a href="<?php echo do_shortcode('[frn_privacy_url]'); ?><?php //old: echo get_permalink(PrivacyPolicyPageID); ?>">Privacy Policy</a></li>
								-->
							</ul>
							<?php 
							wp_nav_menu(array(
								'theme_location' => 'footer',
								'container' => false,
								'menu_class' => 'nav'
							));
							?>
							<ul class="nav" style="padding-top:20px;">
								<li><a href="<?php echo do_shortcode('[frn_privacy_url]'); ?>" target="_blank">Privacy Policy</a></li>
								<li><a href="/web_content/privacy_guide.pdf" target="_blank">Privacy Guide</a></li>
								<li><a href="/web_content/terms_of_use.pdf" target="_blank">Terms of Use</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>

		<?php wp_footer(); ?>

	</body>
</html>
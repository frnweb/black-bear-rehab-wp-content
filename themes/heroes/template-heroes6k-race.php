<?php 
/* Template Name: Heroes 6k Race */
get_header(); 
global $cfs;
?>
<div id="main" class="heroes6k clearfix">
	<?php while (have_posts()) : the_post(); ?>
	<div id="content6k" class="clearfix">
		<h1><?php the_title(); ?></h1>
		<?php 
			// check if the post has a Post Thumbnail assigned to it.
			if ( has_post_thumbnail() ) {
		?>
		<div id="headerimg">
				<?php the_post_thumbnail('full'); ?>
		</div>
		<?php 
		} 
		?>
		<div class="registration">
			<?php echo get_post_meta($post->ID,'hir_6k_registration', true);?>
		</div>
		<div class="racedetails">
			<strong><?php echo get_post_meta($post->ID,'hir_6k_race_name', true);?></strong><br />
			<strong><?php echo get_post_meta($post->ID,'hir_6k_date', true);?></strong><br />
			<strong><?php echo get_post_meta($post->ID,'hir_6k_race_time', true);?></strong><br />
			<?php echo get_post_meta($post->ID,'hir_6k_race_address', true);?><br />
			<?php echo get_post_meta($post->ID,'hir_6k_race_city', true);?>, <?php echo get_post_meta($post->ID,'hir_6k_race_state', true);?><br />
			<?php if(get_post_meta($post->ID,'hir_6k_map', true)) { ?>
				<?php
				$images = rwmb_meta( 'hir_6k_map', 'type=image' );
				foreach ( $images as $image )
				{
				    echo "<a href='{$image['url']}' target='_blank'>View Map</a>";
				}
				?>			
			<?php } ?>
			<br />
			<?php if(get_post_meta($post->ID,'hir_6k_race_results', true)) { ?>
				<br /><strong>Race Results</strong><br />
				<?php echo get_post_meta($post->ID,'hir_6k_race_results', true);?>	
			<?php } ?>
		</div>
		<?php the_content(); ?>
	</div>
	<?php endwhile; ?>
	
	<div id="sidebar6k">
		<?php if ( function_exists( 'ADDTOANY_SHARE_SAVE_KIT' ) ) { 
			echo '
			<div class="clearfix" id="raceLocationsContainer">
				<h2 style="margin-top:0;padding-top:0; margin-bottom: 5px; float: left;">Spread the Word...</h2>';
				ADDTOANY_SHARE_SAVE_KIT(); 
			echo '
			</div>
			';
		} else { ?>
		<div class="clearfix" id="socialContainer">
			<div class="social-media">
			<a href="https://www.facebook.com/HeroesinRecovery">
			<img src="/wp-content/themes/heroes/images/icon-facebook.png" />
			</a>
			<a href="http://twitter.com/#!/HeroesNRecovery">
			<img src="/wp-content/themes/heroes/images/icon-twitter.png" />
			</a>
			<a href="http://www.youtube.com/user/foundationsrnetwork">
			<img src="/wp-content/themes/heroes/images/icon-youtube.png" />
			</a>
			<a href="/feed/">
			<img src="/wp-content/themes/heroes/images/icon-rss.png" />
			</a>
			</div>
		</div>
		<?php } ?>
		<div id="raceLocationsContainer">
			<h2>Race Locations</h2>
			<ul>
			<?php 
			$pages = get_pages('title_li=&child_of=2216&echo=0');
			foreach($pages as $page)
			{ ?>
				<li id="pages"><a href="<?php echo get_page_link($page->ID) ?>"><?php echo get_post_meta($page->ID,'hir_6k_race_city', true);?>, <?php echo get_post_meta($page->ID,'hir_6k_race_state', true);?></a></li>
			<?php } ?>
			</ul>
		</div>
		<?php if ( function_exists('dynamic_sidebar') ) { dynamic_sidebar('Races Sidebar'); } ?>
	</div>
	<div style="clear:both;"></div>
</div>

<?php get_footer(); ?>
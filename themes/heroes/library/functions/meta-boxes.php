<?php
/**
* Registering meta boxes
*
* All the definitions of meta boxes are listed below with comments.
* Please read them CAREFULLY.
*
* You also should read the changelog to know what has been changed before updating.
*
* For more information, please visit:
* @link http://www.deluxeblogtips.com/meta-box/
*/


add_filter( 'rwmb_meta_boxes', 'hir_register_meta_boxes' );

/**
* Register meta boxes
*
* @return void
*/
function hir_register_meta_boxes( $meta_boxes )
{
/**
* Prefix of meta keys (optional)
* Use underscore (_) at the beginning to make keys hidden
* Alt.: You also can make prefix empty to disable it
*/
// Better has an underscore as last sign
$prefix = 'hir_';

// 1st meta box
$meta_boxes[] = array(
// Meta box id, UNIQUE per meta box. Optional since 4.1.5
'id' => 'standard',

// Meta box title - Will appear at the drag and drop handle bar. Required.
'title' => __( '6K Fields', 'rwmb' ),

// Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
'pages' => array( 'page' ),

// Where the meta box appear: normal (default), advanced, side. Optional.
'context' => 'normal',

// Order of meta box: high (default), low. Optional.
'priority' => 'low',

// Auto save: true, false (default). Optional.
'autosave' => false,

// List of meta fields
'fields' => array(
// Registration
array(
'name' => __( 'Registration'),
'desc' => __( 'Registration description' ),
'id' => "{$prefix}6k_registration",
'type' => 'wysiwyg',
// Set the 'raw' parameter to TRUE to prevent data being passed through wpautop() on save
'raw' => false,
),
// Race Name
array(
// Field name - Will be used as label
'name' => __( 'Race Name'),
// Field ID, i.e. the meta key
'id' => "{$prefix}6k_race_name",
// Field description (optional)
'type' => 'textarea',
),
// Race DATE
array(
'name' => __( 'Race Date' ),
'id' => "{$prefix}6k_date",
'type' => 'date',

// jQuery date picker options. See here http://api.jqueryui.com/datepicker
'js_options' => array(
'appendText' => __( '(yyyy-mm-dd)', 'rwmb' ),
'dateFormat' => __( 'yy-mm-dd', 'rwmb' ),
'changeMonth' => true,
'changeYear' => true,
'showButtonPanel' => true,
),
),

// Race Time
array(
// Field name - Will be used as label
'name' => __( 'Race Time'),
// Field ID, i.e. the metakey
'id' => "{$prefix}6k_race_time",
// Field description (optional)
'type' => 'text',
),

// Race Address
array(
// Field name - Will be used as label
'name' => __( 'Race Address'),
// Field ID, i.e. the meta key
'id' => "{$prefix}6k_race_address",
// Field description (optional)
'type' => 'textarea',
),
// Race City
array(
// Field name - Will be used as label
'name' => __( 'Race City'),
// Field ID, i.e. the meta key
'id' => "{$prefix}6k_race_city",
// Field description (optional)
'type' => 'text',
),
// Race State
array(
// Field name - Will be used as label
'name' => __( 'Race State'),
// Field ID, i.e. the metakey
'id' => "{$prefix}6k_race_state",
// Field description (optional)
'type' => 'text',
),
// View Map
array(
'name' => __( 'View Map' ),
'id' => "{$prefix}6k_map",
'type' => 'file',
),
// Race Results
array(
'name' => __( 'Race Results'),
'id' => "{$prefix}6k_race_results",
'type' => 'wysiwyg',
// Set the 'raw' parameter to TRUE to prevent data being passed through wpautop() on save
'raw' => false,
),

)
);


// 2nd meta box
$meta_boxes[] = array(
// Meta box id, UNIQUE per meta box. Optional since 4.1.5
'id' => 'standard',

// Meta box title - Will appear at the drag and drop handle bar. Required.
'title' => __( 'Slider Options'),

// Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
'pages' => array( 'featured' ),

// Where the meta box appear: normal (default), advanced, side. Optional.
'context' => 'normal',

// Order of meta box: high (default), low. Optional.
'priority' => 'low',

// Auto save: true, false (default). Optional.
'autosave' => false,

// List of meta fields
'fields' => array(
// Link URL
array(
// Field name - Will be used as label
'name' => __( 'Link URL'),
// Field ID, i.e. the meta key
'id' => "{$prefix}link_url",
// Field description (optional)
'type' => 'textarea',
),

)
);

// 3nd meta box
$meta_boxes[] = array(
// Meta box id, UNIQUE per meta box. Optional since 4.1.5
'id' => 'standard',

// Meta box title - Will appear at the drag and drop handle bar. Required.
'title' => __( 'Story Options'),

// Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
'pages' => array( 'stories' ),

// Where the meta box appear: normal (default), advanced, side. Optional.
'context' => 'normal',

// Order of meta box: high (default), low. Optional.
'priority' => 'low',

// Auto save: true, false (default). Optional.
'autosave' => false,

// List of meta fields
'fields' => array(

// Hero Name
array(
// Field name - Will be used as label
'name' => __( 'Hero Name'),
// Field ID, i.e. the meta key
'id' => "{$prefix}hero_story_name",
// Field description (optional)
'type' => 'text',
),

// Hero Featured Image
array(
// Field name - Will be used as label
'name' => __( 'Fetured Image URL'),
// Field ID, i.e. the meta key
'id' => "{$prefix}hero_featured_image",
// Field description (optional)
'type' => 'textarea',
),

// Hero Excerpt
array(
// Field name - Will be used as label
'name' => __( 'Excerpt'),
// Field ID, i.e. the meta key
'id' => "{$prefix}hero_excerpt",
// Field description (optional)
'type' => 'textarea',
),

// Heroic Words
array(
'name' => __( 'Heroic Words'),
'id' => "{$prefix}heroic_words",
'type' => 'wysiwyg',
// Set the 'raw' parameter to TRUE to prevent data being passed through wpautop() on save
'raw' => false,
),

// Submitted By
array(
// Field name - Will be used as label
'name' => __( 'Submitted By'),
// Field ID, i.e. the meta key
'id' => "{$prefix}hero_submitted_by",
// Field description (optional)
'type' => 'text',
),

// Featured Story
array(
'name' => __( 'Set as featured story' ),
'id' => "{$prefix}hero_featured_story",
'type' => 'checkbox',
// Value can be 0 or 1
'std' => 0,
),

)
);


// 4th meta box
$meta_boxes[] = array(
// Meta box id, UNIQUE per meta box. Optional since 4.1.5
'id' => 'heroes-behind',

// Meta box title - Will appear at the drag and drop handle bar. Required.
'title' => __( 'Heroes Behind The Logo'),

// Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
'pages' => array( 'stories' ),

// Where the meta box appear: normal (default), advanced, side. Optional.
'context' => 'normal',

// Order of meta box: high (default), low. Optional.
'priority' => 'low',

// Auto save: true, false (default). Optional.
'autosave' => false,

// List of meta fields
'fields' => array(

// Logo Letter
array(
'name' => __( 'Logo Letter' ),
'id' => "{$prefix}logo_letter",
'type' => 'select',
// Array of 'value' => 'Label' pairs for select box
'options' => array(
'value1' => __( 'None' ),
'value2' => __( 'H' ),
'value3' => __( 'E' ),
'value4' => __( 'R' ),
'value5' => __( 'O' ),
'value6' => __( 'EE' ),
'value7' => __( 'S' ),
),
),

// Left Tagline
array(
// Field name - Will be used as label
'name' => __( 'Left Box Tagline'),
// Field ID, i.e. the meta key
'id' => "{$prefix}hero_left_box_tagline",
// Field description (optional)
'type' => 'textarea',
),

// Right Tagline
array(
// Field name - Will be used as label
'name' => __( 'Right Box Tagline'),
// Field ID, i.e. the meta key
'id' => "{$prefix}hero_right_box_tagline",
// Field description (optional)
'type' => 'textarea',
),

// Heroes Name
array(
// Field name - Will be used as label
'name' => __( 'Heroes Name'),
// Field ID, i.e. the meta key
'id' => "{$prefix}hero_name",
// Field description (optional)
'type' => 'text',
),


)
);


// 5th meta box
$meta_boxes[] = array(
// Meta box id, UNIQUE per meta box. Optional since 4.1.5
'id' => 'heroes-behind',

// Meta box title - Will appear at the drag and drop handle bar. Required.
'title' => __( 'Event Options'),

// Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
'pages' => array( 'events' ),

// Where the meta box appear: normal (default), advanced, side. Optional.
'context' => 'normal',

// Order of meta box: high (default), low. Optional.
'priority' => 'low',

// Auto save: true, false (default). Optional.
'autosave' => false,

// List of meta fields
'fields' => array(

// Day of Event
array(
// Field name - Will be used as label
'name' => __( 'Day Of Event'),
// Field ID, i.e. the meta key
'id' => "{$prefix}event_day",
// Field description (optional)
'type' => 'text',
),

// Featured
array(
'name' => __( 'Set as featured' ),
'id' => "{$prefix}event_featured",
'type' => 'checkbox',
// Value can be 0 or 1
'std' => 0,
),

// Thumbnail
array(
'name' => __( 'Thumbnail - Must be 75x75 pixels' ),
'id' => "{$prefix}event_image",
'type' => 'image',
),

// Event Excerpt
array(
// Field name - Will be used as label
'name' => __( 'Excerpt'),
// Field ID, i.e. the meta key
'id' => "{$prefix}event_excerpt",
// Field description (optional)
'type' => 'textarea',
),

)
);

return $meta_boxes;
}
<?php
/*
Template Name: Resources
*/
?>

<?php get_header(); ?>

<div id="main" >
	<div align="center">
	<div style="font-family: rockwell; font-size: 90px; color:#bb962e; padding: 20px 0 20px 0;">SPREAD THE MISSION</div><br />
	<div style="font-family:Georgia, 'Times New Roman', Times, serif; font-size: 24px; color:#a2a2a2; padding: 0 0 40px 0;"><strong><em>Here are some tools to help you spread the mission of Heroes in Recovery</em></strong></div>
    </div>

<table align="center" width="900px" style="margin-left: auto;
margin-right: auto; border-style: hidden;">
	<tr>
		<td align="center" style="padding: 0 20px 20px 20px;">
        <div style="text-align: center; font-family: rockwell; font-size: 30px; color:#333333; padding-bottom: 10px;">WEAR THE BADGE</div>
        <div align="center"><a href="http://www.heroesinrecovery.com/wp-content/uploads/heroes-in-recovery-resource-i-shared-my-story.jpg" target="new"><img src="http://www.heroesinrecovery.com/wp-content/uploads/heroes-in-recovery-resource-badge.png" onmouseover="this.src='http://www.heroesinrecovery.com/wp-content/uploads/heroes-in-recovery-resource-badge-h.png'" onmouseout="this.src='http://www.heroesinrecovery.com/wp-content/uploads/heroes-in-recovery-resource-badge.png'" /></a></div>

<div align="center">

<a href="#" 
  onclick="
    window.open(
      'https://www.facebook.com/sharer/sharer.php?u=heroesinrecovery.com/wp-content/uploads/heroes-in-recovery-resource-i-shared-my-story.jpg', 
      'facebook-share-dialog', 
      'width=626,height=436'); 
    return false;">
  <img src="http://www.heroesinrecovery.com/wp-content/uploads/facebook-resource.png" />
</a>


<a href="//pinterest.com/pin/create/button/?url=http%3A%2F%2Fwww.heroesinrecovery.com&media=http%3A%2F%2Fwww.heroesinrecovery.com%2Fwp-content%2Fuploads%2Fheroes-in-recovery-resource-i-shared-my-story.jpg&description=I%20shared%20my%20story%20with%20Heroes%20in%20Recovery...to%20help%20eliminate%20the%20social%20stigma%20that%20keeps%20individuals%20with%20addiction%20and%20mental%20health%20issues%20from%20seeking%20help." data-pin-do="buttonPin" data-pin-config="none"><img src="http://www.heroesinrecovery.com/wp-content/uploads/pinterest-resource.png" /></a>


<a href="http://twitter.com/share?url=http%3A%2F%2Ft.co%2FftrHoF7n1x&text=I%20Shared%20My%20Story!&via=heroesnrecovery" target="new"><img src="http://www.heroesinrecovery.com/wp-content/uploads/twitter-resource.png" /></a>
</div>
        
        <div style="text-align: center;font-family:Georgia, 'Times New Roman', Times, serif; font-size: 16px;"><em>
        If you’ve already shared your story, let<br />
        the world know it! Download this badge <br />
        and share it on Facebook, Twitter, <br />
        Instagram, & Pinterest. </em></div>
        </td>
        <td align="center" style="padding: 0 20px 20px 20px;">
        <div style="text-align: center;font-family: rockwell; font-size: 30px; color:#333333;padding-bottom: 10px;">SHOW THEM HOW</div>
        <div align="center"><a href="http://www.heroesinrecovery.com/wp-content/uploads/heroes-in-recovery-resource-share-your-story.jpg" target="new"><img src="http://www.heroesinrecovery.com/wp-content/uploads/heroes-in-recovery-resource-share.png" onmouseover="this.src='http://www.heroesinrecovery.com/wp-content/uploads/heroes-in-recovery-resource-share-h.png'" onmouseout="this.src='http://www.heroesinrecovery.com/wp-content/uploads/heroes-in-recovery-resource-share.png'" /></a></div>
        
        <div align="center">
        
        <a href="#" 
  onclick="
    window.open(
      'https://www.facebook.com/sharer/sharer.php?u=heroesinrecovery.com/wp-content/uploads/heroes-in-recovery-resource-share-your-story.jpg', 
      'facebook-share-dialog', 
      'width=626,height=436'); 
    return false;">
  <img src="http://www.heroesinrecovery.com/wp-content/uploads/facebook-resource.png" />
</a>
        
        
        <a href="//pinterest.com/pin/create/button/?url=http%3A%2F%2Fwww.heroesinrecovery.com%2F&media=http%3A%2F%2Fwww.heroesinrecovery.com%2Fwp-content%2Fuploads%2Fheroes-in-recovery-resource-share-your-story.jpg&description=Heroes%20in%20Recovery%20is%20here%20to%20help%20eliminate%20the%20social%20stigma%20that%20keeps%20individuals%20with%20addiction%20and%20mental%20health%20issues%20from%20seeking%20help%2C%20and%20does%20so%20by%20sharing%20stories%20of%20real%20journeys.%20%20This%20is%20how%20you%20can%20share%20your%20story." data-pin-do="buttonPin" data-pin-config="none"><img src="http://www.heroesinrecovery.com/wp-content/uploads/pinterest-resource.png" /></a>
        
        
         <a href="http://twitter.com/share?url=http%3A%2F%2Ft.co%2FAsNG2uemYY&text=Learn%20How%20To%20Share%20Your%20Story%20With%20This%20Infographic&via=heroesnrecovery" target="new"><img src="http://www.heroesinrecovery.com/wp-content/uploads/twitter-resource.png" /></a>
        
        
        
        
        </div>
        
        <div style="text-align: center;font-family:Georgia, 'Times New Roman', Times, serif; font-size: 16px;"><em>Tell your friends how they can break the <br />stigma! Share this infographic and show<br /> them how easy it is to share their story<br /> and change a life!</em></div>
        </td>
    </tr>
    <tr>
		<td align="center" style="padding: 0 20px 20px 20px;">
        <div style="text-align: center;font-family: rockwell; font-size: 30px; color:#333333;padding-bottom: 10px; padding-top: 25px;">GET IN SHAPE</div>
        <div align="center"><a href="http://www.heroesinrecovery.com/wp-content/uploads/heroes-in-recovery-resource-6k-training.jpg" target="new"><img src="http://www.heroesinrecovery.com/wp-content/uploads/heroes-in-recovery-resource-workout.png" onmouseover="this.src='http://www.heroesinrecovery.com/wp-content/uploads/heroes-in-recovery-resource-workout-h.png'" onmouseout="this.src='http://www.heroesinrecovery.com/wp-content/uploads/heroes-in-recovery-resource-workout.png'" /></a></div>
        
        <div align="center">
        
        <a href="#" 
  onclick="
    window.open(
      'https://www.facebook.com/sharer/sharer.php?u=heroesinrecovery.com/wp-content/uploads/heroes-in-recovery-resource-6k-training.jpg', 
      'facebook-share-dialog', 
      'width=626,height=436'); 
    return false;">
  <img src="http://www.heroesinrecovery.com/wp-content/uploads/facebook-resource.png" />
</a>
        
        
        <a href="//pinterest.com/pin/create/button/?url=http%3A%2F%2Fwww.heroes6k.com&media=http%3A%2F%2Fwww.heroesinrecovery.com%2Fwp-content%2Fuploads%2Fheroes-in-recovery-resource-6k-training.jpg&description=Get%20in%20shape%20for%20the%20next%20Heroes%206K%20and%20run%20the%20race%20to%20help%20eliminate%20the%20social%20stigma%20that%20keeps%20individuals%20with%20addiction%20and%20mental%20health%20issues%20from%20seeking%20help." data-pin-do="buttonPin" data-pin-config="none"><img src="http://www.heroesinrecovery.com/wp-content/uploads/pinterest-resource.png" /></a>
        
        
        <a href="http://twitter.com/share?url=http%3A%2F%2Ft.co%2FoUc7qWYym5&text=I'm%20Getting%20In%20Shape%20for%20the%20Heroes6K!&via=heroesnrecovery&hashtags=heroes6k" target="new"><img src="http://www.heroesinrecovery.com/wp-content/uploads/twitter-resource.png" /></a>
        
        </div>
       
        
        <div style="text-align: center;font-family:Georgia, 'Times New Roman', Times, serif; font-size: 16px;"><em>Get on those running shoes and get into<br /> shape! Download and share this 6k<br /> Training Guide Infographic and hit <br />the road for a great race!</em></div>
        </td>
        <td align="center" style="padding: 0 20px 20px 20px;">
        <div style="text-align: center;font-family: rockwell; font-size: 30px; color:#333333; padding-bottom: 10px; padding-top: 25px;">RUN THE RACE</div>
        <div align="center"><a href="http://www.heroesinrecovery.com/wp-content/uploads/heroes-in-recovery-resource-6k-flyer.pdf" target="new"><img src="http://www.heroesinrecovery.com/wp-content/uploads/heroes-in-recovery-resource-6k.png" onmouseover="this.src='http://www.heroesinrecovery.com/wp-content/uploads/heroes-in-recovery-resource-6k-h.png'" onmouseout="this.src='http://www.heroesinrecovery.com/wp-content/uploads/heroes-in-recovery-resource-6k.png'" /></a></div>
        
        <div align="center">
<a href="#" 
  onclick="
    window.open(
      'https://www.facebook.com/sharer/sharer.php?u=heroesinrecovery.com/wp-content/uploads/heroes-in-recovery-resource-6k-flyer.pdf', 
      'facebook-share-dialog', 
      'width=626,height=436'); 
    return false;">
  <img src="http://www.heroesinrecovery.com/wp-content/uploads/facebook-resource.png" />
</a>
        
        
        <a href="//pinterest.com/pin/create/button/?url=http%3A%2F%2Fwww.heroes6k.com&media=http%3A%2F%2Fwww.heroesinrecovery.com%2Fwp-content%2Fuploads%2Fheroes-in-recovery-resource-6k-flyer.pdf&description=The%20Heroes%206K%2C%20run%20the%20race%20to%20help%20eliminate%20the%20social%20stigma%20that%20keeps%20individuals%20with%20addiction%20and%20mental%20health%20issues%20from%20seeking%20help." data-pin-do="buttonPin" data-pin-config="none"><img src="http://www.heroesinrecovery.com/wp-content/uploads/pinterest-resource.png" /></a>
        
        
        <a href="http://twitter.com/share?url=http%3A%2F%2Ft.co%2Fpxpq1fGj9O&text=I'm%20Running%20The%20Heroes6K!&via=heroesnrecovery&hashtags=heroes6k" target="new"><img src="http://www.heroesinrecovery.com/wp-content/uploads/twitter-resource.png" /></a>
       
        
        </div>
        
        <div style="text-align: center;font-family:Georgia, 'Times New Roman', Times, serif; font-size: 16px;"><em>Are you running a Heroes 6K? Share<br /> this flyer online and get all your friends,<br /> no matter their location, to run the<br /> race and break the stigma!</em></div>
        </td>
    </tr>
    <tr>
		<td align="center" style="padding: 0 20px 20px 20px;">
        <div style="text-align: center;font-family: rockwell; font-size: 30px; color:#333333; padding-bottom: 10px; padding-top: 25px;">THE NUMBERS</div>
        <div align="center"><a href="http://www.heroesinrecovery.com/wp-content/uploads/heroes-in-recovery-resource-stigma-infographic.jpg" target="new"><img src="http://www.heroesinrecovery.com/wp-content/uploads/heroes-in-recovery-resource-numbers.png" onmouseover="this.src='http://www.heroesinrecovery.com/wp-content/uploads/heroes-in-recovery-resource-numbers-h.png'" onmouseout="this.src='http://www.heroesinrecovery.com/wp-content/uploads/heroes-in-recovery-resource-numbers.png'" /></a></div>
        
        <div align="center">
        <a href="#" 
  onclick="
    window.open(
      'https://www.facebook.com/sharer/sharer.php?u=heroesinrecovery.com/wp-content/uploads/heroes-in-recovery-resource-stigma-infographic.jpg', 
      'facebook-share-dialog', 
      'width=626,height=436'); 
    return false;">
  <img src="http://www.heroesinrecovery.com/wp-content/uploads/facebook-resource.png" />
</a>
        
        <a href="//pinterest.com/pin/create/button/?url=http%3A%2F%2Fwww.heroesinrecovery.com%2F&media=http%3A%2F%2Fwww.heroesinrecovery.com%2Fwp-content%2Fuploads%2Fheroes-in-recovery-resource-stigma-infographic.jpg&description=Heroes%20in%20Recovery%20is%20here%20to%20help%20eliminate%20the%20social%20stigma%20that%20keeps%20individuals%20with%20addiction%20and%20mental%20health%20issues%20from%20seeking%20help." data-pin-do="buttonPin" data-pin-config="none"><img src="http://www.heroesinrecovery.com/wp-content/uploads/pinterest-resource.png" /></a>
        
        <a href="http://twitter.com/share?url=http%3A%2F%2Ft.co%2FIZ0gNyhX5I&text=The%20Real%20Data%20On%20Addiction,%20Mental%20Health,%20and%20Recovery&via=heroesnrecovery" target="new"><img src="http://www.heroesinrecovery.com/wp-content/uploads/twitter-resource.png" /></a>
        
        </div>
        
        <div style="text-align: center;font-family:Georgia, 'Times New Roman', Times, serif; font-size: 16px;"><em>Want some serious data on addiction<br /> & recovery? Download and share this<br /> infographic on misconceptions of<br /> addiction treatment.</em></div>
        </td>
        <td align="center" style="padding: 0 20px 20px 20px;">
        <div style="text-align: center;font-family: rockwell; font-size: 30px; color:#333333; padding-bottom: 10px; padding-top: 25px;">DIY T-SHIRT</div>
        <div align="center"><a href="http://www.heroesinrecovery.com/wp-content/uploads/heroes-in-recovery-resource-diy-t-shirt.jpg" target="new"><img src="http://www.heroesinrecovery.com/wp-content/uploads/heroes-in-recovery-resource-shirt.png" onmouseover="this.src='http://www.heroesinrecovery.com/wp-content/uploads/heroes-in-recovery-resource-shirt-h.png'" onmouseout="this.src='http://www.heroesinrecovery.com/wp-content/uploads/heroes-in-recovery-resource-shirt.png'" /></a></div>
        <div align="center">
        
        <a href="#" 
  onclick="
    window.open(
      'https://www.facebook.com/sharer/sharer.php?u=heroesinrecovery.com/wp-content/uploads/heroes-in-recovery-resource-diy-t-shirt.jpg', 
      'facebook-share-dialog', 
      'width=626,height=436'); 
    return false;">
  <img src="http://www.heroesinrecovery.com/wp-content/uploads/facebook-resource.png" />
</a>
        
        
        
        <a href="//pinterest.com/pin/create/button/?url=http%3A%2F%2Fwww.heroesinrecovery.com%2F&media=http%3A%2F%2Fwww.heroesinrecovery.com%2Fwp-content%2Fuploads%2Fheroes-in-recovery-resource-diy-t-shirt.jpg&description=Heroes%20in%20Recovery%20is%20here%20to%20help%20eliminate%20the%20social%20stigma%20that%20keeps%20individuals%20with%20addiction%20and%20mental%20health%20issues%20from%20seeking%20help." data-pin-do="buttonPin" data-pin-config="none"><img src="http://www.heroesinrecovery.com/wp-content/uploads/pinterest-resource.png" /></a>
        
        <a href="http://twitter.com/share?url=http%3A%2F%2Ft.co%2FD7YttEjOvt&text=Make%20Your%20Own%20'Break%20The%20Stigma'%20T-Shirt%20With%20This%20Template&via=heroesnrecovery" target="new"><img src="http://www.heroesinrecovery.com/wp-content/uploads/twitter-resource.png" /></a>
        
        </div>
        <div style="text-align: center;font-family:Georgia, 'Times New Roman', Times, serif; font-size: 16px;"><em>Make your own t-shirt and rep<br /> Heroes in Recovery wherever you go! <br />Download this template, place it on<br /> a t-shirt, then add paint...voila!</em>        </div>
        </td>
    </tr>
    <tr>
		<td align="center" style="padding: 0 20px 20px 20px;">
       <div style="text-align: center;font-family: rockwell; font-size: 30px; color:#333333;padding-bottom: 10px; padding-top: 25px;">DIY STICKERS</div>
        <div align="center"><a href="http://www.heroesinrecovery.com/wp-content/uploads/round-sticker-template.doc" target="new"><img src="http://www.heroesinrecovery.com/wp-content/uploads/heroes-in-recovery-resource-stickers.png" onmouseover="this.src='http://www.heroesinrecovery.com/wp-content/uploads/heroes-in-recovery-resource-stickers-h.png'" onmouseout="this.src='http://www.heroesinrecovery.com/wp-content/uploads/heroes-in-recovery-resource-stickers.png'" /></a></div>
        <div align="center">
        
                <a href="#" 
  onclick="
    window.open(
      'https://www.facebook.com/sharer/sharer.php?u=heroesinrecovery.com/wp-content/uploads/heroes-in-recovery-resource-stickers.png', 
      'facebook-share-dialog', 
      'width=626,height=436'); 
    return false;">
  <img src="http://www.heroesinrecovery.com/wp-content/uploads/facebook-resource.png" />
</a>
        
        <a href="//pinterest.com/pin/create/button/?url=http%3A%2F%2Fwww.heroesinrecovery.com%2F&media=http%3A%2F%2Fwww.heroesinrecovery.com%2Fwp-content%2Fuploads%2Fround-sticker-template.jpg&description=Heroes%20in%20Recovery%20is%20here%20to%20help%20eliminate%20the%20social%20stigma%20that%20keeps%20individuals%20with%20addiction%20and%20mental%20health%20issues%20from%20seeking%20help%2C%20and%20does%20so%20by%20sharing%20stories%20of%20real%20journeys.%20%20This%20is%20how%20you%20can%20share%20your%20story.%20Print%20these%20stickers%20(using%20the%20Avery%205294%20template)%20and%20spread%20the%20mission." data-pin-do="buttonPin" data-pin-config="none"><img src="http://www.heroesinrecovery.com/wp-content/uploads/pinterest-resource.png" /></a>
        
        <a href="http://twitter.com/share?url=http%3A%2F%2Ft.co%2FCtKxiL8wxt&text=Make%20Your%20Own%20Heroes%20In%20Recvoery%20Stickers%20With%20This%20Template&via=heroesnrecovery" target="new"><img src="http://www.heroesinrecovery.com/wp-content/uploads/twitter-resource.png" /></a>
       
        </div>
        <div style="text-align: center;font-family:Georgia, 'Times New Roman', Times, serif; font-size: 16px;"><em>Take this template and print your own<br /> stickers.  Slap em’ on your bumper, <br />binder, or anything you can find!<br /> *Avery Template #5294</em></div>
        </td>
        <td align="center" style="padding: 0 20px 20px 20px;">
        <div style="text-align: center;font-family: rockwell; font-size: 30px; color:#333333;padding-bottom: 10px; padding-top: 25px;">HEROES BROCHURE</div>
        <div align="center"><a href="http://www.heroesinrecovery.com/wp-content/uploads/HeroesInRecovery.pdf" target="new"><img src="http://www.heroesinrecovery.com/wp-content/uploads/heroes-in-recovery-resource-one-sheet.png" onmouseover="this.src='http://www.heroesinrecovery.com/wp-content/uploads/heroes-in-recovery-resource-one-sheet-h.png'" onmouseout="this.src='http://www.heroesinrecovery.com/wp-content/uploads/heroes-in-recovery-resource-one-sheet.png'" /></a></div>
        
        <div align="center">
        
        <a href="#" 
  onclick="
    window.open(
      'https://www.facebook.com/sharer/sharer.php?u=heroesinrecovery.com/wp-content/uploads/HeroesInRecovery.pdf', 
      'facebook-share-dialog', 
      'width=626,height=436'); 
    return false;">
  <img src="http://www.heroesinrecovery.com/wp-content/uploads/facebook-resource.png" />
</a>
        
        
        <a href="//pinterest.com/pin/create/button/?url=http%3A%2F%2Fwww.heroesinrecovery.com%2F&media=http%3A%2F%2Fwww.heroesinrecovery.com%2Fwp-content%2Fuploads%2FHeroesInRe&description=Heroes%20in%20Recovery%20is%20here%20to%20help%20eliminate%20the%20social%20stigma%20that%20keeps%20individuals%20with%20addiction%20and%20mental%20health%20issues%20from%20seeking%20help%2C%20and%20does%20so%20by%20sharing%20stories%20of%20real%20journeys.%20" data-pin-do="buttonPin" data-pin-config="none"><img src="http://www.heroesinrecovery.com/wp-content/uploads/pinterest-resource.png" /></a>
        
        
        <a href="http://twitter.com/share?url=http%3A%2F%2Ftinyurl.com%2Fq54we6t&text=Learn%20More%20About%20Heroes%20In%20Recovery%20With%20This%20One-Sheet&via=heroesnrecovery" target="new"><img src="http://www.heroesinrecovery.com/wp-content/uploads/twitter-resource.png" /></a>
        
       
        </div>
        
        <div style="text-align: center;font-family:Georgia, 'Times New Roman', Times, serif; font-size: 16px;"><em>This brochure lays it all out; the numbers, <br />the logo, and the mission. Print this one<br /> sheet off and distribute to those who<br /> want to know more.</em></div>
        </td>
    </tr>
     <tr>
		
        <td align="center" style="padding: 0 20px 20px 20px;">
        <div style="text-align: center;font-family: rockwell; font-size: 30px; color:#333333;padding-bottom: 10px; padding-top: 25px;">HEROES 6K BROCHURE</div>
        <div align="center"><a href="http://www.heroesinrecovery.com/wp-content/uploads/Heroes6K.pdf" target="new"><img src="http://www.heroesinrecovery.com/wp-content/uploads/heroes-in-recovery-6k-one-sheet.png" onmouseover="this.src='http://www.heroesinrecovery.com/wp-content/uploads/heroes-in-recovery-6k-one-sheet-h.png'" onmouseout="this.src='http://www.heroesinrecovery.com/wp-content/uploads/heroes-in-recovery-6k-one-sheet.png'" /></a></div>
        
        <div align="center">
        
        <a href="#" 
  onclick="
    window.open(
      'https://www.facebook.com/sharer/sharer.php?u=heroesinrecovery.com/wp-content/uploads/Heroes6K.pdf', 
      'facebook-share-dialog', 
      'width=626,height=436'); 
    return false;">
  <img src="http://www.heroesinrecovery.com/wp-content/uploads/facebook-resource.png" />
</a>
        
        
        <a href="//pinterest.com/pin/create/button/?url=http%3A%2F%2Fwww.heroesinrecovery.com%2F&media=http%3A%2F%2Fwww.heroesinrecovery.com%2Fwp-content%2Fuploads%2FHeroes6K.pdf&description=Heroes%20in%20Recovery%20is%20here%20to%20help%20eliminate%20the%20social%20stigma%20that%20keeps%20individuals%20with%20addiction%20and%20mental%20health%20issues%20from%20seeking%20help%2C%20and%20does%20so%20by%20sharing%20stories%20of%20real%20journeys.%20" data-pin-do="buttonPin" data-pin-config="none"><img src="http://www.heroesinrecovery.com/wp-content/uploads/pinterest-resource.png" /></a>
        
        
        <a href="http://twitter.com/share?url=http%3A%2F%2Ftinyurl.com%2Fpjaxdhx&text=Learn%20More%20About%20Heroes%20In%20Recovery%20With%20This%20One-Sheet&via=heroesnrecovery" target="new"><img src="http://www.heroesinrecovery.com/wp-content/uploads/twitter-resource.png" /></a>
        
       
        </div>
        
        <div style="text-align: center;font-family:Georgia, 'Times New Roman', Times, serif; font-size: 16px;"><em>This brochure shares the details on the Heroes 6K.<br />Print this one brochure off and distribute to those who<br /> 
        want to know more about our race series.</em></div>
        </td>
    </tr>
</table>
</div>

<?php get_footer(); ?>

<?php
/*
Template Name: [Share Your Story] Frame
*/
?>

<?php get_header(); ?>

<div id="main">

	<div id="share">

		<h1>Would you share a heroes story?</h1>
		<h2>You or your loved one's heroic journey could change someone's life.</h2>
		
		
		<!--
		<div class="form-selection">
			<a class="yes" href="" onclick="jQuery('#form-maybe').hide(); jQuery('#form-no').hide(); jQuery('#form-yes').show(); jQuery('.form-selection').css('background-image', 'url(/wp-content/themes/heroes/images/share-checkbox-yes.png)'); return false;"></a>
			<a class="maybe" href="" onclick="jQuery('#form-yes').hide(); jQuery('#form-no').hide(); jQuery('#form-maybe').show(); jQuery('.form-selection').css('background-image', 'url(/wp-content/themes/heroes/images/share-checkbox-maybe.png)'); return false;"></a>
			<a class="no" href="" onclick="jQuery('#form-yes').hide(); jQuery('#form-maybe').hide(); jQuery('#form-no').show(); jQuery('.form-selection').css('background-image', 'url(/wp-content/themes/heroes/images/share-checkbox-no.png)'); return false;"></a>
		</div>
		-->
		
		<div id="form-yes">
			<iframe src="/wp-content/themes/heroes/share-form.php" scrolling="no" style="overflow:visible; width:100%; height:100%; margin:0;"></iframe>
		</div>
		
		<!--
		<div id="form-maybe">
			<h1>We know that sharing the story of your recovery is an intimate and big step. Got questions? We understand.</h1>
			<div class="faq">
				<div class="q">
					<dt>Do I have to be in recovery to share a story?</dt>
					<dd>Not at all! We know that there are many different kinds of heroes in the recovery process...a supportive parent, sibling or friend can take a heroic step too.</dd>
				</div>
				<div class="q">
					<dt>Can I submit my story anonymously?</dt>
					<dd>We created the Heroes in Recovery movement to help break the stigma associated with addiction and recovery, so that more people can feel comfortable sharing their story or finding the help they need. We want you to feel comfortable in sharing your story, so you can include as much or as little personal information as you choose.</dd>
				</div>
				<div class="q">
					<dt>Where will you use my story?</dt>
					<dd>We publish our HEROES stories here on the website, and may also individually promote them on the Heroes in Recovery facebook page and twitter account.</dd>
				</div>
				<div class="q">
					<dt>Do I have to include a photo?</dt>
					<dd>Adding a pic to your story is up to you. We feel like seeing the beautiful faces of our heroes helps others feel connected, so if you have a great photo, share it! If you would rather not include a photo of yourself, you can choose one of the profile pics from the evergrowing library of HEROES avatars. Or you could even upload a picture of something that is relavant and significant to your recovery.</dd>
				</div>
				<div class="q">
					<dt>I just submitted my story; when will it be published?</dt>
					<dd>We review each story to make sure it will contribute positively to the community, and that it doesn't include any language or offensive remarks. Our edits will never change the integrity of your story. With this process, it can sometimes take a week or two before a submitted story is published. You will receive an email when your story has been published. </dd>
				</div>
			</div>
			<form method="post" action="/thank-you/">
				<fieldset>
					<h2>Still have more questions? Contact us below!</h2>
					<div class="field">
						<input type="text" name="yourname" placeholder="NAME" />
					</div>
					<div class="field">
						<input type="text" name="email" placeholder="EMAIL" />
					</div>
					<div class="field">
						<input type="text" name="phone" placeholder="PHONE" />
					</div>
					<div class="field">
						<textarea name="message" placeholder="HOW CAN WE HELP?"></textarea>
					</div>
					<div class="field">
						<input type="hidden" name="form" value="maybe" />
						<input type="image" src="/wp-content/themes/heroes/images/formmaybe-submit.png" />
					</div>
				</fieldset>
			</form>
		</div>
		
		<div id="form-no">
			<form method="post" action="/thank-you/">
				<fieldset>
					<h1>WE WANT SHARING A STORY OF RECOVERY TO BE APPEALING TO EVERYONE, BUT MAYBE THERE ARE SOME ROADBLOCKS WE HAVEN'T THOUGhT OF. WOULD YOU BE SO KIND TO TELL US?</h1>
					<div class="field">
						<input type="text" name="yourname" placeholder="NAME" />
					</div>
					<div class="field">
						<input type="text" name="email" placeholder="EMAIL" />
					</div>
					<div class="field">
						<input type="text" name="phone" placeholder="PHONE" />
					</div>
					<div class="field">
						<textarea name="message" placeholder="I AM NOT COMFORTABLE SHARING BECAUSE:"></textarea>
					</div>
					<div class="field">
						<input type="hidden" name="form" value="no" />
						<input type="image" src="/wp-content/themes/heroes/images/formno-submit.png" />
					</div>
				</fieldset>
			</form>
		</div>
		-->
	</div>
	
	<script>
		jQuery(document).ready(function() {
			preload();
		});
		function preload(arrayOfImages) {
			jQuery(arrayOfImages).each(function(){
				jQuery('<img/>')[0].src = this;
			});
		}

		// Usage:

		preload([
			'/wp-content/themes/heroes/images/share-checkbox-maybe.png',
			'/wp-content/themes/heroes/images/share-checkbox-no.png'
		]);
	</script>
	
</div>

<?php get_footer(); ?>

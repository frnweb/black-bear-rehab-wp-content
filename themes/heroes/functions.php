<?php

include( TEMPLATEPATH.'/constants.php' );
include( TEMPLATEPATH.'/classes.php' );
include( TEMPLATEPATH.'/widgets.php' );

/**
 * Disable automatic general feed link outputting.
 */
automatic_feed_links( false );

//remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'wp_generator');

//add_filter( 'show_admin_bar', '__return_false' );

if ( function_exists('register_sidebar') ) {
	register_sidebar(array(
		'name' => 'Default Main',
		'before_widget' => '<div class="widget %2$s" id="%1$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>'
	));
	register_sidebar(array(
		'name' => 'Default Sidebar',
		'before_widget' => '<div class="widget %2$s" id="%1$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>'
	));
	register_sidebar(array(
		'name' => 'Default Sidebar 2',
		'before_widget' => '<div class="categories">',
		'after_widget' => '</div>',
		'before_title' => '<h4>',
		'after_title' => '</h4>'
	));
	register_sidebar(array(
		'name' => 'Default Sidebar 3',
		'before_widget' => '<div class="box">',
		'after_widget' => '</div>',
		'before_title' => '<h4>',
		'after_title' => '</h4>'
	));
	register_sidebar(array(
		'name' => 'Inspiring Stories Sidebar',
		'before_widget' => '<div class="widget %2$s" id="%1$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>'
	));
	register_sidebar(array(
		'name' => 'Share Your Story Sidebar',
		'before_widget' => '<div class="widget %2$s" id="%1$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>'
	));
	register_sidebar(array(
		'name' => 'Events Sidebar',
		'before_widget' => '<div class="widget %2$s" id="%1$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>'
	));
	register_sidebar(array(
		'name' => 'Races Sidebar',
		'before_widget' => '<div class="racessidebar widget %2$s" id="%1$s">',
		'after_widget' => '</div>',
		'before_title' => '<h2>',
		'after_title' => '</h2>'
	));
}

if ( function_exists( 'add_theme_support' ) ) {
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 50, 50, true ); // Normal post thumbnails
	add_image_size( 'single-post-thumbnail', 400, 9999, true );
}

register_nav_menus( array(
	'main' => __( 'Main Navigation'),
	'footer' => __( 'Footer Navigation')
) );


//add [email]...[/email] shortcode
function shortcode_email($atts, $content) {
	$result = '';
	for ($i=0; $i<strlen($content); $i++) {
		$result .= '&#'.ord($content{$i}).';';
	}
	return $result;
}
add_shortcode('email', 'shortcode_email');

// register tag [template-url]
function filter_template_url($text) {
	return str_replace('[template-url]',get_bloginfo('template_url'), $text);
}
add_filter('the_content', 'filter_template_url');
add_filter('get_the_content', 'filter_template_url');
add_filter('widget_text', 'filter_template_url');

// register tag [site-url]
function filter_site_url($text) {
	return str_replace('[site-url]',get_bloginfo('url'), $text);
}
add_filter('the_content', 'filter_site_url');
add_filter('get_the_content', 'filter_site_url');
add_filter('widget_text', 'filter_site_url');


/* Replace Standart WP Menu Classes */
function change_menu_classes($css_classes) {
        $css_classes = str_replace("current-menu-item", "active", $css_classes);
        $css_classes = str_replace("current-menu-parent", "active", $css_classes);
        return $css_classes;
}
add_filter('nav_menu_css_class', 'change_menu_classes');


//allow tags in category description
$filters = array('pre_term_description', 'pre_link_description', 'pre_link_notes', 'pre_user_description');
foreach ( $filters as $filter ) {
    remove_filter($filter, 'wp_filter_kses');
}


//Events Post Type & Taxonomies
register_taxonomy (  
	'event_category',  
	array('staff'),  
	array (  
		'hierarchical' => true,  
		'label' => 'Categories',  
		'query_var' => true,  
		'rewrite' => true,
	)  
);


//Events Post Type
function post_type_events() {
	register_post_type(
	'events', 
	array('label' => __('News &amp; Events'), 
	    'public' => true,
	    'publicly_queryable' => true,
	    'show_ui' => true, 
	    'query_var' => true,
	    'menu_position' => 30,
	    'rewrite' => array(
			'slug' => 'events',
			'with_front' => false
			),
		'menu_icon' => '/wp-content/themes/heroes/images/newspaper.png',
	    'supports' => array('title','editor','thumbnail'),
	    'has_archive' => true,
	    'taxonomies' => array('event_category')
	    
	)); 
}
function event_categories() {
    register_taxonomy_for_object_type('event_category', 'events');
}
add_action('init', 'post_type_events');
add_action('init', 'event_categories');


//Stories Post Type & Taxonomies
register_taxonomy (  
	'story_category',  
	array('staff'),  
	array (  
		'hierarchical' => true,  
		'label' => 'Categories',  
		'query_var' => true,  
		'rewrite' => array(
			'slug' => 'story_category',
			'with_front' => false
			),
	)  
);

function post_type_stories() {
	register_post_type(
	'stories', 
	array('label' => __('Stories'), 
	    'public' => true,
	    'publicly_queryable' => true,
	    'show_ui' => true, 
	    'query_var' => true,
	    'menu_position' => 30,
	    'rewrite' => array(
			'slug' => 'stories',
			'with_front' => false
			),
		'menu_icon' => '/wp-content/themes/heroes/images/book.png',
	    'supports' => array('title','editor','thumbnail','comments'),
	    'has_archive' => true,
		'taxonomies' => array('story_category')
	    
	)); 
}
function story_categories() {
    register_taxonomy_for_object_type('story_category', 'stories');
}
add_action('init', 'post_type_stories');
add_action('init', 'story_categories');


//Featured Post Type
function post_type_featured() {
	register_post_type(
	'featured', 
	array('label' => __('Featured Slider'), 
	    'public' => true,
	    'publicly_queryable' => true,
	    'show_ui' => true, 
	    'query_var' => true,
	    'menu_position' => 30,
	    'rewrite' => array(
			'slug' => 'featured',
			'with_front' => false
			),
		'menu_icon' => '/wp-content/themes/heroes/images/featured.png',
	    'supports' => array('title','thumbnail'),
	    'has_archive' => true
	    
	)); 
}
add_action('init', 'post_type_featured');


// Featured
register_taxonomy(  
	'featured',  
	array('post'),  
	array(  
		'hierarchical' => true,  
		'label' => 'Featured',  
		'query_var' => true,  
		'rewrite' => true  
	)  
); 
 
// Login Logo and Title
function my_custom_login_logo() {
    echo '<style type="text/css">
        h1 a { background-image:url('.get_bloginfo('template_directory').'/images/wp_login_logo.png) !important; }
    </style>';
}

add_action('login_head', 'my_custom_login_logo');

function change_wp_login_url() {
echo bloginfo('url');
}

function change_wp_login_title() {
echo get_option('blogname');
}

add_filter('login_headerurl', 'change_wp_login_url');
add_filter('login_headertitle', 'change_wp_login_title');


//Remove RIch Editor
//add_filter( 'user_can_richedit' , '__return_false', 50 );

//Meta Boxes
require_once (TEMPLATEPATH . '/library/functions/meta-boxes.php');

//Ranklab Pagination
function ranklab_pagination() {
global $wp_rewrite, $wp_query;
$wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;
$pagination = array(
'base' => @add_query_arg('page','%#%'),
'format' => '',
'total' => $wp_query->max_num_pages,
'current' => $current,
'prev_text' => __('Previous'),
'next_text' => __('Next'),
'end_size' => 1,
'mid_size' => 2,
'show_all' => false,
'type' => 'list'
);
if ( $wp_rewrite->using_permalinks() )
$pagination['base'] = user_trailingslashit( trailingslashit( remove_query_arg( 's', get_pagenum_link( 1 ) ) ) . 'page/%#%/', 'paged' );
if ( !empty( $wp_query->query_vars['s'] ) )
$pagination['add_args'] = array( 's' => get_query_var( 's' ) );
echo paginate_links( $pagination );
}
 
?>
<?php
/*
Template Name: [Share Your Story] Form
*/
?>
<?
require('/home/heroesin/public_html/wp-blog-header.php');
header("HTTP/1.1 200 OK");

$name = "";
if($_POST) {
	if(trim($_POST["yourname"])!="" && trim($_POST["email"])!="" && trim($_POST["yourname"])!="NAME" && trim($_POST["email"])!="EMAIL") {  //makes sure a spider is not bypassing the javascript verification requiring that the initial two form fields are filled out. If it does, it just keeps displaying the same step 1 content over and over.
		$page = $_POST["page"];
		$name = $_POST["yourname"];
		$email = $_POST["email"];
		$direction = $_POST["direction"];
		$avatar = $_POST["avatar"];
		$quote = $_POST["quote"];
		$categories = $_POST["categories"];

		if (is_array($categories) == 1): $str_categories = implode(',', $categories); else: $str_categories = $_POST["str_categories"]; endif;

		$video = $_POST["video"];
		$storyfull = $_POST["storyfull"];
		$story1 = $_POST["story1"];
		$story2 = $_POST["story2"];
		$story3 = $_POST["story3"];
		$story4 = $_POST["story4"];
		$story5 = $_POST["story5"];
		$story6 = $_POST["story6"];
		$newsletter = $_POST["newsletter"];
		$findout = $_POST["findout"];
		
	}
	else $page = 1;
}
if ($page=='') $page = 1;

if ($direction=='next') {
	if(trim($storyfull)!="" && $page==6) $page=7; //makes sure the questions portion is skipped if they write out their own story
	$page = $page + 1;
}
if ($direction=='back') {
	if(trim($storyfull)!="" && $page==8) $page=7; //makes sure the questions portion was skipped and they wrote out their own story and hit the back button on the last page (#8), then it takes them to the submit story page instead of the questions page.
	$page = $page - 1;
}

$site_name=" | Heroes in Recovery";
if ($page==2) $page_title="Step ".$page.": Upload a Photo or Choose an Avatar" . $site_name; 
else if ($page==3) $page_title="Step ".$page.": Words to Live By"  . $site_name; 
else if ($page==4) $page_title="Step ".$page.": Select Your Story Category"  . $site_name; 
else if ($page==5) $page_title="Step ".$page.": Upload a Video"  . $site_name; 
else if ($page==6) $page_title="Step ".$page.": Write Your Story"  . $site_name; 
else if ($page==7) $page_title="Step ".$page.": Q & A Your Story"  . $site_name; 
else if ($page==8) $page_title="Step ".$page.": Newsletter Sign-up and Referral"  . $site_name;

?>
<html>
<head>
	<title><?=$page_title; ?></title>
	<link rel="stylesheet" type="text/css" href="/wp-content/themes/heroes/js/formupload/uploadifive.css">
	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_url'); ?>/share-form.css" />
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
	<script src="/wp-content/themes/heroes/js/formupload/jquery.uploadifive.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
	<meta name="robots" content="noindex,nofollow" />
	<script src="/wp-content/themes/heroes/js/ckeditor/ckeditor.js"></script>
	
	<?php if ($page>1):?>
	<!-- #####
	Google Analytics
	##### -->
	<?php //frn GA version 2.3 10/2/2013 

	//Defines page variables for central file references and chat screens
		echo "\n";

		//For tracking visitors NOT logged in.
		if ( !is_user_logged_in() ) { 
	?><script type='text/javascript'><?php echo "\n";

	//Our customized GA code for tracking outbound links, etc. ?>
		var _gaq_Account='UA-24915404-1';
		
		var baseDomain = location.hostname.match(RegExp(crossDomains + '$'));
		baseDomain = (baseDomain ? baseDomain[1].replace(/\:\d+/, '') : location.hostname);
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', _gaq_Account]);
		_gaq.push(['_trackPageview']);
		(function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		})();
	</script><?php 
	} 
	else echo "\n <!--YOU ARE LOGGED IN and therefore not being tracked in Google Analytics.--> \n \n" ?>
	<!-- #####
	Google Analytics Ends
	##### -->
	<?php endif; ?>
	
</head>
<body>

<?php


if ($page==1):
?>
<script  type='text/javascript'>
	function updateInput(fid,dvalue){
		if(document.getElementById(fid).value=="") document.getElementById(fid).value = dvalue;
	}
</script>
<form name="page1_form" id="page1" class="form" method="post">
	<fieldset>
		<h1>Awesome, let's cover the basics</h1>
		<div class="field">
			<input type="text" name="yourname" id="yourname" value="<?=($name=="") ? "NAME" : $name; ?>" placeholder="NAME" 
			onblur="if(value=='') value = 'NAME'" 
			onfocus="if(value=='NAME') value = ''" /><!--onchange="updateInput(this.id,'NAME')"-->
			<span class="instructions">You can use your first name only, your full name, initials or even use anonymous. <br />Whatever you are comfortable with.</span>
		</div>
		<div class="field">
			<input type="text" name="email" id="email" value="<?=($email=="") ? "EMAIL" : $email; ?>" placeholder="EMAIL" 
			onblur="if(value=='') value = 'EMAIL'" 
			onfocus="if(value=='EMAIL') value = ''"  />
			<span class="instructions">Unless you choose to publish your email as part of your story to allow others to contact you, it will remain private and Heroes in Recovery will only use it to contact you when your story has been published.</span>
		</div>
		<div class="field checkbox">
			<input type="checkbox" id="agree"> &nbsp;Click here to confirm you have read and agreed to our <br /><a href="/web_content/terms_of_use.pdf" target="_blank">Terms of Use</a>, <a href="/web_content/privacy_policy.pdf" target="_blank">Privacy Policy</a> and <a href="/web_content/privacy_guide.pdf" target="_blank">Privacy Guide</a>.
		</div>
		<!-- Data Input Storage -->
		<input type="hidden" name="avatar" value="<?=$avatar?>" /> <!-- Step 2 -->
		<input type="hidden" name="quote" value="<?=$quote?>" /> <!-- Step 3 -->
		<input type="hidden" name="str_categories" value="<?=$str_categories?>" /> <!-- Step 4 -->
		<input type="hidden" name="video" value="<?=$video?>" /> <!-- Step 5 -->
		<input type="hidden" name="storyfull" value="<?=$storyfull?>" /> <!-- Step 6 -->
		<input type="hidden" name="story1" value="<?=$story1?>" /> <!-- Step 7 -->
		<input type="hidden" name="story2" value="<?=$story2?>" /> <!-- Step 7 -->
		<input type="hidden" name="story3" value="<?=$story3?>" /> <!-- Step 7 -->
		<input type="hidden" name="story4" value="<?=$story4?>" /> <!-- Step 7 -->
		<input type="hidden" name="story5" value="<?=$story5?>" /> <!-- Step 7 -->
		<input type="hidden" name="story6" value="<?=$story6?>" /> <!-- Step 7 -->
		<input type="hidden" name="newsletter" value="<?=$newsletter?>" /> <!-- Step 8 -->
		<input type="hidden" name="findout" value="<?=$findout?>" /> <!-- Step 8 -->
		<input type="hidden" name="page" value="<?=$page?>" />
		<input type="hidden" name="direction" value="" id="direction" />
		<!-- ------------------ -->
		<div class="nav">
			<!--<a onclick="$('#direction').val('back'); $('.form').submit();" style="display:block; float:left; width:105px; height:26px; background:url(/wp-content/themes/heroes/images/form-back.png) no-repeat 0 0;"></a>-->
			<a onclick="return checkForm();" style="display:block; float:right; width:103px; height:26px; background:url(/wp-content/themes/heroes/images/form-next.png) no-repeat 0 0;"></a>
		</div>
	</fieldset>
	<script>
		function checkForm() {
			if (!($('#agree').is(':checked'))) { 
				alert('You must agree to the terms to continue.'); 
				return false; 
			} 			
			if ( $('#yourname').val() == '' || $('#email').val() == '' ) {
				alert('Please enter your name and email address');
				return false;
			}			
			
			$('#direction').val('next');
			$('.form').submit();
		}
	</script>
	<a href="/wp-content/uploads/heroes_story_starter.pdf" target="_blank" class="help"><img src="/wp-content/themes/heroes/js/formupload/form-help.png" /></a>
</form>
<?
endif;



if ($page==2):
?>
<form name="page2_form" id="page2" class="form" method="post" enctype="multipart/form-data">
	<fieldset>
		<h1>You are the face of recovery</h1>
		<h2>Upload a photo. It's way more than a profile pic.</h2>
		<p>
		We believe that having a photo along with your submission makes your story all the more real and  powerful. But we also respect the 
		choice for anonymity. Simply upload a photo of your choice, OR, please select an avatar that you feel represents you and your story...
		</p>
		<div class="uploadavatar" style="cursor:pointer;" >
			<input id="file_upload" type="file" name="file_upload" multiple="false" style="cursor:pointer;" />
			<?php $timestamp = time(); $token = md5('unique_salt' . $timestamp); ?>
			<script>
				$(function() {
					$('#file_upload').uploadifive({
						'formData'     : {
							'timestamp' : '<?php echo $timestamp; ?>',
							'token'     : '<?php echo $token; ?>'
						},
						'uploadScript' : '/wp-content/themes/heroes/js/formupload/uploadifive.php',
						'onUploadComplete' :function(file, data) { 
							$('#avatar').val('/web_content/avatars/<?php echo $token . '_'; ?>' + file.name);
						}
					});
					$("a.fancybox").fancybox({
					    'autoDimensions'   : false,
					    "width": 550,
					    "height": 450,
					    "onComplete": function(){ // Will be called once the content is displayed
					        $(".avatar").click(function(event){
					            event.preventDefault();
					            // activate item
                                $(".avatar").removeClass("active");
                                $(this).addClass("active");
                                // update form value
					            $("#avatar").val($(this).find("img").attr("src").replace(/^.*\/\/[^\/]+/, ''));
					            // close fancybox
					            $.fancybox.close();
					        });
					    }
					});
				});
				
			</script>
			<p>
			Click here to upload a photo from your computer. <span>HINT: We love big happy smiles. Have fun, get creative.</span>
			</p>
		</div>
		<div class="or">
			OR
		</div>
		<div class="selectavatar">
			<a href="#avatarGallery" class="fancybox"><img src="/wp-content/themes/heroes/js/formupload/form-avatarupload.png" /></a>
			<p>
			If you would rather not post a photo of yourself we respect that. You can choose a profile pic that relates to your story.
			</p>
		</div>
		<div style="display:none">
		    <div id="avatarGallery" class="avatarGallery">
		        <ul>
		            <li><a href="#" class="avatar"><?php echo wp_get_attachment_image( 1801 ); ?></a></li> <!-- SERENITY -->
		            <li><a href="#" class="avatar"><?php echo wp_get_attachment_image( 1800 ); ?></a></li> <!-- RENEW -->
		            <li><a href="#" class="avatar"><?php echo wp_get_attachment_image( 1799 ); ?></a></li> <!-- OVERCOME -->
		            <li><a href="#" class="avatar"><?php echo wp_get_attachment_image( 1798 ); ?></a></li> <!-- LIVE -->
		            <li><a href="#" class="avatar"><?php echo wp_get_attachment_image( 1797 ); ?></a></li> <!-- HOPE -->
		            <li><a href="#" class="avatar"><?php echo wp_get_attachment_image( 1796 ); ?></a></li> <!-- INSPIRE -->
		            <li><a href="#" class="avatar"><?php echo wp_get_attachment_image( 1795 ); ?></a></li> <!-- HONESTY -->
		            <li><a href="#" class="avatar"><?php echo wp_get_attachment_image( 1794 ); ?></a></li> <!-- HEAL -->
		            <li><a href="#" class="avatar"><?php echo wp_get_attachment_image( 1793 ); ?></a></li> <!-- GROW -->
		            <li><a href="#" class="avatar"><?php echo wp_get_attachment_image( 1792 ); ?></a></li> <!-- FREE -->   
		            <li><a href="#" class="avatar"><?php echo wp_get_attachment_image( 1791 ); ?></a></li> <!-- FORGIVE -->
		            <li><a href="#" class="avatar"><?php echo wp_get_attachment_image( 1790 ); ?></a></li> <!-- FAITH -->
		            <li><a href="#" class="avatar"><?php echo wp_get_attachment_image( 1789 ); ?></a></li> <!-- EXPLORE -->
		            <li><a href="#" class="avatar"><?php echo wp_get_attachment_image( 1788 ); ?></a></li> <!-- EVOLVE -->
		            <li><a href="#" class="avatar"><?php echo wp_get_attachment_image( 1787 ); ?></a></li> <!-- EMPOWER -->
		            <li><a href="#" class="avatar"><?php echo wp_get_attachment_image( 1786 ); ?></a></li> <!-- COURAGE -->
		            <li><a href="#" class="avatar"><?php echo wp_get_attachment_image( 1785 ); ?></a></li> <!-- BELIEVE -->
		        </ul>
		    </div>
		</div>
		<!-- Data Input Storage -->
		<input type="hidden" name="avatar" value="<?=$avatar?>" id="avatar" /> <!-- This Step -->
		<input type="hidden" name="yourname" value="<?=$name?>" /> <!-- Step 1 -->
		<input type="hidden" name="email" value="<?=$email?>" /> <!-- Step 1 -->
		<input type="hidden" name="quote" value="<?=$quote?>" /> <!-- Step 3 -->
		<input type="hidden" name="str_categories" value="<?=$str_categories?>" /> <!-- Step 4 -->
		<input type="hidden" name="video" value="<?=$video?>" /> <!-- Step 5 -->
		<input type="hidden" name="storyfull" value="<?=$storyfull?>" /> <!-- Step 6 -->
		<input type="hidden" name="story1" value="<?=$story1?>" /> <!-- Step 7 -->
		<input type="hidden" name="story2" value="<?=$story2?>" /> <!-- Step 7 -->
		<input type="hidden" name="story3" value="<?=$story3?>" /> <!-- Step 7 -->
		<input type="hidden" name="story4" value="<?=$story4?>" /> <!-- Step 7 -->
		<input type="hidden" name="story5" value="<?=$story5?>" /> <!-- Step 7 -->
		<input type="hidden" name="story6" value="<?=$story6?>" /> <!-- Step 7 -->
		<input type="hidden" name="newsletter" value="<?=$newsletter?>" /> <!-- Step 8 -->
		<input type="hidden" name="findout" value="<?=$findout?>" /> <!-- Step 8 -->
		<input type="hidden" name="page" value="<?=$page?>" />
		<input type="hidden" name="direction" value="" id="direction" />
		<!-- ------------------ -->
		<div class="nav">
			<a onclick="$('#direction').val('back'); $('.form').submit();" style="display:block; float:left; width:105px; height:26px; background:url(/wp-content/themes/heroes/images/form-back.png) no-repeat 0 0;"></a>
			<a onclick="$('#direction').val('next'); if( $('#avatar').val()=='' ) { alert('Please select or upload an avatar to continue'); return false; } else { $('.form').submit(); }" style="display:block; float:right; width:103px; height:26px; background:url(/wp-content/themes/heroes/images/form-next.png) no-repeat 0 0;"></a>
		</div>
	</fieldset>
	<a href="/wp-content/uploads/heroes_story_starter.pdf" target="_blank" class="help"><img src="/wp-content/themes/heroes/js/formupload/form-help.png" /></a>
</form>

<?
endif;



if ($page==3):
?>
<form name="page3_form" id="page3" class="form" method="post" >
	<fieldset>
		<h1>How about some words to live by?</h1>
		<p>
		Maybe you have some favorite words to live by, a quote that you love, some scripture or a prayer. Use the field below to share. (Limited to 150 characters.)  
		</p>
		<div class="field">
			<textarea name="quote" id="quote"><?=$quote?></textarea>
		</div>
		<!-- Data Input Storage -->
		<input type="hidden" name="yourname" value="<?=$name?>" /> <!-- Step 1 -->
		<input type="hidden" name="email" value="<?=$email?>" /> <!-- Step 1 -->
		<input type="hidden" name="avatar" value="<?=$avatar?>" /> <!-- Step 2 -->
		<input type="hidden" name="str_categories" value="<?=$str_categories?>" /> <!-- Step 4 -->
		<input type="hidden" name="video" value="<?=$video?>" /> <!-- Step 5 -->
		<input type="hidden" name="storyfull" value="<?=$storyfull?>" /> <!-- Step 6 -->
		<input type="hidden" name="story1" value="<?=$story1?>" /> <!-- Step 7 -->
		<input type="hidden" name="story2" value="<?=$story2?>" /> <!-- Step 7 -->
		<input type="hidden" name="story3" value="<?=$story3?>" /> <!-- Step 7 -->
		<input type="hidden" name="story4" value="<?=$story4?>" /> <!-- Step 7 -->
		<input type="hidden" name="story5" value="<?=$story5?>" /> <!-- Step 7 -->
		<input type="hidden" name="story6" value="<?=$story6?>" /> <!-- Step 7 -->
		<input type="hidden" name="newsletter" value="<?=$newsletter?>" /> <!-- Step 8 -->
		<input type="hidden" name="findout" value="<?=$findout?>" /> <!-- Step 8 -->
		<input type="hidden" name="page" value="<?=$page?>" />
		<input type="hidden" name="direction" value="" id="direction" />
		<!-- ------------------ -->		
		<div class="nav">
			<a onclick="$('#direction').val('back'); $('.form').submit();" style="display:block; float:left; width:105px; height:26px; background:url(/wp-content/themes/heroes/images/form-back.png) no-repeat 0 0;"></a>
			<a onclick="$('#direction').val('next'); $('.form').submit();" style="display:block; float:right; width:103px; height:26px; background:url(/wp-content/themes/heroes/images/form-next.png) no-repeat 0 0;"></a>
		</div>
	</fieldset>
	<a href="/wp-content/uploads/heroes_story_starter.pdf" target="_blank" class="help"><img src="/wp-content/themes/heroes/js/formupload/form-help.png" /></a>
</form>
<?php
endif;



if ($page==4):
?>
<form name="page4_form" id="page4" class="form" method="post" >
	<fieldset>
		<h1>Tag your story.</h1>
		<p>
		Select all the categories that apply to your story. This will help our community members and readers find the stories that will 
		be the most encouraging to them.
		</p>
		<div class="clearfix">
			<?php
			$categories = explode(',', $str_categories);
			$terms = get_terms( 'story_category', array('hide_empty' => false, 'orderby' => 'id') );
			foreach ( $terms as $term ): ?>
				<div class="checkbox">
					<input type="checkbox" name="categories[]" value="<?=$term->name?>"<?=in_array($term->name, $categories) ? ' checked' : ''?> /> <?=$term->name?>
				</div>
			<?php endforeach; ?>
		</div>
		<!-- Data Input Storage -->
		<input type="hidden" name="yourname" value="<?=$name?>" /> <!-- Step 1 -->
		<input type="hidden" name="email" value="<?=$email?>" /> <!-- Step 1 -->
		<input type="hidden" name="avatar" value="<?=$avatar?>" /> <!-- Step 2 -->
		<input type="hidden" name="quote" value="<?=$quote?>" /> <!-- Step 3 -->
		<input type="hidden" name="str_categories" value="<?=$str_categories?>" /> <!-- Step 4 -->
		<input type="hidden" name="video" value="<?=$video?>" /> <!-- Step 5 -->
		<input type="hidden" name="storyfull" value="<?=$storyfull?>" /> <!-- Step 6 -->
		<input type="hidden" name="story1" value="<?=$story1?>" /> <!-- Step 7 -->
		<input type="hidden" name="story2" value="<?=$story2?>" /> <!-- Step 7 -->
		<input type="hidden" name="story3" value="<?=$story3?>" /> <!-- Step 7 -->
		<input type="hidden" name="story4" value="<?=$story4?>" /> <!-- Step 7 -->
		<input type="hidden" name="story5" value="<?=$story5?>" /> <!-- Step 7 -->
		<input type="hidden" name="story6" value="<?=$story6?>" /> <!-- Step 7 -->
		<input type="hidden" name="newsletter" value="<?=$newsletter?>" /> <!-- Step 8 -->
		<input type="hidden" name="findout" value="<?=$findout?>" /> <!-- Step 8 -->
		<input type="hidden" name="page" value="<?=$page?>" />
		<input type="hidden" name="direction" value="" id="direction" />
		<!-- ------------------ -->	
		<div class="nav">
			<a onclick="$('#direction').val('back'); $('.form').submit();" style="display:block; float:left; width:105px; height:26px; background:url(/wp-content/themes/heroes/images/form-back.png) no-repeat 0 0;"></a>
			<a onclick="$('#direction').val('next'); $('.form').submit();" style="display:block; float:right; width:103px; height:26px; background:url(/wp-content/themes/heroes/images/form-next.png) no-repeat 0 0;"></a>
		</div>
	</fieldset>
	<a href="/wp-content/uploads/heroes_story_starter.pdf" target="_blank" class="help"><img src="/wp-content/themes/heroes/js/formupload/form-help.png" /></a>
</form>
<?php
endif;



if ($page==5):
?>
<form name="page5_form" id="page5" class="form" method="post" enctype="multipart/form-data">
	<fieldset>
		<h1>Do you want to start with a video?</h1>
		<p>
		If you'd like to share your story by video, you're in the right place! You can get as creative as you want or keep it simple. 
		Script it out or just hit record and go for it. The main thing is to just relax, take a deep breath, have fun and tell it 
		straight from your heart. Please keep your videos under 5 minutes.
		</p>
		<p class="instructions">
			Accepted file types: .mov | .mp4 | .wmv<br />
			Maximum file size: 50MB
		</p>
		<div class="video">
			<!--
			<div class="videolink">
				Your video:<br />
				<span id="link"></span>
			</div>
			-->
			<input id="video_upload" type="file" name="video_upload" multiple="false" />
			<?php $timestamp = time(); $token = md5('unique_salt' . $timestamp); ?>
			<script>
				$(function() {
					$('#video_upload').uploadifive({
						'formData'     : {
							'timestamp' : '<?php echo $timestamp; ?>',
							'token'     : '<?php echo $token; ?>',
							'utype'		: 'video',
						},
						'fileSizeLimit' : 256000,
						'uploadScript' : '/wp-content/themes/heroes/js/formupload/uploadifive.php',
						'onUploadComplete' :function(file, data) { 
							$('#video').val('<?php echo $token . '_'; ?>' + file.name);
							$('#skip').css('background-image', 'url(/wp-content/themes/heroes/images/form-next.png)');
							$('.nav span').hide();
							$('.nav').css('width', '485px');
						}
					});
				});
			</script>
		</div>
		<!-- Data Input Storage -->
		<input type="hidden" name="video" value="<?=$video?>" id="video" /> <!-- This Step -->
		<input type="hidden" name="yourname" value="<?=$name?>" /> <!-- Step 1 -->
		<input type="hidden" name="email" value="<?=$email?>" /> <!-- Step 1 -->
		<input type="hidden" name="avatar" value="<?=$avatar?>" /> <!-- Step 2 -->
		<input type="hidden" name="quote" value="<?=$quote?>" /> <!-- Step 3 -->
		<input type="hidden" name="str_categories" value="<?=$str_categories?>" /> <!-- Step 4 -->
		<input type="hidden" name="storyfull" value="<?=$storyfull?>" /> <!-- Step 6 -->
		<input type="hidden" name="story1" value="<?=$story1?>" /> <!-- Step 7 -->
		<input type="hidden" name="story2" value="<?=$story2?>" /> <!-- Step 7 -->
		<input type="hidden" name="story3" value="<?=$story3?>" /> <!-- Step 7 -->
		<input type="hidden" name="story4" value="<?=$story4?>" /> <!-- Step 7 -->
		<input type="hidden" name="story5" value="<?=$story5?>" /> <!-- Step 7 -->
		<input type="hidden" name="story6" value="<?=$story6?>" /> <!-- Step 7 -->
		<input type="hidden" name="newsletter" value="<?=$newsletter?>" /> <!-- Step 8 -->
		<input type="hidden" name="findout" value="<?=$findout?>" /> <!-- Step 8 -->
		<input type="hidden" name="page" value="<?=$page?>" />
		<input type="hidden" name="direction" value="" id="direction" />
		<!-- ------------------ -->	
		<div class="nav">
			<a onclick="$('#direction').val('back'); $('.form').submit();" style="display:block; float:left; width:105px; height:26px; background:url(/wp-content/themes/heroes/images/form-back.png) no-repeat 0 0;"></a>
			<span>You can also head straight to writing your story.</span>
			<a onclick="$('#direction').val('next'); $('.form').submit();" id="skip" style="display:block; float:right; width:103px; height:26px; background:url(/wp-content/themes/heroes/images/form-skip.png) no-repeat 0 0;"></a>
		</div>
		<script>
			//$('#link').html( $('#video').val() );
			if ( $('#video').val() != '' ) {
				$('#skip').css('background-image', 'url(/wp-content/themes/heroes/images/form-next.png)');
				$('.nav span').hide();
				$('.nav').css('width', '485px');
			}
		</script>
	</fieldset>
	<a href="/wp-content/uploads/heroes_story_starter.pdf" target="_blank" class="help"><img src="/wp-content/themes/heroes/js/formupload/form-help.png" /></a>
</form>
<?php
endif;



if ($page==6):
?>
<form name="page6_form" id="page6" class="form" method="post" >
	<style>
		.field span.cke_button_label, .field span.cke_voice_label { display:none; }
	</style>
	<fieldset>
		<h1>I have my story ready to share.</h1>
		<p>
		Some of us are born to be storytellers and the words just come to us naturally. If that's you, simply begin typing out your story or 
		craft it elsewhere and paste it below. It can be as short or as long as you care to make it. Once you're done, click the "Submit Story" 
		button. 
		</p>
		<p><strong><i>If the words aren't coming to you so quickly, don't worry. Click "NEXT" to answer some guiding questions.</i></strong></p>
		<div class="field">
			<textarea name="storyfull" id="storyfull"><?=$storyfull?></textarea>
			<script>

			// Replace the <textarea id="editor"> with an CKEditor
			// instance, using default configurations.
			CKEDITOR.replace( 'storyfull', {
				toolbar: [
					[ 'Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Paste', 'PasteText', 'PasteFromWord' ]
				],
				skin: 'moono',
				height: 150,
				resize_enabled: false,
				removePlugins: 'elementspath'
			});

		</script>

		</div>		
		<!-- Data Input Storage -->
		<input type="hidden" name="yourname" value="<?=$name?>" /> <!-- Step 1 -->
		<input type="hidden" name="email" value="<?=$email?>" /> <!-- Step 1 -->
		<input type="hidden" name="avatar" value="<?=$avatar?>" /> <!-- Step 2 -->
		<input type="hidden" name="quote" value="<?=$quote?>" /> <!-- Step 3 -->
		<input type="hidden" name="str_categories" value="<?=$str_categories?>" /> <!-- Step 4 -->
		<input type="hidden" name="video" value="<?=$video?>" /> <!-- Step 5 -->
		<input type="hidden" name="story1" value="<?=$story1?>" /> <!-- Step 7 -->
		<input type="hidden" name="story2" value="<?=$story2?>" /> <!-- Step 7 -->
		<input type="hidden" name="story3" value="<?=$story3?>" /> <!-- Step 7 -->
		<input type="hidden" name="story4" value="<?=$story4?>" /> <!-- Step 7 -->
		<input type="hidden" name="story5" value="<?=$story5?>" /> <!-- Step 7 -->
		<input type="hidden" name="story6" value="<?=$story6?>" /> <!-- Step 7 -->
		<input type="hidden" name="newsletter" value="<?=$newsletter?>" /> <!-- Step 8 -->
		<input type="hidden" name="findout" value="<?=$findout?>" /> <!-- Step 8 -->
		<input type="hidden" name="page" value="<?=$page?>" id="page" />
		<input type="hidden" name="direction" value="" id="direction" />
		<input type="hidden" name="form" value="yes" />
		<!-- ------------------ -->	
		<div class="nav">
			<a onclick="$('#direction').val('back'); $('.form').submit();" style="display:block; float:left; width:105px; height:26px; background:url(/wp-content/themes/heroes/images/form-back.png) no-repeat 0 0;"></a>
			<!--<a onclick="$('#direction').val('next'); $('.form').submit();" style="margin:-16px 0 0 70px; display:block; float:left; width:137px; height:61px; background:url(/wp-content/themes/heroes/js/formupload/form-uploadstory.png) no-repeat 0 0;"></a>-->
			<!--<a onclick="$('#direction').val('next'); $('#page6').attr('action', '/thank-you/'); $('#page6').attr('target', '_top'); $('.form').submit();" style="margin:-16px 0 0 70px; display:block; float:left; width:137px; height:61px; background:url(/wp-content/themes/heroes/js/formupload/form-uploadstory.png) no-repeat 0 0;"></a>-->
			<span>If the words aren't coming to you so quickly, don't worry. Just leave the box above completely blank and click "NEXT". We'll provide some guiding questions to help you out.  </span>
			<a onclick="$('#direction').val('next'); $('.form').submit();" style="display: block; float: right; width: 103px; height: 26px; background: url(/wp-content/themes/heroes/images/form-next.png) no-repeat 0 0;"></a>
		</div>
	</fieldset>
	<a href="/wp-content/uploads/heroes_story_starter.pdf" target="_blank" class="help"><img src="/wp-content/themes/heroes/js/formupload/form-help.png" /></a>
</form>
<?php
endif;



if ($page==7):
?>
<form name="page7_form" id="page7" class="form" method="post">
	<fieldset>
		<h1>No problem. We can guide you.</h1>
		<p>
		Here's a series of questions that relate to recovery. Your story wll be posted in a Q &amp; A style. Take all the time you need 
		and skipping a question is just fine. 
		</p>
		<div class="field" id="story1">
			<h2>question 1</h2>
			<p>Start by describing the situation that changed your life or a loved one's life.</p>
			<textarea name="story1"><?=$story1?></textarea>
		</div>
		<div class="field" id="story2">
			<h2>question 2</h2>
			<p>Based on your situation or story, was there a turning point that prompted the need for change or help?</p>
			<textarea name="story2"><?=$story2?></textarea>
		</div>
		<div class="field" id="story3">
			<h2>question 3</h2>
			<p>How did you or your HERO get help?<br /><br /></p>
			<textarea name="story3"><?=$story3?></textarea>
		</div>
		<div class="field" id="story4">
			<h2>question 4</h2>
			<p>Based on your experience, what lessons did you learn? Do you have any advice to give?</p>
			<textarea name="story4"><?=$story4?></textarea>
		</div>
		<div class="field" id="story5">
			<h2>question 5</h2>
			<p>If you or your loved one is in recovery, describe what life is like today.</p>
			<textarea name="story5"><?=$story5?></textarea>
		</div>
		<div class="field" id="story6">
			<h2>question 6</h2>
			<p>Is there anything else you'd like to share?<br /><br /></p>
			<textarea name="story6"><?=$story6?></textarea>
		</div>
		<!-- Data Input Storage -->
		<input type="hidden" name="yourname" value="<?=$name?>" /> <!-- Step 1 -->
		<input type="hidden" name="email" value="<?=$email?>" /> <!-- Step 1 -->
		<input type="hidden" name="avatar" value="<?=$avatar?>" /> <!-- Step 2 -->
		<input type="hidden" name="quote" value="<?=$quote?>" /> <!-- Step 3 -->
		<input type="hidden" name="str_categories" value="<?=$str_categories?>" /> <!-- Step 4 -->
		<input type="hidden" name="video" value="<?=$video?>" /> <!-- Step 5 -->
		<input type="hidden" name="storyfull" value="<?=$storyfull?>" /> <!-- Step 6 -->
		<input type="hidden" name="newsletter" value="<?=$newsletter?>" /> <!-- Step 8 -->
		<input type="hidden" name="findout" value="<?=$findout?>" /> <!-- Step 8 -->
		<input type="hidden" name="page" value="<?=$page?>" />
		<input type="hidden" name="direction" value="" id="direction" />
		<input type="hidden" name="question" value="1" />
		<input type="hidden" name="form" value="yes" />
		<!-- ------------------ -->	
		<div class="nav">
			<a id="back-p" onclick="$('#direction').val('back'); $('.form').submit();" style="display:block; float:left; width:105px; height:26px; background:url(/wp-content/themes/heroes/images/form-back.png) no-repeat 0 0;"></a>
			<a onclick="$('#direction').val('next'); $('.form').submit();" id="skip" style="display:block; float:right; width:100px; height:26px; background:url(/wp-content/themes/heroes/images/form-skip.png) no-repeat 0 0;"></a>
			<!--<a id="next-p" onclick="$('#direction').val('next'); $('#page7').attr('action', '/thank-you/'); $('#page7').attr('target', '_top'); $('.form').submit();" style="display:none; float:right; width:103px; height:26px; background:url(/wp-content/themes/heroes/images/form-next.png) no-repeat 0 0;"></a>-->
			<a id="back-q" onclick="toggleQuestion('back', 1)" style="display:none; float:left; width:105px; height:26px; background:url(/wp-content/themes/heroes/images/form-back.png) no-repeat 0 0;"></a>
			<!--<a id="next-q" onclick="toggleQuestion('next', 1)" style="display:block; float:right; width:247px; height:26px; background:url(/wp-content/themes/heroes/images/form-nextq.png) no-repeat 0 0;"></a>-->
			<a id="next-q" onclick="toggleQuestion('next', 1)" style="border-left:1px solid gray; border-right:1px solid gray; display:block; float:right; height:30px; margin-left:5px; margin-right:10px; padding-left:9px; padding-right:5px; width:240px;"><img src="/wp-content/themes/heroes/images/form-nextq.png" style="width:235px;" alt="Next Question" /></a>
		</div>
		<script>
			function toggleQuestion(direction, question) {
				$('#story'+question).hide();
				if (direction=='back') { question = question - 1; } else { question = question + 1; }
				$('#story'+question).show();
				
				if (question > 1) {
					$('#back-p').hide();
					$('#back-q').show();
				}
				else if (question == 1) {
					$('#back-q').hide();
					$('#back-p').show();
				}
				if (question == 6) {
					$('#next-q').hide();
					$('#next-p').show();
				}
				else {
					$('#next-p').hide();
					$('#next-q').show();
				}
				$('#next-q').attr('onclick', 'toggleQuestion(\'next\', '+question+')');
				$('#back-q').attr('onclick', 'toggleQuestion(\'back\', '+question+')');
			}
		</script>
	</fieldset>
	<a href="/wp-content/uploads/heroes_story_starter.pdf" target="_blank" class="help"><img src="/wp-content/themes/heroes/js/formupload/form-help.png" /></a>
</form>
<?php
endif;



if ($page==8):
?>
<form name="page8_form" id="page8" class="form" method="post" action="">
	<fieldset>
		<h1>Oops, one more last thing :) </h1>
		<h2>Okay two more last things</h2>
		<h3>Can we sign you up for our HEROES newsletter?</h3>
		<div class="clearfix" style="padding-bottom:50px;">
			<div class="radio">
				<input type="radio" name="newsletter" value="yes"<?=$newsletter=='yes'?' checked':'';?> /> Yes of course
			</div>
			<div class="radio">
				<input type="radio" name="newsletter" value="no"<?=$newsletter=='no'?' checked':'';?> /> Thanks, but no thanks
			</div>
		</div>
		<h3>How did you find out about Heroes in Recovery?</h3>
		<div class="clearfix">
			<div class="radio">
				<input type="radio" name="findout" value="Family"<?=$findout=='Family'?' checked':'';?> /> Family
			</div>
			<div class="radio">
				<input type="radio" name="findout" value="Counselor_Physician"<?=$findout=='Counselor_Physician'?' checked':'';?> /> Counselor or Physician
			</div>
			<div class="radio">			
				<input type="radio" name="findout" value="Friend"<?=$findout=='Friend'?' checked':'';?> /> Friend
			</div>
			<div class="radio">
				<input type="radio" name="findout" value="Heroes_Advocate"<?=$findout=='Heroes_Advocate'?' checked':'';?> /> Heroes Advocate
			</div>
			<div class="radio">
				<input type="radio" name="findout" value="FRN_Alumni"<?=$findout=='FRN_Alumni'?' checked':'';?> /> FRN Alumni
			</div>
			<div class="radio">
				<input type="radio" name="findout" value="Other"<?=$findout=='Other'?' checked':'';?> /> Other
			</div>
		</div>
		<!-- Data Input Storage -->
		<input type="hidden" name="yourname" value="<?=$name?>" /> <!-- Step 1 -->
		<input type="hidden" name="email" value="<?=$email?>" /> <!-- Step 1 -->
		<input type="hidden" name="avatar" value="<?=$avatar?>" /> <!-- Step 2 -->
		<input type="hidden" name="quote" value="<?=$quote?>" /> <!-- Step 3 -->
		<input type="hidden" name="str_categories" value="<?=$str_categories?>" /> <!-- Step 4 -->
		<input type="hidden" name="video" value="<?=$video?>" /> <!-- Step 5 -->
		<input type="hidden" name="storyfull" value="<?=$storyfull?>" /> <!-- Step 6 -->
		<input type="hidden" name="story1" value="<?=$story1?>" /> <!-- Step 7 -->
		<input type="hidden" name="story2" value="<?=$story2?>" /> <!-- Step 7 -->
		<input type="hidden" name="story3" value="<?=$story3?>" /> <!-- Step 7 -->
		<input type="hidden" name="story4" value="<?=$story4?>" /> <!-- Step 7 -->
		<input type="hidden" name="story5" value="<?=$story5?>" /> <!-- Step 7 -->
		<input type="hidden" name="story6" value="<?=$story6?>" /> <!-- Step 7 -->
		<input type="hidden" name="page" value="<?=$page?>" />
		<input type="hidden" name="direction" value="" id="direction" />
		<input type="hidden" name="form" value="yes" />
		<!-- ------------------ -->	
		<div class="nav">
			<!--<a onclick="$('#direction').val('back'); $('.form').submit();" style="display:block; float:left; width:105px; height:26px; background:url(/wp-content/themes/heroes/images/form-back.png) no-repeat 0 0;"></a>-->
			<!--<a onclick="$('#direction').val('next'); $('#page8').attr('action', '/thank-you/'); $('#page8').attr('target', '_top'); $('.form').submit();" style="display:block; float:right; width:103px; height:26px; background:url(/wp-content/themes/heroes/images/form-next.png) no-repeat 0 0;"></a>-->
			<!--$('#direction').val('next'); $('#page8').attr('action', '/thank-you/');  -->
			<a onclick="$('#direction').val('back'); $('.form').submit();" style="display:block; float:left; width:105px; height: 26px; margin-top: 20px;background:url(/wp-content/themes/heroes/images/form-back.png) no-repeat 0 0;"></a>
			<a onclick="$('#page8').attr('action', '/thank-you/'); $('#page8').attr('target', '_top'); $('.form').submit();" style="display:block; float: right; width: 137px; height: 63px; background: url(/wp-content/themes/heroes/images/formno-submit.png) no-repeat 0 0;"></a>
		</div>
	</fieldset>
	<a href="/wp-content/uploads/heroes_story_starter.pdf" target="_blank" class="help"><img src="/wp-content/themes/heroes/js/formupload/form-help.png" /></a>
</form>
<?php
endif;

//echo "<p>Page: " . $page . "</p>";

?>

</body>
</html>
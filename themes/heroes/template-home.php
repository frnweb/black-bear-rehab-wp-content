<?php
/*
Template Name: Home Template
*/
?>

<?php get_header(); ?>

<script type="text/javascript">
jQuery(document).ready(function(){
	if (jQuery.browser.safari && document.readyState != "complete"){
    	setTimeout( arguments.callee, 100 );
    	return;
  	} 
 	var pic = jQuery(".post .post-content .placeholder img");
	pic.removeAttr("width"); 
	pic.removeAttr("height");  
});
</script>

<div id="main">
	<div id="slides">
		<div id="slidecontainer">
		<div id="slideshow">
			<?php query_posts('post_type=featured&posts_per_page=10');?>
			<div id="slide-nav"></div>
			<?php if (have_posts()) :  while  (have_posts()) : the_post(); ?>
				<a href="<?php echo get_post_meta($post->ID,'hir_link_url', true);?>"><?php the_post_thumbnail( 'full' ); ?></a>
			
			<?php endwhile; endif; ?>
			<?php wp_reset_query();?>
        </div>
        </div><!-- end slide-container -->
		<div id="slide-buttons">
			<div id="slidebutton1" class="slide">
				<a href="/stories/">read inspiring HEROES stories></a>
			</div>
			<div id="slidebutton2" class="slide">
				<a href="/share/">share your inspiring HEROES story></a>
			</div>
			<div id="slidebutton3" class="slide nomargin">
				<a href="/about/">be inspired by the HEROES movement></a>
			</div>
		</div>
		<? 
		/*
		<script type="text/javascript">
			var t;
			jQuery(document).ready(function() {
				t = setTimeout(function() { moveSlide(<?php echo $count; ?>) }, 5000);
			});
			
			function moveSlide(idx) {
				var slidein, slideout
				slideout = 'slide'+idx;
				if (idx == <?php echo $count;  ?>) { idx = 1; } else { idx = idx+1; }
				slidein = 'slide'+idx;
				jQuery('#'+slideout).fadeOut(1000);
				jQuery('#'+slidein).fadeIn(1000);
				t = setTimeout(function() { moveSlide(idx) }, 5000);
			}
			
			function changeSlide(idx) {
				clearTimeout(t);
				jQuery('.slide').each(function(index) {
					jQuery(this).fadeOut(1000);
				});
				jQuery('#slide'+idx+'').fadeIn(1000);	
				t = setTimeout(function() { moveSlide(idx+1) }, 30000);				
			}
		</script>
		*/ 
		?>
		<script type="text/javascript">
		    jQuery('#slideshow').cycle({
		        fx:'fade',
		        speed:'slow',
		        timeout:5000,
		        pager:'#slide-nav',
				slideExpr:'img'
		    });
		</script>
	</div>

	
	<div id="homeleft">
		<div class="video"><a class="hp-video" href="hp-video.html"><img src="<?php bloginfo('template_url'); ?>/images/hp-video-play.jpg" /></a></div>
		<div class="intro">
			<h2>WHAT IS HEROES IN RECOVERY?</h2>
			<p>
			Heroes in Recovery celebrates the heroic efforts of those who seek the addiction and mental health help they need without feeling ashamed or isolated. This grassroots movement is intended to remove the social stigma associated with people in recovery, to recognize the heroic effort it takes to overcome the obstacles in seeking help and to celebrate the act of preventing the past from kidnapping the future. <a href="/about/">[READ MORE]</a>
			</p>
		</div>
		<div style="clear:both;"></div>
		<div class="recent">
			<div class="recent-story">
				<span class="recent-story-flag"><span>RECENT HEROES STORIES</span></span>
				<div class="stories">
					<?php
					global $post;
					$args = array( 'numberposts' => 2, 'post_type' => 'stories', 'post_status' => 'publish', 'orderby' => 'post_date' );
					$myposts = get_posts( $args );
					foreach( $myposts as $post ) :	setup_postdata($post); $id = get_the_ID(); ?>
						<div class="story">
							<p class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
							<p class="excerpt"><?php echo get_post_meta($post->ID,'hir_hero_excerpt', true);?> (<a href="<?php the_permalink(); ?>">more</a>)</p>
						</div>
					<?php endforeach; ?>	
				</div>
			</div>
			<div class="recent-events">
				<div class="events">
					<h2>News &amp; Events</h2>
					<?php
					global $post;
					$args = array( 'numberposts' => 3, 'post_type' => 'events', 'post_status' => 'publish', 'orderby' => 'post_date' );
					$myposts = get_posts( $args );
					foreach( $myposts as $post ) :	setup_postdata($post); $id = get_the_ID(); ?>
						<div class="event">
							<div class="thumbnail">
								<?php
								$images = rwmb_meta( 'hir_event_image', 'type=image' );
								foreach ( $images as $image )
								{
								    echo "<img src='{$image['url']}' width='{$image['width']}' height='{$image['height']}' alt='{$image['alt']}' />";
								}
								?>
							</div>
							<p class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
							<p class="excerpt"><?php the_excerpt();?> (<a href="<?php the_permalink(); ?>">more</a>)</p>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
			<div style="clear:both;"></div>
		</div>
		
		<div class="twocolumns">
			<div class="two-cols">
				<div class="post-col">
					<div class="post">
						<?php query_posts('featured=homepage&posts_per_page=1'); ?>
						<?php if (have_posts()) : ?>
							<?php while (have_posts()) : the_post(); ?>
								<div class="author-box">
									<?php $user_info = get_userdata($post->post_author); ?>
									<span class="feature"><span>FEATURE</span></span>
									<!--<a href="<?php echo $user_info-> user_url; ?>" class="img-box">--><?php echo get_avatar($post->post_author, 102); ?><!--</a>-->
									
									<div class="about">
										<p><?php echo $user_info->user_firstname . ' ' . $user_info->user_lastname; ?></p>
										<!--<a href="<?php echo $user_info-> user_url; ?>" class="link-more">More About <?php echo $user_info->user_firstname; ?> &gt;</a>-->
									</div>
								</div>
								<div class="post-content">
										<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
										<?php if(get_post_meta(get_the_ID(), 'subtitle', true)){ ?><h3><?php echo get_post_meta(get_the_ID(), 'subtitle', true);?></h3><?php } ?>
										<span class="info">Posted on <strong><?php the_time('F jS'); ?></strong> by <strong><?php echo get_the_author(get_the_ID()); ?></strong> | <?php comments_popup_link('No comments', '1 comment', '% comments'); ?></span>
										<p><?php the_excerpt(); ?> [<a href="<?php the_permalink(); ?>">READ MORE</a>]</p>
										<?php //the_content(); ?>
										<div class="comments">
											<span class="number"><?php comments_popup_link('Comments (0)', 'Comments (1)', 'Comments (%)'); ?></span>
											<?php //the_tags('<ul class="tag-list"><li>', ',</li><li>', '</li></ul>'); ?>
										</div>
										<!--<div class="social-box">
											<ul class="social-comments">
												<li><?php comments_popup_link('No comments', '1 comment', '% comments'); ?></li>
												<li><span class='st_facebook_hcount' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>' displayText='share'></span></li>
												<li><span class='st_twitter_hcount' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>' displayText='share'></span></li>
											</ul>
											<div class="rating-box">
												<span>Rate:</span>
											</div>
											<?php //if(function_exists('the_ratings')) { the_ratings(); } ?>
										</div>-->
								</div>
							<?php endwhile; ?>
						<?php else: ?>
							<h2>Not Found</h2>
							<p>Sorry, but you are looking for something that isn't here.</p>
						<?php endif; ?>
						<?php wp_reset_query(); ?>
					</div>
				</div>
				<?php get_sidebar('2'); ?>
			</div>
		</div>
	</div>
	
	
	<div id="homeright">
		<?php get_sidebar(); ?>
	</div>
	
	<div style="clear:both;"></div>
	
</div>

<?php get_footer(); ?>

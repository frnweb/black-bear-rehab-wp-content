<?php
/*
Template Name: Journey Template
*/
?>

<?php get_header(); ?>

<?
// Set up page values
$name = explode('/', $_SERVER["REQUEST_URI"]);
switch ($name[2]) {
	case 'diana':
		$hero = $diana_story;
		break;
	case 'allen':
		$hero = $allen_story;
		break;
	case 'margaret':
		$hero = $margaret_story;
		break;
	case 'mike':
		$hero = $mike_story;
		break;
	case 'tracy':
		$hero = $tracy_story;
		break;
	case 'morgan':
		$hero = $morgan_story;
		break;
	default:
		$hero[3] = 'morgan';
		$hero[4] = 'diana';
		break;
}

if (empty($name[2])){ //Landing page
	$next = '<a href="/heroes/'.$hero[4].'/" class="link-next">next</a>';
	$prev = '<a href="/heroes/'.$hero[3].'/" class="link-prev">prev</a>';
}
elseif ($name[2] == 'morgan') {
	$next = '<a href="/heroes/" class="link-next">next</a>';
	$prev = '<a href="/heroes/'.$hero[3].'/" class="link-prev">prev</a>';
}
else {
	if ($hero[3]) { $prev = '<a href="/heroes/'.$hero[3].'/" class="link-prev">prev</a>'; } else { $prev = ''; }
	if ($hero[3] == 'home') { $prev = '<a href="/heroes/" class="link-prev">prev</a>'; }
	if ($hero[4]) { $next = '<a href="/heroes/'.$hero[4].'/" class="link-next">next</a>'; } else { $next = ''; }
}

?>

<div id="main">
	<div class="gallery-box" <? if (empty($name[2])) { echo 'style="padding-bottom:40px;"'; } ?>>
		<div class="gallery">
			<?=$prev?>
			<?=$next?>
			<div class="gallery-holder">
				<ul>
					<li>
						<? if (empty($name[2])): ?>
						
						<div class="box journey-left">
							<a href="/heroes/<?=$the_heroes[5]?>/">
								<img src="<?php bloginfo('template_url'); ?>/images/img-journey.png" />
							</a>
						</div>
						
						<? else: ?>
						
						<? if ($hero[3] == 'home'): ?>
							<a href="/heroes/">
						<? else: ?>
							<a href="/heroes/<?=$hero[3]?>/">
						<? endif; ?>
							<div class="box" style="background:<?=$hero[1]?> url(<?php bloginfo('stylesheet_directory'); ?>/images/journeyleft-<?=$hero[0]?>.png) no-repeat 0 0;"></div>
						</a>
						
						<? endif; ?>
					</li>
					<li>
						<? if (empty($name[2])): ?>
						
							<div class="box">
								<div class="rotator">
									<ul>
										<li class="show" style="left:-31px; top:20px;">
											<a href="/heroes/<?=$the_heroes[0]?>/"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/img-h.jpg" /></a>
										</li>
										<li style="left:-31px;">
											<a href="/heroes/<?=$the_heroes[1]?>/"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/img-e.jpg" /></a>
										</li>
										<li>
											<a href="/heroes/<?=$the_heroes[2]?>/"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/img-r.jpg" /></a>
										</li>
										<li>
										<a href="/heroes/<?=$the_heroes[3]?>/">
											<img src="<?php bloginfo('stylesheet_directory'); ?>/images/img-o.jpg" />
										</a>
										</li>
										<li>
											<a href="/heroes/<?=$the_heroes[4]?>/">
												<img src="<?php bloginfo('stylesheet_directory'); ?>/images/img-ee.jpg" />
											</a>
										</li>
										<li>
											<a href="/heroes/<?=$the_heroes[5]?>/">
												<img src="<?php bloginfo('stylesheet_directory'); ?>/images/img-s.jpg" />
											</a>
										</li>
									</ul>
								</div>
							</div>
						
						<? else: ?>
						
						<div class="box" style="margin-bottom:10px;">
							<img src="<?php bloginfo('template_url'); ?>/images/img-<?=$hero[0]?>.jpg" alt="Heroes' Letter" class="img-<?=$hero[0]?>"  />
						</div>
						<a href="/journey/<?=$name[2]?>/" title="And The '<?=ucwords($hero[0])?>' Stands For"><img src="<?php bloginfo('template_url'); ?>/images/standsfor-<?=$hero[0]?>.png" alt="" /></a>
						
						<? endif; ?>
					</li>
					<li>
						<? if (empty($name[2])): ?>
						
						<div class="box journey-right">
							<div class="about-block">
								<a href="/heroes/<?=$hero[4]?>/">
								<?php 
								$the_query = new WP_Query( 'p=81&post_type=page' );
								while ( $the_query->have_posts() ) { 
									$the_query->the_post(); 
									echo '<p>'.substr($the_query->posts[0]->post_content,0,555).'</p>';
								}
								?>
								</a>
							</div>
						</div>
						
						<? else: ?>
						
						<? if ($hero[4] == 'home'): ?>
							<a href="/heroes/">
						<? else: ?>
							<a href="/heroes/<?=$hero[4]?>/">
						<? endif; ?>
							<div class="box" style="background:<?=$hero[2]?> url(<?php bloginfo('stylesheet_directory'); ?>/images/journeyright-<?=$hero[0]?>.png) no-repeat 0 0;"></div>
						</a>
						
						<? endif; ?>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- /gallery-box -->
	
	<? if (empty($name[2])): ?>
	
	<div class="twocolumns">
		<div id="content">
			<div class="dates-block">
				<h2>GET READY CAUSE HERE WE COME!</h2>
				<div class="boxes">
					<div class="box">
						<span class="month"></span>
						<strong class="name">NASHVILLE</strong>
						<a href="#">Heroes &gt;</a>
					</div>
					<div class="box brown">
						<span class="month"></span>
						<strong class="name">MEMPHIS</strong>
						<a href="#">Heroes &gt;</a>
					</div>
					<div class="box blue">
						<span class="month"></span>
						<strong class="name"><span>PALMS</span> <span>SPRINGS</span></strong>
						<a href="#">Heroes &gt;</a>
					</div>
					<div class="box orange">
						<span class="month"></span>
						<strong class="name">MALIBU</strong>
						<a href="#">Heroes &gt;</a>
					</div>
				</div>
			</div>
			<div class="two-cols">
				<div class="post-col">
					<div class="post">
						<?php query_posts('post_type=post&posts_per_page=1'); ?>
						<?php if (have_posts()) : ?>
							<?php while (have_posts()) : the_post(); ?>
								<div class="author-box">
									<?php $user_info = get_userdata($post->post_author); ?>
									<span class="feature"><span>FEATURE</span></span>
									<a href="<?php echo $user_info-> user_url; ?>" class="img-box"><?php echo get_avatar($post->post_author, 102); ?></a>
									
									<div class="about">
										<p><a href="<?php echo $user_info-> user_url; ?>"><?php echo $user_info->user_firstname . ' ' . $user_info->user_lastname; ?></a> <?php echo $user_info->user_description; ?></p>
										<a href="<?php echo $user_info-> user_url; ?>" class="link-more">More About <?php echo $user_info->user_firstname; ?> &gt;</a>
									</div>
								</div>
								<div class="post-content">
										<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
										<?php if(get_post_meta(get_the_ID(), 'subtitle', true)){ ?><h3><?php echo get_post_meta(get_the_ID(), 'subtitle', true);?></h3><?php } ?>
										<span class="info">Posted on <strong><?php the_time('F jS'); ?></strong> by <strong><?php echo $user_info->user_firstname . ' ' . $user_info->user_lastname; ?></strong> | currently <?php comments_popup_link('No commenting', '1 commenting', '% commenting'); ?></span>
										<?php the_content(); ?>
										<div class="comments">
											<span class="number"><?php comments_popup_link('Comments (0)', 'Comments (1)', 'Comments (%)'); ?> <strong>Tags:</strong></span>
											<?php the_tags('<ul class="tag-list"><li>', ',</li><li>', '</li></ul>'); ?>
										</div>
										<div class="social-box">
											<ul class="social-comments">
												<li><?php comments_popup_link('No comments', '1 comment)', '% comments', 'comments-icon'); ?></li>
												<li><span class='st_facebook_hcount' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>' displayText='share'></span></li>
												<li><span class='st_twitter_hcount' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>' displayText='share'></span></li>
											</ul>
											<div class="rating-box">
												<span>Rate:</span>
											</div>
											<?php if(function_exists('the_ratings')) { the_ratings(); } ?>
										</div>
								</div>
							<?php endwhile; ?>
						<?php else: ?>
							<h2>Not Found</h2>
							<p>Sorry, but you are looking for something that isn't here.</p>
						<?php endif; ?>
						<?php wp_reset_query(); ?>
					</div>
				</div>
				<?php get_sidebar('2'); ?>
			</div>
		</div>
		<?php get_sidebar(); ?>
	</div>
	<!-- /two-columns -->
	
	<? else: ?>
	
	<div class="threecolumns">
		<div id="content">
			<?php if (have_posts()) : ?>
				<?php while (have_posts()) : the_post(); ?>
				<div class="c1">
					<div class="form-block">
						<?php the_content(); ?>
					</div>
				</div>
				<?php endwhile; ?>
			<?php else: ?>
				<h2>Not Found</h2>
				<p>Sorry, but you are looking for something that isn't here.</p>
			<?php endif; ?>
		</div>
		<div class="aside">
			<img src="<?php bloginfo('stylesheet_directory'); ?>/images/thumb-<?=$hero[0]?>.jpg" alt="<?=ucwords($name[2])?>" class="thumb" />
		</div>
		<div id="sidebar">
		</div>
	</div>
	<!-- /three-columns -->
	
	<? endif; ?>
	
</div>

<?php get_footer(); ?>

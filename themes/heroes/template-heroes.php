<?php
/*
Template Name: Heroes Template
*/
?>

<?php get_header(); ?>

<?
// Set up page values
$name = explode('/', $_SERVER["REQUEST_URI"]);
$hero = $$name[2];
//$name = ucwords($hero);

if (empty($name[2])){
	$next = '<a href="/journey/diana/" class="link-next">next</a>';
	$prev = '';
}
else {
	if ($hero[5]) { $prev = '<a href="/journey/'.$hero[5].'/" class="link-prev">prev</a>'; } else { $prev = ''; }
	if ($hero[5] == 'home') { $prev = '<a href="/journey/" class="link-prev">prev</a>'; }
	if ($hero[6]) { $next = '<a href="/journey/'.$hero[6].'/" class="link-next">next</a>'; } else { $next = ''; }
}

?>

<div id="main">
	<div class="gallery-box" <? if (empty($name[2])) { echo 'style="padding-bottom:40px;"'; } ?>>
		<div class="gallery">
		
			<?=$prev?>
			<?=$next?>
			<div class="gallery-holder">
				<ul>
					<li>
						<? if (empty($name[2])): ?>
						
						<div class="box heroes-left">
							<a href="/journey/<?=$the_heroes[5]?>/">
								<img src="<?php bloginfo('template_url'); ?>/images/img-heroes.png" />
							</a>
						</div>
						
						<? else: ?>
						
						<? if ($hero[5] == 'home'): ?>
							<a href="/journey/">
						<? else: ?>
							<a href="/journey/<?=$hero[5]?>/">
						<? endif; ?>
						<div class="box" style="background-color:<?=$hero[2];?>">
							<ul class="add-nav">
								<li><span><?=$hero[1][0];?></span></li>
								<li><span><?=$hero[1][1];?></span></li>
								<li><span><?=$hero[1][2];?></span></li>
							</ul>
						</div>
						</a>
						
						<? endif; ?>
					</li>
					<li>
						<? if (empty($name[2])): ?>
						
							<div class="box">
								<div class="rotator">
									<ul>
										<li class="show" style="left:-31px; top:20px;">
											<a href="/heroes/<?=$the_heroes[0]?>/"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/img-h.jpg" /></a>
										</li>
										<li style="left:-31px;">
											<a href="/heroes/<?=$the_heroes[1]?>/"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/img-e.jpg" /></a>
										</li>
										<li>
										<a href="/heroes/<?=$the_heroes[2]?>/">
											<img src="<?php bloginfo('stylesheet_directory'); ?>/images/img-r.jpg" />
										</a>
										</li>
										<li>
										<a href="/heroes/<?=$the_heroes[3]?>/">
											<img src="<?php bloginfo('stylesheet_directory'); ?>/images/img-o.jpg" />
										</a>
										</li>
										<li>
											<a href="/heroes/<?=$the_heroes[4]?>/">
												<img src="<?php bloginfo('stylesheet_directory'); ?>/images/img-ee.jpg" />
											</a>
										</li>
										<li>
											<a href="/heroes/<?=$the_heroes[5]?>/">
												<img src="<?php bloginfo('stylesheet_directory'); ?>/images/img-s.jpg" />
											</a>
										</li>
									</ul>
								</div>
							</div>
						
						<? else: ?>
						
						<div class="box" style="margin-bottom:10px;">
							<img src="<?php bloginfo('template_url'); ?>/images/img-<?=$hero[0]?>.jpg" alt="Heroes' Letter" class="img-<?=$hero[0]?>"  />
						</div>
						<a href="/heroes/<?=$name[2]?>/" title="<?=ucwords($name[2])?>'s Journey"><img src="<?php bloginfo('template_url'); ?>/images/readstory-<?=$hero[0]?>.png" alt="" /></a>
						
						<? endif; ?>
					</li>
					<li>
						<? if (empty($name[2])): ?>

						<div class="box heroes-right">
							<div class="about-block">
								<a href="/journey/<?=$the_heroes[0]?>/">
								<?php 
								$the_query = new WP_Query( 'p=96&post_type=page' );
								while ( $the_query->have_posts() ) { 
									$the_query->the_post(); 
									echo '<p>'.substr($the_query->posts[0]->post_content,0,555).'</p>';
								}
								?>
								</a>
							</div>
						</div>
						
						<? else: ?>

						<? if ($hero[6] == 'home'): ?>
							<a href="/journey/">
						<? else: ?>
							<a href="/journey/<?=$hero[6]?>/">
						<? endif; ?>
						<div class="box" style="background-color:<?=$hero[3];?>">
							<div class="about-block heroes">
								<p><?=$hero[7]?></p>
							</div>
						</div>
						</a>
						
						<? endif; ?>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- /gallery-box -->
	
	<div class="twocolumns">
		<div id="content">
			<div class="dates-block">
				<h2>GET READY CAUSE HERE WE COME!</h2>
				<div class="boxes">
					<div class="box">
						<span class="month"></span>
						<strong class="name">NASHVILLE</strong>
						<a href="#">Heroes &gt;</a>
					</div>
					<div class="box brown">
						<span class="month"></span>
						<strong class="name">MEMPHIS</strong>
						<a href="#">Heroes &gt;</a>
					</div>
					<div class="box blue">
						<span class="month"></span>
						<strong class="name"><span>PALMS</span> <span>SPRINGS</span></strong>
						<a href="#">Heroes &gt;</a>
					</div>
					<div class="box orange">
						<span class="month"></span>
						<strong class="name">MALIBU</strong>
						<a href="#">Heroes &gt;</a>
					</div>
				</div>
			</div>
			<div class="two-cols">
				<div class="post-col">
					<div class="post">
						<?php query_posts('post_type=post&posts_per_page=1'); ?>
						<?php if (have_posts()) : ?>
							<?php while (have_posts()) : the_post(); ?>
								<div class="author-box">
									<?php $user_info = get_userdata($post->post_author); ?>
									<span class="feature"><span>FEATURE</span></span>
									<?php echo get_avatar($post->post_author, 102); ?>
									
									<div class="about">
										<p><?php echo $user_info->user_firstname . ' ' . $user_info->user_lastname; ?> <?php echo $user_info->user_description; ?></p>
									</div>
								</div>
								<div class="post-content">
										<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
										<?php if(get_post_meta(get_the_ID(), 'subtitle', true)){ ?><h3><?php echo get_post_meta(get_the_ID(), 'subtitle', true);?></h3><?php } ?>
										<span class="info">Posted on <strong><?php the_time('F jS'); ?></strong> by <strong><?php echo $user_info->user_firstname . ' ' . $user_info->user_lastname; ?></strong> | currently <?php comments_popup_link('No commenting', '1 commenting', '% commenting'); ?></span>
										<?php the_content(); ?>
										<div class="comments">
											<span class="number"><?php comments_popup_link('Comments (0)', 'Comments (1)', 'Comments (%)'); ?> <strong>Tags:</strong></span>
											<?php the_tags('<ul class="tag-list"><li>', ',</li><li>', '</li></ul>'); ?>
										</div>
										<div class="social-box">
											<ul class="social-comments">
												<li><?php comments_popup_link('No comments', '1 comment)', '% comments', 'comments-icon'); ?></li>
												<li><span class='st_facebook_hcount' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>' displayText='share'></span></li>
												<li><span class='st_twitter_hcount' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>' displayText='share'></span></li>
											</ul>
											<div class="rating-box">
												<span>Rate:</span>
											</div>
											<?php if(function_exists('the_ratings')) { the_ratings(); } ?>
										</div>
								</div>
							<?php endwhile; ?>
						<?php else: ?>
							<h2>Not Found</h2>
							<p>Sorry, but you are looking for something that isn't here.</p>
						<?php endif; ?>
						<?php wp_reset_query(); ?>
					</div>
				</div>
				<?php get_sidebar('2'); ?>
			</div>
		</div>
		<?php get_sidebar(); ?>
	</div>
	
</div>

<?php get_footer(); ?>

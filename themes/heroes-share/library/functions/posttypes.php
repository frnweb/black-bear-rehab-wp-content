<?php
// ---------------------  Custom Post Types --------------------------------
// Stories
function post_type_stories() {
register_post_type(
'stories', 
array('label' => __('Stories'), 
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true, 
    'query_var' => true,
    'menu_position' => 5,
    'has_archive' => true,
    'rewrite' => array( 'slug' => 'stories', 'with_front' => false ),
    'supports' => array('title','editor','comments','thumbnail'),
));
register_taxonomy( 'Categories', 'stories', array( 'hierarchical' => true, 'label' => __('Cities') ) ); 
}
add_action('init', 'post_type_stories');
?>
<?php get_header(); global $cfs; ?>

<div id="main">
	<div id="blog">
	
		<div id="slidecontainer" class="cycler">
			<img src="/wp-content/themes/heroes-share/images/slides/1_ERIC_StoryJPEGS.jpg" />
			<img class="cycle" src="/wp-content/themes/heroes-share/images/slides/2_NICK_StoryJPEGS.jpg" />
			<img class="cycle" src="/wp-content/themes/heroes-share/images/slides/3_ERICA_StoryJPEGS.jpg" />
			<img class="cycle" src="/wp-content/themes/heroes-share/images/slides/4_GREG_StoryJPEGS.jpg" />
			<img class="cycle" src="/wp-content/themes/heroes-share/images/slides/5_TAR_StoryJPEGS.jpg" />
			<img class="cycle" src="/wp-content/themes/heroes-share/images/slides/6_WADE_StoryJPEGS.jpg" />
			<img class="cycle" src="/wp-content/themes/heroes-share/images/slides/7_ELLISHA_StoryJPEGS.jpg" />
			<img class="cycle" src="/wp-content/themes/heroes-share/images/slides/8_SEAN_StoryJPEGS.jpg" />
		</div>

		<div class="subnav">
			<ul>
				<li><a href="/share-it-with-sean/">HOME</a> &nbsp;&nbsp; | &nbsp;&nbsp;</li>
				<li><a href="/share-it-with-sean/stories/" class="active">STORIES</a> &nbsp;&nbsp; | &nbsp;&nbsp;</li>
				<li><a href="/share-it-with-sean/blog/">SEAN'S BLOG</a></li>
			</ul>
		</div>
		
		<?php query_posts('post_type=stories&posts_per_page=10&paged='.$paged);?>
		<?php if (have_posts()) : ?>
		<div class="twocolumns twocolumns-2">
			<div id="content">
				<div class="two-cols">
					<div class="post-col">
						<?php while (have_posts()) : the_post(); ?>
							<div class="post">
								<div class="author-box">
									<?php $user_info = get_userdata($post->post_author); ?>
									<?php 
									if (has_post_thumbnail())
										the_post_thumbnail(array(102,102)); 
									else
										echo '<img src="/wp-content/themes/heroes-share/style/images/story-hero-thumb-default.jpg" />';
									?>
									
									<div class="about">
										<!--<p class="day">day<span><?php //echo $cfs->get('day_number', get_the_ID()); ?></span></p>-->
										<p class="city"><?php echo $cfs->get('city', get_the_ID()); ?></p>
									</div>
								</div>
								<div class="post-content">
									<h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
									<?php if(get_post_meta(get_the_ID(), 'subtitle', true)){ ?><h3><?php echo get_post_meta(get_the_ID(), 'subtitle', true);?></h3><?php } ?>
									<span class="info">Posted on <strong><?php the_time('F jS'); ?></strong> by <strong>Sean</strong> | currently <?php comments_popup_link('No comments', '1 comment', '% comments'); ?></span>
									<?php the_content(); ?> 

									<div class="social-box">
										<ul class="social-plagin">
											<li><a style="margin-top:3px;" href="<?php the_permalink()?>#respond"><img src="<?php bloginfo('template_url'); ?>/images/btn-comment.gif" width="89" height="20" alt="image description" /></a></li>
											<li><a href="http://twitter.com/share" class="twitter-share-button" data-count="none">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script></li>
											<!--<li><iframe src="http://www.facebook.com/plugins/like.php?app_id=251273934887780&amp;href&amp;send=false&amp;layout=button_count&amp;width=100&amp;show_faces=true&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:100px; height:21px;" allowTransparency="true"></iframe></li>-->
										</ul>
										<div class="rating-box">
											<span>Rate:</span>
										</div>
										<?php if(function_exists('the_ratings')) { the_ratings(); } ?>
									</div>
								</div>
							</div>
						<?php endwhile; ?>
						<div class="navigation">
							<div class="next"><?php next_posts_link('Older Entries &raquo;') ?></div>
							<div class="prev"><?php previous_posts_link('&laquo; Newer Entries') ?></div>
						</div>
					</div>
				</div>
			</div>
			<div id="sidebar">
				<?php dynamic_sidebar('Story Sidebar'); ?>
			</div>
		</div>
		<?php endif; ?>
		
<?php get_footer(); ?>
					</div> <!-- blog -->
				</div> <!-- main -->
						<div id="footer" class="clearfix">
							<ul class="item-list">
								<li>&copy; <?php print date('Y'); ?> Heroes In Recovery</li>
								<li>Encouraged by Heroes In Recovery</li>
								<li>1.888.312.4220</li>
								<!--
								<li><a href="#">Site Map</a></li>
								
								<li><a href="<?php echo get_permalink(TermsofUsePageID); ?>">Terms of Use</a></li>
								<li><a href="<?php echo get_permalink(PrivacyPolicyPageID); ?>">Privacy Policy</a></li>
								-->
							</ul>

							<ul class="nav" style="padding-top:20px;">
								<li><a href="/web_content/privacy_policy.pdf" target="_blank">Privacy Policy</a></li>
								<li><a href="/web_content/privacy_guide.pdf" target="_blank">Privacy Guide</a></li>
								<li><a href="/web_content/terms_of_use.pdf" target="_blank">Terms of Use</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php wp_footer(); ?>
	</body>
</html>
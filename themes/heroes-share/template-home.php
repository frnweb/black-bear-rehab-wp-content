<?php
/*
Template Name: Home
*/
?>
<?php get_header(); ?>

<div id="main">
	<div id="home" class="share">
	
		<div id="slidecontainer">
			<div class="slide">
				<img src="<?php bloginfo('template_directory'); ?>/style/images/home-graphic.jpg" />
				<div class="details">
					<p class="welcome">I'm Sean Morrison. Welcome to my...</p>
					<h1>Road Trip!</h1>
					<a href="stories/" class="stories">stories<span>></span></a>
					<a href="blog/" class="blog">blog<span>></span></a>
				</div>
			</div>
		</div>
		
		<aside>
			<p class="date">JULY 11 - AUGUST 8</p>
			<p class="where">Colorado to<br />New York City <br />to Nashville Tennessee</p>
			<p class="text">and a few spots<br />in between!</p>
			<p class="videotext">Watch My Video</p>
			<div class="video">
				<a href="/sean-video.html" class="hp-video"><img src="/wp-content/themes/heroes-share/style/images/video.jpg"></a>
			</div>
		</aside>
		<article>
			
			<?php if (have_posts()) : ?>
				<?php while (have_posts()) : the_post(); ?>
						<?php the_content(); ?>
				<?php endwhile; ?>
			<?php else: ?>
				<h2>Not Found</h2>
				<p>Sorry, but you are looking for something that isn't here.</p>
			<?php endif; ?> 
		</article>
		
		<div id="map" class="clearfix">
			
			<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDSbK72cbpwnj7OVTp2nPmDhfAP5Xt1GDo&sensor=false"></script>
			<script type="text/javascript">
			var directionsDisplay;
			var directionsService = new google.maps.DirectionsService();
			var map;

			function initialize() {
			  directionsDisplay = new google.maps.DirectionsRenderer();
			  var us = new google.maps.LatLng(39.8106460, -98.5569760);
			  var myOptions = {
				zoom: 4,
				mapTypeId: google.maps.MapTypeId.ROADMAP,
				center: us
			  }
			  map = new google.maps.Map(document.getElementById("map"), myOptions);
			  directionsDisplay.setMap(map);
			}

			function calcRoute() {
			  var start = 'Lansing, MI';
			  var end = 'Nashville, TN';
			  //var waypoints = []
			  //var checkboxArray = waypts[];
			  /*
			  for (var i = 0; i < waypts.length; i++) {
				  waypts.push({
					  location:waypts[i],
					  stopover:true
				  });
			  }
			  */

			  var request = {
				  origin: start,
				  destination: end,
				  waypoints: [
					{
					  location:"Louisville, CO",
					  stopover:true
					},
					{
					  location:"New York City, NY",
					  stopover:true
					},
					{
					  location:"Washington, DC",
					  stopover:true
					},
					{
					  location:"Berkeley Springs, WV",
					  stopover:true
					},
					{
					  location:"Roanoke, VA",
					  stopover:true
					},
					{
					  location:"Knoxville, TN",
					  stopover:true
					},
					{
					  location:"Jackson, TN",
					  stopover:true
					},
					{
					  location:"Memphis, TN",
					  stopover:true
					}],
				  optimizeWaypoints: false,
				  travelMode: google.maps.TravelMode.DRIVING
			  };
			  directionsService.route(request, function(response, status) {
				if (status == google.maps.DirectionsStatus.OK) {
				  directionsDisplay.setDirections(response);
				  var route = response.routes[0];
				 // var summaryPanel = document.getElementById("directions_panel");
				  //summaryPanel.innerHTML = "";
				  // For each route, display summary information.
				  /*
				  for (var i = 0; i < route.legs.length; i++) {
					var routeSegment = i+1;
					summaryPanel.innerHTML += "<b>Route Segment: " + routeSegment + "</b><br />";
					summaryPanel.innerHTML += route.legs[i].start_address + " to ";
					summaryPanel.innerHTML += route.legs[i].end_address + "<br />";
					summaryPanel.innerHTML += route.legs[i].distance.text + "<br /><br />";
				  }
				  */
				}
			  });
			}
			
			jQuery(document).ready(function() {
				initialize();
				calcRoute();
			});
			</script>			
				
		</div>
		
	</div>
	
	<!--
	<div id="page-sidebar">
		<?php //get_sidebar('1'); ?>
	</div>
	-->
</div>

<?php get_footer(); ?>
<?php 
get_header();
global $cfs;
query_posts('posts_per_page=5&paged='.$paged);
?>

<div id="main">
	<div id="blog">
	
		<div id="slidecontainer">
			<div class="slide">
				<img src="<?php bloginfo('template_directory'); ?>/style/images/home-graphic.jpg" />
				<div class="details">
					<?php
					query_posts('post_type=post&posts_per_page=1&orderby=date&order=DESC');
					if (have_posts()) :
					?>
						<p class="welcome1">Welcome to</p>
						<p class="welcome2">my travel...</p>
						<p class="title">Blog</p>
						<?php
						while (have_posts()) : the_post();
							$day_num = $cfs->get('day_number', get_the_ID());
							if ($day_num == '' || $day_num == 0) { 
								echo '<div class="date"><p class="before">The Journey Begins July 11</p></div>';
							}
							else
								echo '<div class="date"><p>Day '. $day_num .'</p></div>';
						endwhile;
					else:
						echo '<div class="date"><p class="before">The Journey Begins July 11</p></div>';
					endif;
					wp_reset_query();
					?>
				</div>
			</div>
		</div>

		<div class="subnav">
			<ul>
				<li><a href="/share-it-with-sean/">HOME</a> &nbsp;&nbsp; | &nbsp;&nbsp;</li>
				<li><a href="/share-it-with-sean/stories/">STORIES</a> &nbsp;&nbsp; | &nbsp;&nbsp;</li>
				<li><a href="/share-it-with-sean/blog/" class="active">SEAN'S BLOG</a></li>
			</ul>
		</div>
		
		<?php query_posts('post_type=post&posts_per_page=10&paged='.$paged);?>
		<?php if (have_posts()) : ?>
		<div class="twocolumns twocolumns-2">
			<div id="content">
				<div class="two-cols">
					<div class="post-col">
						<?php while (have_posts()) : the_post(); ?>
							<div class="post">
								<div class="author-box">
									<?php $user_info = get_userdata($post->post_author); ?>
									<img src="/wp-content/themes/heroes-share/images/sean_avatar.jpg" />
									<?php
									$day_num = $cfs->get('day_number', get_the_ID());
									if ($day_num == '' || $day_num == 0): ?>
										<div class="about">
											<p class="city"><?php echo $cfs->get('city', get_the_ID()); ?></p>
										</div>
									<?php else: ?>
										<div class="about">
											<!--<p class="day">day<span><?php //echo $cfs->get('day_number', get_the_ID()); ?></span></p>-->
											<p class="city"><?php echo $cfs->get('city', get_the_ID()); ?></p>
										</div>
									<?php endif; ?>
								</div>
								<div class="post-content">
									<h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
									<?php if(get_post_meta(get_the_ID(), 'subtitle', true)){ ?><h3><?php echo get_post_meta(get_the_ID(), 'subtitle', true);?></h3><?php } ?>
									<span class="info">Posted on <strong><?php the_time('F jS'); ?></strong> by <strong><?php echo get_the_author(); ?></strong> | currently <?php comments_popup_link('No comments', '1 comment', '% comments'); ?></span>
									<?php //$content = get_the_content(); $content = substr($content, 0, 500); echo $content; ?>
									<?php the_excerpt(); ?> <!--(<a href="<?php //the_permalink(); ?>">more</a>)-->

									<div class="social-box">
										<ul class="social-plagin">
											<li><a style="margin-top:3px;" href="<?php the_permalink()?>#respond"><img src="<?php bloginfo('template_url'); ?>/images/btn-comment.gif" width="89" height="20" alt="image description" /></a></li>
											<li><a href="http://twitter.com/share" class="twitter-share-button" data-count="none">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script></li>
											<!--<li><iframe src="http://www.facebook.com/plugins/like.php?app_id=251273934887780&amp;href&amp;send=false&amp;layout=button_count&amp;width=100&amp;show_faces=true&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:100px; height:21px;" allowTransparency="true"></iframe></li>-->
										</ul>
										<div class="rating-box">
											<span>Rate:</span>
										</div>
										<?php if(function_exists('the_ratings')) { the_ratings(); } ?>
									</div>
								</div>
							</div>
						<?php endwhile; ?>
						<div class="navigation">
							<div class="next"><?php next_posts_link('Older Entries &raquo;') ?></div>
							<div class="prev"><?php previous_posts_link('&laquo; Newer Entries') ?></div>
						</div>
					</div>
				</div>
			</div>
			<div id="sidebar">
				<?php dynamic_sidebar('Story Blog Sidebar'); ?>
			</div>
		</div>
		<?php endif; ?>
		
<?php get_footer(); ?>
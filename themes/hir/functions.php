<?php


/**
 * Enque Scripts & Styles
 */
function sparks_scripts() {
    // Font Awesome
    wp_enqueue_style( 'font-awesome', 'http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css', array(), '4.2.0' );
    // Sticky Nav
    wp_enqueue_script( 'sticky', get_template_directory_uri() . '/js/vendor/sticky.js', array('jquery'), '2.6.2', false );
    // Mondernizr
    wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/js/vendor/modernizr-2.6.2-respond-1.1.0.min.js', array(), '2.6.2', false );
    // Fancybox
    wp_enqueue_script( 'fancy', get_template_directory_uri() . '/js/vendor/fancy/jquery.fancybox.pack.js', array( 'jquery' ), '19.0', true );
    wp_enqueue_style( 'fancy', get_template_directory_uri() . '/js/vendor/fancy/jquery.fancybox.css', '19.0', true );
    // Scripts only for front page
    if( is_front_page() ) {
        // Image Grid
        wp_enqueue_script( 'grid', get_template_directory_uri() . '/js/vendor/grid/js/jquery.gridrotator.js', array('jquery'), '2.6.2', false );
        wp_enqueue_style( 'grid', get_template_directory_uri() . '/js/vendor/grid/css/style.css', array(), '4.2.0' );
    }  
    // Form Validation
    if( is_page_template('page-share.php') ) {
        wp_enqueue_script( 'validation', get_template_directory_uri() . '/js/vendor/jquery.validate.min.js', array( 'jquery' ), '19.0', true );
    }
    // Custom Scripts
    wp_enqueue_script( 'sparks-js', get_template_directory_uri() . '/js/sparks.js', array( 'jquery' ), '1.2.0', true );      
    wp_enqueue_script( 'reveal-js', get_template_directory_uri() . '/js/jquery.reveal.js', array( 'jquery', 'sparks-js' ), true );      
}
add_action( 'wp_enqueue_scripts', 'sparks_scripts' );

/**
 * Registering Menus
 */
function sparks_register_menus() {
    register_nav_menus( array(
            'header-menu' => __( 'Header Menu', 'sparks' ),
            'footer-menu' => __( 'Footer Menu', 'sparks' ),
        )
    );
}
add_action( 'init', 'sparks_register_menus' );

/**
 * Image Sizes
 *
 * Register Custom Image Sizes
 */
if ( function_exists( 'add_image_size' ) ) {
    add_image_size( 'sub-page', 1200, 00, true );
    add_image_size( 'loop-thumb', 300, 300, true );
    add_image_size( 'hero-image', 210, 210, true );
    add_image_size( 'avatar', 96, 96, true );
}

/**
* Hide editor on specific pages.
*
*/
add_action( 'admin_init', 'hide_editor' );
    function hide_editor() {
        // Get the Post ID.
        $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
        if( !isset( $post_id ) ) return;
        // Hide the editor on the page titled 'Homepage'
        $homepgname = get_the_title($post_id);
        if($homepgname == 'Homepage'){
        remove_post_type_support('page', 'editor');
        }
        // Hide the editor on a page with a specific page template
        // Get the name of the Page Template file.
        $template_file = get_post_meta($post_id, '_wp_page_template', true);
        if($template_file == 'page-about.php' || $template_file == 'page-contact.php' || $template_file == 'page-event.php' || $template_file == 'page-events-landing.php' || $template_file == 'page-race-landing.php' || $template_file == 'page-share.php'){ // the filename of the page template
        remove_post_type_support('page', 'editor');
    }
} 

/**
 * Add Theme Support
 *
 * A support for things like featured images, etc.
 */
    
// Thumbnails
add_theme_support( 'post-thumbnails' );

add_filter( 'post_thumbnail_html', 'thesis_to_featured' );
function thesis_to_featured( $html ) {
    global $post;

    // Get the thesis post image
    $thesis_img = get_post_meta( $post->ID, 'thesis_post_image' );
    
    // Is there a featured image?
    if ( empty( $html ) ) {
        // Do we have a thesis image to work with?
        if( !empty( $thesis_img ) ) {
            // Overwrite the $html variable with the thesis image
            $html = '<img src="' . $thesis_img[0] . '" />';
        } elseif(preg_match('/<img[^>]+>/i', $post->post_content)) {
            preg_match_all('/<img[^>]+>/i', $post->post_content, $result);
            $html = $result[0][0];
        } else {
            $html = '';
        }
    }        

    return $html;
}    

// Post Formats
// Uncomment to activate
/*
add_theme_support(
    'post-formats',
    array(
        'aside',
        'gallery',
        'link',
        'image',
        'quote',
        'status',
        'video',
        'audio',
        'chat' 
    )
)
*/

// Custom Background
// Uncomment to activate
/*
$defaults = array(
    'default-color'          => '#',
    'default-image'          => '',
    'wp-head-callback'       => 'sparks_custom_background_callback',
    'admin-head-callback'    => '',
    'admin-preview-callback' => ''
);
add_theme_support( 'custom-background', $defaults );
*/

// Custom Header
// Uncomment to activate
/*
$defaults = array(
    'default-image'          => '',
    'random-default'         => false,
    'width'                  => 0,
    'height'                 => 0,
    'flex-height'            => false,
    'flex-width'             => false,
    'default-text-color'     => '',
    'header-text'            => true,
    'uploads'                => true,
    'wp-head-callback'       => '',
    'admin-head-callback'    => '',
    'admin-preview-callback' => '',
);
add_theme_support( 'custom-header', $defaults );    
*/


/**
 * Translations
 */
load_theme_textdomain( 'sparks' , get_template_directory() . '/languages' );

/**
 * Custom Post Excerpt Length.
 */
function sparks_custom_excerpt_length( $length ) {
    return 18;
}
add_filter( 'excerpt_length', 'sparks_custom_excerpt_length', 999 );

/**
 * Remove "..." from excerpt
 */
function sparks_remove_dots_from_excerpt( $more ) {
    return ' ...';
}
add_filter('excerpt_more', 'sparks_remove_dots_from_excerpt');
/**
 * Button Shortcode
 */ 
function displayButton($atts) {
    $a = shortcode_atts( array(
        'title' => '',
        'url' => '',
        'type' => '',
        'target' => '',
        'color' => ''
    ), $atts );

    if($a['color']) {
        $classes = 'hir-btn-' . $a['color'];
    }    

    if($a['type'] == 'slim') {
        $classes .= ' slim'; 
    }

    $html = '<a href="'.$a['url'].'" title="'.$a['title'].'" class="hir-btn ' . $classes . '" target="' . $a['target'] . '">'.$a['title'].'</a>';

    return $html;
}
add_shortcode('hirButton', 'displayButton');

function template_chooser($template)   
{    
  global $wp_query;   
  $post_type = get_query_var('post_type');   
  if( $wp_query->is_search && $post_type == 'stories' )   
  {
    return locate_template('archive-stories.php');  //  redirect to archive-stories.php
  }   
  return $template;   
}
add_filter('template_include', 'template_chooser');    
/**
 * Custom Post Types
 *
 * Place all custom post types here.
 */

add_action( 'init', 'register_cpt_stories' );

function register_cpt_stories() {

    //Stories Post Type & Taxonomies
    register_taxonomy (  
        'story_category',  
        array('staff'),  
        array (  
            'hierarchical' => true,  
            'label' => 'Categories',  
            'query_var' => true,  
            'rewrite' => array(
                'slug' => 'story_category',
                'with_front' => false
                ),
        )  
    );    

    $labels = array( 
        'name' => _x( 'Stories', 'stories' ),
        'singular_name' => _x( 'Story', 'stories' ),
        'add_new' => _x( 'Add New', 'stories' ),
        'add_new_item' => _x( 'Add New Story', 'stories' ),
        'edit_item' => _x( 'Edit Story', 'stories' ),
        'new_item' => _x( 'New Story', 'stories' ),
        'view_item' => _x( 'View Story', 'stories' ),
        'search_items' => _x( 'Search Stories', 'stories' ),
        'not_found' => _x( 'No stories found', 'stories' ),
        'not_found_in_trash' => _x( 'No stories found in Trash', 'stories' ),
        'parent_item_colon' => _x( 'Parent Story:', 'stories' ),
        'menu_name' => _x( 'Stories', 'stories' ),
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => false,
        
        'supports' => array( 'title', 'editor', 'thumbnail', 'revisions', 'comments' ),
        'taxonomies' => array( 'story_category' ),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 20,
        'menu_icon' => 'dashicons-book-alt',
        'show_in_nav_menus' => false,
        'publicly_queryable' => true,
        'exclude_from_search' => true,
        'has_archive' => 'stories',
        'query_var' => true,
        'can_export' => true,
        'rewrite' => array(
            'slug' => 'stories',
            'with_front' => false
            ),
        'capability_type' => 'post'
    );

    register_post_type( 'stories', $args );
}

function register_cpt_news() {

    register_taxonomy (  
        'news_category',  
        array('news'),  
        array (  
            'hierarchical' => true,  
            'label' => 'Categories',  
            'query_var' => true,  
            'rewrite' => true,
        )  
    );    

    $labels = array( 
        'name' => _x( 'News', 'news' ),
        'singular_name' => _x( 'News', 'news' ),
        'add_new' => _x( 'Add New', 'news' ),
        'add_new_item' => _x( 'Add New News', 'news' ),
        'edit_item' => _x( 'Edit News', 'news' ),
        'new_item' => _x( 'New News', 'news' ),
        'view_item' => _x( 'View News', 'news' ),
        'search_items' => _x( 'Search News', 'news' ),
        'not_found' => _x( 'No News found', 'news' ),
        'not_found_in_trash' => _x( 'No News found in Trash', 'news' ),
        'parent_item_colon' => _x( 'Parent News:', 'news' ),
        'menu_name' => _x( 'News', 'news' ),
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => true,
        
        'supports' => array( 'title', 'editor', 'thumbnail' ),
        
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-welcome-learn-more',
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => array('with_front'=>false),
        'capability_type' => 'post',
        'taxonomies' => array('news_category')
    );

    register_post_type( 'news', $args );
}
add_action( 'init', 'register_cpt_news' );

/**
 * Settings Page
 */
add_action('admin_init', 'settings_admin_init');
function settings_admin_init(){
    register_setting( 'settings_options', 'settings_options', 'settings_options_validate' );
    add_settings_section('settings_general', 'General Settings', 'settings_section_text', 'settings');

    // Facebook URL
    add_settings_field('facebook_url', 'Facebook URL', 'settings_facebook_input', 'settings', 'settings_general');
    // Twitter URL
    add_settings_field('twitter_url', 'Twitter URL', 'settings_twitter_input', 'settings', 'settings_general');    
    // Instagram URL
    add_settings_field('instagram_url', 'Instagram URL', 'settings_instagram_input', 'settings', 'settings_general');
    // YouTube
    add_settings_field('youtube_url', 'YouTube URL', 'settings_youtube_input', 'settings', 'settings_general');
    // Phone Number
    add_settings_field('phone_number', 'Phone Number', 'settings_phone_number_input', 'settings', 'settings_general');
    // Street Address
    add_settings_field('address', 'Address', 'settings_address_input', 'settings', 'settings_general');
    // City
    add_settings_field('city', 'City', 'settings_city_input', 'settings', 'settings_general');    
    // State
    add_settings_field('state', 'State', 'settings_state_input', 'settings', 'settings_general');
    // Zip
    add_settings_field('zip', 'Zip', 'settings_zip_input', 'settings', 'settings_general');    
    // Email
    add_settings_field('email', 'Email', 'settings_email_input', 'settings', 'settings_general');        
}

function settings_section_text() {
    echo '<p>Settings related to various site sections.</p>';
}

function settings_facebook_input() {
    $options = get_option('settings_options');
    echo "<input id='settings_facebook_input' name='settings_options[facebook_url_input]' size='40' type='text' value='{$options['facebook_url_input']}' />";
}

function settings_twitter_input() {
    $options = get_option('settings_options');
    echo "<input id='settings_twitter_input' name='settings_options[twitter_url_input]' size='40' type='text' value='{$options['twitter_url_input']}' />";
}

function settings_instagram_input() {
    $options = get_option('settings_options');
    echo "<input id='settings_instagram_input' name='settings_options[instagram_url_input]' size='40' type='text' value='{$options['instagram_url_input']}' />";
}

function settings_youtube_input() {
    $options = get_option('settings_options');
    echo "<input id='settings_youtube_input' name='settings_options[youtube_url_input]' size='40' type='text' value='{$options['youtube_url_input']}' />";
}

function settings_phone_number_input() {
    $options = get_option('settings_options');
    echo "<input id='settings_phone_number_input' name='settings_options[phone_number_input]' size='40' type='text' value='{$options['phone_number_input']}' />";
}

function settings_address_input() {
    $options = get_option('settings_options');
    echo "<textarea id='settings_address_input' name='settings_options[address_input]' cols='40' rows='10'>{$options['address_input']}</textarea>";
}

function settings_city_input() {
    $options = get_option('settings_options');
    echo "<input id='settings_city_input' name='settings_options[city_input]' size='40' type='text' value='{$options['city_input']}' />";
}

function settings_state_input() {
    $options = get_option('settings_options');
    echo "<input id='settings_state_input' name='settings_options[state_input]' size='40' type='text' value='{$options['state_input']}' />";
}

function settings_zip_input() {
    $options = get_option('settings_options');
    echo "<input id='settings_zip_input' name='settings_options[zip_input]' size='40' type='text' value='{$options['zip_input']}' />";
}

function settings_email_input() {
    $options = get_option('settings_options');
    echo "<input id='settings_email_input' name='settings_options[email_input]' size='40' type='text' value='{$options['email_input']}' />";
}

function settings_options_validate($input) {
    $newinput['facebook_url_input'] = trim($input['facebook_url_input']);
    $newinput['twitter_url_input'] = trim($input['twitter_url_input']);
    $newinput['instagram_url_input'] = trim($input['instagram_url_input']);
    $newinput['youtube_url_input'] = trim($input['youtube_url_input']);
    $newinput['phone_number_input'] = trim($input['phone_number_input']);
    $newinput['address_input'] = trim($input['address_input']);
    $newinput['city_input'] = trim($input['city_input']);
    $newinput['state_input'] = trim($input['state_input']);
    $newinput['zip_input'] = trim($input['zip_input']);
    $newinput['email_input'] = trim($input['email_input']);

    return $newinput;
}

add_action('admin_menu', 'settings_admin_add_page');
function settings_admin_add_page() {
    add_options_page('Site Settings', 'Site Settings', 'manage_options', 'settings', 'settings_options_page');
}

function settings_options_page() {
?>
<div>
    <h1>Site Settings</h1>
    <p>Global site settings.</p>
    <form action="options.php" method="post">
        <?php settings_fields('settings_options'); ?>
        <?php do_settings_sections('settings'); ?>
         
        <input name="Submit" type="submit" value="<?php esc_attr_e('Save Changes'); ?>" />
    </form>
</div> 
<?php
}


//Function to get author URL for Facebook opengraph tagging in Meta
function get_frn_author_url($avatar_html){
	$avatar_matches = preg_match('/src=["|\'](.*?)["|\']/i', $avatar_html, $matches);
    if($avatar_matches) return $matches[1];
	else return "";
}



/////////////////////
////
//// CHANGES TO AUTHOR OPTIONS
////
/////////////////////

//Add more contact options to user (author) info
if(!function_exists('frn_contact_options')) {
function frn_contact_options($profile_fields) {
    // Add new fields
    $profile_fields['linkedin'] = 'LinkedIn URL';
    $profile_fields['slideshare'] = 'SlideShare Profile';
    $profile_fields['phone'] = 'Company Phone';
    
    // Remove old fields
    unset($profile_fields['aim']);
    unset($profile_fields['jabber']);
    unset($profile_fields['yim']);
    
    return $profile_fields;
}
}
add_filter('user_contactmethods', 'frn_contact_options');
remove_filter('pre_user_description', 'wp_filter_kses'); //removes HTML filter from author bios
add_filter( 'pre_user_description', 'wp_filter_post_kses' );  //adds traditional filters to bios like page content has

// BEGIN: Adds a FRN author settings section at the bottom of user profile settings
function add_frn_author_fields( $user ) {
    $author_type = esc_attr( get_the_author_meta( 'author_type', $user->ID) ); //esc_attr only needed for text fields, not checkboxes or dropdowns
    
    if(!isset($author_type)) $author_type="";
?>
    <div style="border:1px solid #ccc; padding:0 10px 10px 10px;">
    <h3><?php _e('FRN Custom Author Options'); ?></h3>
    <table class="form-table">
        <tr>
            <th><label for="author_type"><?php _e('Type of Author:'); ?></label></th>
            <td>
                <select class="regular-text" id="author_type" name="author_type">
                    <option value="" <?=($author_type=="") ? "selected " : "" ;?>></option><?php //Used numbers instead of titles to give renaming flexibility and a preferred sort order ?>
                    <option value="0" <?=($author_type=="0") ? "selected " : "" ;?>>Advocate</option>
                    <option value="1" <?=($author_type=="1") ? "selected " : "" ;?>>Partner</option>
                    <option value="2" <?=($author_type=="2") ? "selected " : "" ;?>>Blogger</option>
                </select>
            </td>
        </tr>
        <!--<tr>
            <th><label for=""><?php _e('Company Phone:'); ?></label></th>
            <td>
              <input type="text" name="" id="phone" value="<?=$phone; ?>" class="regular-text" /><span class="description"><?php _e(''); ?></span>
            </td>
        </tr>
        <tr>
            <th><label for="Address"><?php _e('Company Address'); ?></label></th>
            <td>
              <textarea rows="5" name="address" id="address" value="<?php echo $address; ?>" class="regular-text" /><span class="description"><?php _e('Address'); ?></span>
            </td>
        </tr>
        <tr>
            <th><label for="Make Author Public"><?php _e('Make Author Public'); ?></label></th>
            <td>
                <label><input type="checkbox" name="author_public" <?php if ($author_public == 'Y' ) { ?>checked="checked"<?php }?> value="Y">Yes &nbsp;</label><small class="description"><?php _e('(Activates authorship in posts & pages)'); ?></small>
            </td>
        </tr>-->
    </table>
    </div>
<?php 
}
function frn_custom_user_profile_fields( $user_id ) {
    //if ( !current_user_can( 'edit_user', $user_id ) ) //Used if we want to block certain features
        //return FALSE;
    update_user_meta( $user_id, 'author_type', $_POST['author_type'] );
    update_user_meta( $user_id, 'phone', $_POST['phone'] );
}
add_action( 'show_user_profile', 'add_frn_author_fields' );
add_action( 'edit_user_profile', 'add_frn_author_fields' );
add_action( 'personal_options_update', 'frn_custom_user_profile_fields' );
add_action( 'edit_user_profile_update', 'frn_custom_user_profile_fields' );


//change the "author" portion of url and use partner (not used since we have bloggers and partners)
//if(!function_exists('frn_change_author_permalinks')) {
//function frn_change_author_permalinks() {
//    global $wp_rewrite;
//    $wp_rewrite->author_base = 'partner/';
    //$wp_rewrite->author_structure = '/' . $wp_rewrite->author_base. '%author%'; //this removes it from the URL
//}
//}
//add_action('init','frn_change_author_permalinks');

if(!function_exists('biographical_info_tinymce')) {
function biographical_info_tinymce() {
    if ( basename($_SERVER['PHP_SELF']) == 'profile.php' || basename($_SERVER['PHP_SELF']) == 'user-edit.php' && function_exists('wp_tiny_mce') ) {
        wp_admin_css(); 
        wp_enqueue_script('utils');
        wp_enqueue_script('editor');
        do_action('admin_print_scripts');
        do_action("admin_print_styles-post-php");
        do_action('admin_print_styles');
        remove_all_filters('mce_external_plugins');

        add_filter( 'teeny_mce_before_init', create_function( '$a', '
        $a["theme"] = "advanced";
        $a["skin"] = "wp_theme";
        $a["height"] = "300";
        $a["width"] = "440";
        $a["onpageload"] = "";
        $a["mode"] = "exact";
        $a["elements"] = "description";
        $a["theme_advanced_buttons1"] = "formatselect, forecolor, bold, italic, pastetext, pasteword, bullist, numlist, link, unlink, outdent, indent, charmap, removeformat, spellchecker, fullscreen, wp_adv";
        $a["theme_advanced_buttons2"] = "underline, justifyleft, justifycenter, justifyright, justifyfull, forecolor, pastetext, undo, redo, charmap, wp_help";
        $a["theme_advanced_blockformats"] = "p,h2,h3,h4,h5,h6";
        $a["theme_advanced_disable"] = "strikethrough";
        return $a;' ) );

        wp_tiny_mce( true );
    }
}
}
add_action('admin_head', 'biographical_info_tinymce');


/////////////////////
////
//// END CHANGES TO AUTHOR OPTIONS
////
/////////////////////







//////////////       this was commented out already by Proof ////////////////
// //Events Post Type & Taxonomies
// register_taxonomy (  
//     'event_category',  
//     array('staff'),  
//     array (  
//         'hierarchical' => true,  
//         'label' => 'Categories',  
//         'query_var' => true,  
//         'rewrite' => true,
//     )  
// );


// //Events Post Type
// function post_type_events() {
//     register_post_type(
//     'events', 
//     array('label' => __('News &amp; Events'), 
//         'public' => true,
//         'publicly_queryable' => true,
//         'show_ui' => true, 
//         'query_var' => true,
//         'menu_position' => 30,
//         'rewrite' => array(
//             'slug' => 'events',
//             'with_front' => false
//             ),
//         'menu_icon' => '/wp-content/themes/heroes/images/newspaper.png',
//         'supports' => array('title','editor','thumbnail'),
//         'has_archive' => true,
//         'taxonomies' => array('event_category')
        
//     )); 
// }
// function event_categories() {
//     register_taxonomy_for_object_type('event_category', 'events');
// }
// add_action('init', 'post_type_events');
// add_action('init', 'event_categories');
?>
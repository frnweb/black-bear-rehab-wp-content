<?php get_header(); ?>
<div class="page-row row">
	<section class="page-content-wrap col-sm-8 col-sm-offset-2 page-404">
		<h1><?php _e( 'Page Not Found', 'sparks' ); ?></h1>
		<p><?php _e( 'Bummer, we couldn\'t find the page you\'re looking for. Would any of the following posts be what you\'re looking for?', 'sparks' ); ?></p>	
		<?php echo do_shortcode('[frn_related_list html="searchbox"]'); ?>
	</section>
</div><!-- /.row -->
<?php get_footer(); ?>
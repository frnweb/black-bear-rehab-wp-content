<?php
/**
 * Template Name: Events Landing
 */

get_header();
?>
<?php
// Function to check if a page has a thumbnail.
// This includes checks for blog pages


function page_has_post_thumbnial()
{
	if( is_home() )
	{

	    // Get the page ID that has been assigned to the posts page
	    $blog_page_id = get_option( 'page_for_posts' );

	    return has_post_thumbnail( $blog_page_id );

	}
	else if( is_post_type_archive( 'stories' ) || is_tax( 'story_category' ) )
	{
		// This is set to the post id for the "Stories" page
		return has_post_thumbnail( '6927' );
	}
	else
	{
		return has_post_thumbnail();
	}
}

// Get content for a page, includes support for blog page.
function page_get_field( $field )
{
	if( is_home() )
	{

	    // Get the page ID that has been assigned to the posts page
	    $blog_page_id = get_option( 'page_for_posts' );

	    return get_field( $field, $blog_page_id );

	}
	else if( is_post_type_archive( 'stories' ) || is_tax( 'story_category' ) )
	{
		// This is set to the post id for the "Stories" page
		return get_field( $field, '6927' );
	}	
	else
	{
		return get_field( $field );
	}
}

?>	
<div class="row page-top-row<?php echo (page_has_post_thumbnial()) ? ' has-img' : ' no-img'; ?>">	
	<div class="<?php echo page_has_post_thumbnial() ? 'col-sm-12' : 'col-sm-10 col-sm-offset-1' ?> page-top page-top-events">
		<img src="<?php echo get_bloginfo('template_url'); ?>/img/featured_hero_flip.png" id="events-flip">
		<div class="page-top-text-overlay events-top">			
			<div class="page-top-text-overlay-inner">
				<div class="page-top-text-overlay-inner-content">
					<div class="page-top-text-overlay-inner-content-wrap">
						<div class="events-top-month">
							<?php echo get_field('month'); ?>
						</div><!-- /.events-top-month -->
						<div class="events-top-day">
							<?php echo get_field('day'); ?>
						</div><!-- /.events-top-day -->						
						<?php 
							if(page_get_field('page_top_copy')) {
								echo page_get_field('page_top_copy'); 
							}			
						?>
						<a href="<?php echo get_field('learn_more_link'); ?>" class="hir-btn hir-btn-red slim">Learn More</a>
					</div><!-- /.page-top-text-overlay-inner-content -->
				</div><!-- /.page-top-text-overlay-inner-content -->			
			</div><!-- /.page-top-text-overlay-inner -->
		</div><!-- /.page-top-text-overlay -->		
		<?php
			the_post_thumbnail('sub-page');
		?>		
	</div><!-- /.col-sm-12 -->
</div><!-- /.row -->
<div id="home-events-row" class="row">
	<section id="home-events" class="col-sm-10 col-sm-offset-1">
		<h2 class="section-h2">Upcoming Events</h2>
		<table id="home-events-table" itemscope itemtype="http://schema.org/Event">
			<?php
			global $post;

			$get_posts = tribe_get_events(array('start_date' => date( 'Y-m-d H:i:s' ),'posts_per_page'=>5,'order'=>'asc') ); //removed 6/14/17 'eventDisplay'=>'upcoming', from array

			foreach($get_posts as $post) { setup_postdata($post); ?>
				<tr>
					<td class="home-events-date" itemprop="startDate" content="<?php echo tribe_get_start_date($post->ID, true, 'M'); ?>">
						<span class="home-events-date-month"><?php echo tribe_get_start_date($post->ID, true, 'M'); ?></span>
						<span class="home-events-date-day"><?php echo tribe_get_start_date($post->ID, true, 'd'); ?></span>
					</td><!-- /.home-events-date -->
					<td class="home-events-title" itemprop="name">
						<?php echo strlen(get_the_title()) > 27 ? substr(get_the_title(), 0, 27) . '...' : get_the_title(); ?>
					</td>
					<td class="home-events-location" itemprop="location" itemscope itemtype="http://schema.org/Place">
						<span itemprop="addressLocality"><?php the_event_city(); ?></span>, <span itemprop="addressRegion"><?php the_event_state(); ?></span>
					</td>
					<td class="home-events-more-info">
						<?php
							if( tribe_get_event_website_url() && tribe_event_in_category('heroes-6k') )
							{
								$link = tribe_get_event_website_url();
							}
							else
							{
								$link = get_the_permalink();
							}
						?>
						<a href="<?php echo $link; ?>" itemprop="url">Learn More</a>
					</td>
				</tr>
			<?php 
			} //endforeach 

			wp_reset_postdata(); 
			?>
		</table>
		<div class="events-view-all-wrap">
			<a href="<?php echo get_bloginfo('url'); ?>/calendar" title="View All Events" class="hir-btn slim">View All Events</a>
		</div><!-- /.events-view-all-wrap -->		
	</section><!-- /#home-events -->
</div>
<div class="submit-row row">
	<div class="col-sm-12" id="submit-content">
		<?php echo get_field('submit_copy'); ?>
		<a href="<?php echo get_field('submit_link'); ?>" class="hir-btn hir-btn-green slim">Submit Your Event</a>
	</div><!-- /#submit-content -->
</div><!-- /.row -->
<div class="hero-event-row row" style="background-image: url(<?php echo get_field('background_image'); ?>);">
	<div class="col-sm-12" id="hero-event">
		<h4><?php echo get_field('heroes_title'); ?></h4>
		<?php echo get_field('heroes_copy'); ?>
		<a href="<?php echo get_field('heroes_link'); ?>" class="hir-btn hir-btn-blue slim">View All Races</a>
	</div><!-- /#submit-content -->
</div><!-- /.row -->
<?php get_footer(); ?>
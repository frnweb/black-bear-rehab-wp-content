<?php
// Function to check if a page has a thumbnail.
// This includes checks for blog pages
if(!function_exists('page_has_post_thumbnial')) {
function page_has_post_thumbnial()
{
	if( is_home() )
	{

	    // Get the page ID that has been assigned to the posts page
	    $blog_page_id = get_option( 'page_for_posts' );

	    return has_post_thumbnail( $blog_page_id );

	}
	else if( is_post_type_archive( 'stories' ) || is_tax( 'story_category' ) )
	{
		// This is set to the post id for the "Stories" page
		return has_post_thumbnail( '6927' );
	}
	else
	{
		return has_post_thumbnail();
	}
}}

// Get content for a page, includes support for blog page.
if(!function_exists('page_get_field')) {
function page_get_field( $field )
{
	if( is_home() )
	{

	    // Get the page ID that has been assigned to the posts page
	    $blog_page_id = get_option( 'page_for_posts' );

	    return get_field( $field, $blog_page_id );

	}
	else if( is_post_type_archive( 'stories' ) || is_tax( 'story_category' ) )
	{
		// This is set to the post id for the "Stories" page
		return get_field( $field, '6927' );
	}	
	else
	{
		return get_field( $field );
	}
}}

$on_race_page = is_page_template('page-event.php') ? true : false;

?>	
<div class="row page-top-row<?php echo page_has_post_thumbnial() ? ' has-img' : ' no-img' ?>">
	<div class="<?php echo page_has_post_thumbnial() ? 'col-sm-12' : 'col-sm-10 col-sm-offset-1' ?> page-top">
		
		<?php if(!is_page_template('page-race-landing.php')): ?>

		<div class="page-top-text-overlay<?php echo $on_race_page ? ' race-overlay' : ''; ?>">
			<div class="page-top-text-overlay-inner">
				<div class="page-top-text-overlay-inner-content">
					<div class="page-top-text-overlay-inner-content-wrap">
						<?php if($on_race_page): ?>
						<div class="race-img-wrap">
							<img src="<?php echo get_bloginfo('template_url'); ?>/img/race_icon.png" alt="6k Race">
						</div><!-- /.race-img-wrap -->
						<?php endif; ?>
						<h1><?php echo page_get_field('page_top_title') ? page_get_field('page_top_title') : the_title(); ?></h1>
						<?php 
							if(page_get_field('page_top_copy')) {
								echo page_get_field('page_top_copy'); 
							}			
						?>
						<?php if($on_race_page): ?>
						<!-- <a href="<?php echo get_field('register_link'); ?>" class="hir-btn hir-btn-red slim"<?php echo get_field('new_window') == 'Yes' ? ' target="_blank"' : ''; ?>>Register Now</a> -->
						<?php endif; ?>

						<?php if(is_page_template('page-thanks.php')): ?>
							<div id="social-share">
				                <a href="https://www.facebook.com/sharer/sharer.php?u=http://www.heroesinrecovery.com" title="Facebook" target="_blank" itemprop="url">
				                    <span class="sr-only" itemprop="name">Facebook</span><i class="fa fa-facebook"></i>
				                </a>
				                <?php
				                $tweet = 'I shared my story on HeroesinRecovery.com. #BreakTheStigma of addiction & mental health issues by joining the @HeroesnRecovery movement!';
				                ?>
				                <a href="https://twitter.com/home?status=<?php echo urlencode($tweet); ?>" title="Twitter" target="_blank" itemprop="url">
				                    <span class="sr-only" itemprop="name">Twitter</span><i class="fa fa-twitter"></i>
				                </a>
				                <?php 
				                $body = 'I shared my story on <a href="http://www.heroesinrecovery.com/stories?email_share_visit">HeroesinRecovery.com</a>. Break the stigma of addiction and mental health issues by joining the Heroes in Recovery movement!" <a href="http://www.heroesinrecovery.com/share">Share your story</a> with me and make the difference for those who need to hear it.';
				                ?>
				                <a href="mailto:?subject=I just shared my story on HeroesinRecovery.com&body=<?php echo htmlentities($body); ?>"><span class="sr-only" itemprop="name">Email</span><i class="fa fa-envelope-o"></i>
							</div><!-- /#social-share -->
							<p><a href="<?php echo get_bloginfo('url'); ?>/stories">Read Others' Stories</a></p>
						<?php endif; ?>
						<?php if(is_post_type_archive( 'stories' )): ?>
						<div class="story-page-share">
						    <a href="<?php echo get_bloginfo('url'); ?>/share" class="hir-btn">Share Your Story</a>
						</div><!-- /.story-page-share -->						
						<?php endif; ?>
					</div><!-- /.page-top-text-overlay-inner-content -->
				</div><!-- /.page-top-text-overlay-inner-content -->			
			</div><!-- /.page-top-text-overlay-inner -->
		</div><!-- /.page-top-text-overlay -->	

		<?php else: ?>


		<div id="map-overlay">
			<img src="<?php echo get_bloginfo('template_url'); ?>/img/6k_big.png" id="sixk-big">
			<?php get_template_part('_map'); ?>
			<!-- <img src="<?php echo get_bloginfo('template_url'); ?>/img/6k_top.png"> -->
		</div><!-- /#map-overlay -->
		<?php endif; ?>

		<?php
		if( is_home() ) {

		    // Get the page ID that has been assigned to the posts page
		    $blog_page_id = get_option( 'page_for_posts' );

		    // Check if the post page has a featured image
		    if( has_post_thumbnail( $blog_page_id ) ) {
		        // Display the featured image
		        echo get_the_post_thumbnail( $blog_page_id, 'sub-featured' );
		    }

		} 
		elseif( is_post_type_archive( 'stories' ) || is_tax( 'story_category' ) )		
		{
			echo get_the_post_thumbnail( '6927', 'sub-featured' );
		}
		elseif( has_post_thumbnail() )
		{
			the_post_thumbnail('sub-page');
		} 
		?>		
	</div><!-- /.col-sm-12 -->
</div><!-- /.row -->
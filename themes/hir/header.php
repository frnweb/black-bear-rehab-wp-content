<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=yes" />
    
    <title><?php wp_title( ':', true, 'before' ) ?></title>

    <link rel="shortcut icon" href="<?php echo get_bloginfo( 'template_url' ); ?>/img/favicon.png">
    <link href='http://fonts.googleapis.com/css?family=Enriqueta:400,700|Lato:400,300,700,900' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?php echo get_bloginfo( 'template_url' ); ?>/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo get_bloginfo( 'template_url' ); ?>/style.css?v=1.93">

<?php 
    if(is_single() && !is_singular( 'stories' ) ) { 
		//if it is a blog post, and not a story
		$author_id = get_post_field( 'post_author', get_the_ID() ); 
		$avatar_code = get_avatar( $author_id, 1200, "", get_the_author_meta( 'user_nicename', $author_id ) ); 
		$avatar_url = get_frn_author_url($avatar_code); //get_avatar_url( $avatar_code );  //uses a regular expression to pull our the URL in the src link (only way to get the url)
		if($avatar_url!=="") {
	?><meta property="og:image" content="<?=$avatar_url; ?>" /> 
    <?php }
	}
	elseif(is_singular( 'stories' ) ) {
		//if the post is a blog post type called stories
		//$post_id = get_the_ID(); 
        //previously we put the main FB image here, but now the FRN Plugin provides it
	} 
	?>
<?php wp_head(); ?>

    <?php
    // Setup Body Classes
    if(is_front_page())
    {
        $bodyClasses = 'home';
    } 
    else
    {
        $bodyClasses = 'subpage';
    }

    if(!has_post_thumbnail())
    {
        $bodyClasses .= ' no-featured-img';
    }

    if( is_page_template('page-event.php') || is_page_template('page-about.php') )
    {
        $bodyClasses .= ' has-tabs';
    }

    $bodyClasses .= ' page-'. get_the_ID();
    ?> 
  <!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');

fbq('init', '1687303408188649');
fbq('track', "PageView");</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1687303408188649&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

<script>// ViewContent
// Track key page views (ex: product page, landing page or article)
fbq('track', 'ViewContent'); 
</script> 

</head>
<body class="<?php echo $bodyClasses; ?> <?=get_post_type();?>" itemscope itemtype="http://schema.org/WPHeader">
	<!-- Google Tag Manager -->
	<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-TT4SX5"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-TT4SX5');</script>
	<!-- End Google Tag Manager -->
	<?php 
		if(	(	isset($_GET['ppc']) || isset($_GET['utm_campaign'])	) && strpos($_SERVER['REQUEST_URI'],"heroes6k/") ) 
			$hide_nav="yes";
	?>
    <div id="mobilephone" class="show-for-small-only">Get Help: <strong><?php echo do_shortcode('[frn_phone number="" ga_phone_location="Phone Clicks in ##mobile nav##"]'); ?></strong></div>
    <div class="container-fluid" <?=(isset($hide_nav)) ? "style='display:none;'" : null ; ?>>
        <div id="header-row" class="row" <?=(isset($hide_nav)) ? "style='display:none;'" : null ; ?>>
            <header id="header" class="col-sm-10 col-sm-offset-1">
                <!-- MENU BUTTON
                <button id="header-menu-close-btn"><i class="fa fa-bars"></i> Menu</button>
                -->

                <div id="header-left" class="col-sm-3">
                    <div id="logo" itemscope itemtype="http://schema.org/ImageObject">
                        <a href="<?php echo get_bloginfo('url'); ?>" title="Home">
                            <span class="sr-only" itemprop="name">Heroes In Recovery</span>
                            <img src="<?php echo get_bloginfo('template_url'); ?>/img/logo.svg" 
                                 alt="Heroes In Recovery" 
                                 itemprop="logo">
                        </a>
                    </div><!-- /#logo -->
                </div><!-- /#header-left -->
                <nav id="header-nav" class="col-sm-6" itemscope itemtype="http://schema.org/SiteNavigationElement">
                    <div id="header-nav-inner">
                    <?php
                        if( has_nav_menu( 'header-menu' ) ) {
                            wp_nav_menu( array( 'theme_location' => 'header-menu' ) );
                        }
                    ?>   
                    </div><!-- /#header-nav-inner -->                 
                </nav><!-- /#header-nav -->
                <div id="btn-header-right" class="col-sm-3">
                    <button class="nav-cta">Get Help: <?php echo do_shortcode('[frn_phone number="" ga_phone_location="Phone Clicks in ##main nav##"]'); ?></button>
                </div>
            </header><!-- /#header -->
        </div>
    </div>
    <div class="container-fluid">        
        <div class="row">
            <div class="page-top-wrapper">
                <?php if(is_page_template('page-event.php') || is_page_template('page-race-landing.php')) get_template_part('_page-top'); //removed from page-event 7/14/17 to improve css styling flexibility ?>
                <?php get_template_part('_second_nav');  //added 7/14/17 -  template has logic activating this for about and race pages so we can put this on any page in the site ?>
            </div>
            <main id="main-content" class="col-sm-12">                  
<?php
/**
 * Template Name: Submit Event
 */
?>
<?php get_header(); ?>
<?php get_template_part('_page-top'); ?>
<div class="page-row row submit-event">
	<article class="page-content-wrap col-sm-8 col-sm-offset-2">
		<div class="row page-content-row">
			<?php the_content(); ?>
		</div><!-- /.page-content-row -->
	</article>
</div><!-- /.row -->
<?php get_footer(); ?>
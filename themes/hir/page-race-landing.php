<?php
/**
 * Template Name: 6k Race Landing
 */

get_header();
?>
<?php //get_template_part('_page-top'); ?>
<div class="green-heroes-row row">
	<div class="col-sm-6" id="green-heroes">				
		<h4><?php echo get_field('heroes_title'); ?></h4>
		<?php echo get_field('heroes_copy'); ?>
		<a href="<?php echo get_field('file'); ?>" class="hir-btn hir-btn-green slim" download><?php echo get_field('button_text'); ?></a>
	</div><!-- /#submit-content -->
	<div class="col-sm-6" id="green-heroes-video">				
		<?php echo get_field('video_embed'); ?>
	</div>		
</div><!-- /.row -->
<div class="orange-heroes-row row">
	<div class="col-sm-12 col-md-4" id="meet-race-team">				
		<h4><?php echo get_field('heroes_meet_race_title'); ?></h4>
		<?php echo get_field('heroes_meet_race_copy'); ?>
		<a href="<?php echo get_field('heroes_meet_race_link'); ?>" class="hir-btn hir-btn-blue2 slim"><?php echo get_field('heroes_meet_race_link_copy'); ?></a>
	</div><!-- /meet-race-team -->
	<div class="col-sm-12 col-md-4" id="heroes-get-social">				
		<h4><?php echo get_field('heroes_get_social_title'); ?></h4>
		<p><?php echo get_field('heroes_get_social_copy'); ?></p>
	</div><!-- /heroes-get-social -->
	<div class="col-sm-12 col-md-4" id="blue-heroes">				
		<h4><?php echo get_field('heroes_sponsor_title'); ?></h4>
		<?php echo get_field('heroes_sponsor_copy'); ?>
		<a href="<?php echo get_field('file_sponsor'); ?>" class="hir-btn hir-btn-blue2 slim" download><?php echo get_field('sponsor_button_text'); ?></a>
	</div><!-- /#submit-content -->	
</div><!-- / 2nd row -->
<div class="race-listing-wrap">
	<div class="row race-landing-row">
		<div class="col-sm-10 col-sm-offset-1 race-row">
			<div class="row">	
				<?php
				$i = 1;
				if( have_rows('event') ):

				     // loop through the rows of data
				    while ( have_rows('event') ) : the_row();
				?>


				<div class="col-sm-6 race-wrap">
					<div class="row">
						<div class="col-sm-3 race-page-date">
							<?php if(get_sub_field('show_tbd') == 'Yes'): ?>
							<span class="race-page-date-month">
								TBD
							</span>							
							<?php else: ?>
							<span class="race-page-date-month">
								<?php echo get_sub_field('event_month'); ?>
							</span>
							<span class="race-page-date-day">
								<?php echo get_sub_field('event_day'); ?>
							</span>						
						<?php endif; ?>
						</div><!-- /.race-page-date -->
						<div class="col-sm-5 race-page-location">
							<?php echo get_sub_field('event_location'); ?>
						</div><!-- /.race-page-location -->
						<div class="col-sm-4 race-page-link">
							<a href="<?php echo get_sub_field('event_page'); ?>">Learn More</a>
						</div>
					</div><!-- /.row -->
				</div><!-- /.race-wrap -->

				<?php if($i%2 == 0): ?>
			</div><!-- /.row -->	
		</div><!-- /.race-row -->
	</div><!-- /.race-landing-row -->		
	<div class="row race-landing-row">
		<div class="col-sm-10 col-sm-offset-1 race-row">
			<div class="row">	
				<?php endif; ?>

			    <?php
			    	$i++;
			    	endwhile;
				else :

				    $content = 'No Content Found';

				endif;	
				?>

				<?php if(get_field('last_panel_copy')): ?>
				<div class="col-sm-6 race-wrap">
					<div class="row">
						<div class="col-sm-12 race-page-last-pannel">
							<?php echo get_field('last_panel_copy'); ?>
						</div>
					</div><!-- /.row -->
				</div><!-- /.race-wrap -->
				<?php endif; ?>
			</div><!-- /.row -->	
		</div><!-- /.race-row -->
	</div><!-- /.race-landing-row -->	
</div>	
<?php get_footer(); ?>
<?php get_header(); ?>
<?php 
get_template_part('_page-top'); //moved to header for easier css styling 7/17/17
 ?>
<div class="page-row row">
	<article class="page-content-wrap col-sm-10 col-sm-offset-1">
		<div class="row page-content-row">
			<?php the_content(); ?>
		</div><!-- /.page-content-row -->
	</article>
</div><!-- /.row -->
<?php get_footer(); ?>
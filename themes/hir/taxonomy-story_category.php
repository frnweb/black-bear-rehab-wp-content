<?php get_header(); ?>
<?php 
	if(!wp_is_mobile())
	{
		get_template_part('_page-top'); 
	}	
?>
<?php get_template_part('searchform'); ?>
<div class="row story-archive-row">	
	<!-- Ajax Posts Here -->
	<?php echo do_shortcode('[ajax_load_more post_type="stories" scroll="false" posts_per_page="18" button_label="Load More" taxonomy="story_category" taxonomy_terms="'.get_query_var('story_category').'"]'); ?>
</div><!-- /.story-archive-row -->
<?php get_footer(); ?>
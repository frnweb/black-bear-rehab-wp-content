<?php get_header(); ?>
<?php get_template_part('searchform'); ?>
<div class="page-row row">
	<div class="col-sm-8 col-sm-offset-2">
		<h1 class="page-title"><?php _e( 'Archives', 'sparks' ); ?></h1>
		<?php get_template_part('loop'); ?>
		<?php get_template_part('pagination'); ?>
	</div>
</div>
<?php get_footer(); ?>
<?php
/**
 *
 */

// Swift Mailer
require_once 'swift/swift_required.php';

class story
{
	/**
	 * Errors
	 *
	 * @var array errors
	 */
	public $errors = array();

	/**
	 * Required Form Fields
	 *
	 * @var array fields
	 */
	public $requiredFields = array(
		'person_name'    => 'Please enter your name.', 
		'email'          => 'Please enter a valid email address.', 
		'story_category' => 'Please select at least one category.',
		'terms'          => 'You must agree to the terms before submitting a story.'
	);

	/**
	 * Post Fields
	 * 
	 * @var array post data
	 */
	public $fields;

	/**
	 * Who to notify
	 *
	 * @var string email
 	 */
	public $email = 'info@heroesinrecovery.com';
	// public $email = 'daxon.edwards@frnmail.com';
    // public $email = 'matt.king@frnmail.com';


	/**
	 * Path to Hero's image
	 *
	 * @var string url
	 */	
	public $heroImage;

	/**
	 * Is the form submission valid?
	 *
	 * @var bool
	 */
	public $isValid = false;

	/**
	 * Constructor
	 */
	function __construct( $post, $files )
	{
		
		//Completely strip everything but text and numbers from the rest
		$this->fields = $this->sanitize( $post, $files );

		$this->isValid();

		// If an image has been uploaded, process it.
		if( $files )
		{			
			$this->processUpload();
		}
	}

	/**
	 * Process Image
	 *
	 * @return bool success or failure
	 */
	function processUpload()
	{
		if ( ! function_exists( 'wp_handle_upload' ) ) 
		{
		    require_once( ABSPATH . 'wp-admin/includes/file.php' );
		}

		$uploadedfile = $_FILES['hero_image'];
		$upload_overrides = array( 'test_form' => false );
		$movefile = wp_handle_upload( $uploadedfile, $upload_overrides );

		if ( $movefile && !isset( $movefile['error'] ) ) 
		{
			// File was uploaded, let's make it work with the media library
		    $wp_filetype = $movefile['type'];
		    $filename = $movefile['file'];
		    $wp_upload_dir = wp_upload_dir();
		    $attachment = array(
		        'guid' => $wp_upload_dir['url'] . '/' . basename( $filename ),
		        'post_mime_type' => $wp_filetype,
		        'post_title' => preg_replace('/\.[^.]+$/', '', basename($filename)),
		        'post_content' => '',
		        'post_status' => 'publish'
		    );

		    $attach_id = wp_insert_attachment( $attachment, $filename);		   


		} else {

		    $this->errors[] = $movefile['error'];

		    return false;
		}


		$this->heroImage = $wp_upload_dir['url'] . '/' . basename( $filename );		

	}

	/**
	 * Sanitize Input
	 *
	 * @return array sanitized post content
	 */
	public function sanitize( $dirty )
	{
		$args = array(
			'person_name'    => FILTER_SANITIZE_STRING,
			'email'          => FILTER_SANITIZE_EMAIL,
			'story_category' => array(
		                            'filter' => FILTER_SANITIZE_STRING,
		                            'flags'  => FILTER_REQUIRE_ARRAY,
	                            ),
			'question-1'     => FILTER_SANITIZE_STRING,
			'question-2'     => FILTER_SANITIZE_STRING,
			'question-3'     => FILTER_SANITIZE_STRING,
			'question-4'     => FILTER_SANITIZE_STRING,
			'question-5'     => FILTER_SANITIZE_STRING,
			'question-6'     => FILTER_SANITIZE_STRING,
			'share-date'     => FILTER_SANITIZE_STRING,
			'written-story'  => FILTER_SANITIZE_STRING,
			'title'          => FILTER_SANITIZE_STRING,
			'avatar'         => FILTER_SANITIZE_STRING,
			'newsletter'     => FILTER_SANITIZE_STRING,
			'terms'          => FILTER_SANITIZE_STRING,
			'story-type'     => FILTER_SANITIZE_STRING,
		);
		
		/* The following used to have everything stripped including line breaks

		*/

		return filter_input_array( INPUT_POST, $args );
	}

	/**
	 * Validate Form
	 *
	 * @return bool
	 */
	public function isValid()
	{
		$this->isValid = $this->checkForRequiredFields();
	}

	/**
	 * Check required fields
	 *
	 * @return bool
	 */
	public function checkForRequiredFields()
	{

		foreach( $this->requiredFields as $field=>$message )
		{
			if( empty( $this->fields[$field] ) || $this->fields[$field] == '' )
			{
				$this->errors[] = $message;							
			}			
		}

		if( $this->fields['story-type'] == 'qa' )
		{

			if( empty($this->fields['question-1']) && empty($this->fields['question-2']) && empty($this->fields['question-3']) && empty($this->fields['question-4']) && empty($this->fields['question-5']) && empty($this->fields['question-6']) )
			{
				$this->errors[] = 'Please answer at least one question or write your own story.';
			}

			if(empty($this->fields['title']))
			{
				$this->errors[] = 'Please provide a title.';
			}
		}
		
		if( $this->fields['story-type'] == 'written' )
		{
			if(empty($this->fields['written-story']))
			{
				$this->errors[] = 'Please write your story or answer questions.';
			}

		}
		
		if(empty($this->fields['share-date']))
		{
			$this->errors[] = 'Please provide a date.';
		}

		if( empty( $_FILES['hero_image']['name'] ) && empty( $this->fields['avatar'] ) )
		{
			$this->errors[] = 'Please upload an image or select an avatar.';
		}

		return $this->errors ? false : true;

	}

	/**
	 * See if a checkbox is checked
	 *
	 * @return string the checked attribute
	 */
	public function is_checked( $group, $value )
	{	
		// Grouped checkboxes			
		if( is_array( $this->fields[$group] ) )
		{

			if( in_array($value, $this->fields[$group]) )
			{
				$checked = ' checked="checked"';
			}
			else
			{
				$checked = '';
			}
		}
		// Single checkboxes
		else
		{
			if( $this->fields[$group] == $value )
			{
				$checked = ' checked="checked"';
			}
			else
			{
				$checked = '';
			}
		}

		return $checked;
	}

	/**
	 * Construct Notification Email
	 *
	 * @return var the message
	 */
	public function constructEmail()
	{
		$message .= '<img src="http://proofbrandingdev.net/hir/wp-content/themes/hir/img/email_logo.jpg">';
		$message .= '<p>A new story has been story shared!</p>';
		$message .= '<table border="0">';
			
			// Name
			$message .= '<tr style="border-bottom: 1px solid #000;">';				
				$message .= '<td style="font-weight:bold;padding:10px;width:10%;vertical-align:top;">Name</td>';
				$message .= '<td style="vertical-align:top;width:90%;padding:10px;">'.$this->fields['person_name'].'</td>';			
			$message .= '</tr>';

			// Email
			$message .= '<tr style="border-bottom: 1px solid #000;">';				
				$message .= '<td style="font-weight:bold;padding:10px;width:10%;vertical-align:top;">Email</td>';
				$message .= '<td style="vertical-align:top;width:90%;padding:10px;">'.$this->fields['email'].'</td>';			
			$message .= '</tr>';

			// Phone
			$message .= '<tr style="border-bottom: 1px solid #000;">';				
				$message .= '<td style="font-weight:bold;padding:10px;width:10%;vertical-align:top;">Phone</td>';
				$message .= '<td style="vertical-align:top;width:90%;padding:10px;">'.$this->fields['phone'].'</td>';			
			$message .= '</tr>';

			// Handle photo upload here..

			// Category
			$message .= '<tr style="border-bottom: 1px solid #000;">';				
				$message .= '<td style="font-weight:bold;padding:10px;width:10%;vertical-align:top;">Categories</td>';
				$message .= '<td style="vertical-align:top;width:90%;padding:10px;">';
					foreach( $this->fields['story_category'] as $category) 
					{
						$message .= $category . '<br>';
					}
				$message .= '</td>';			
			$message .= '</tr>';

			// Title
			if($this->fields['share-title'] != '') 
			{
			$message .= '<tr style="border-bottom: 1px solid #000;">';				
				$message .= '<td style="font-weight:bold;padding:10px;width:10%;vertical-align:top;">Title</td>';
				$message .= '<td style="vertical-align:top;width:90%;padding:10px;">'.$this->fields['share-title'].'</td>';			
			$message .= '</tr>';
			}

			// Question 1
			if($this->fields['question-1'] != '')
			{
				$message .= '<tr style="border-bottom: 1px solid #000;">';				
					$message .= '<td style="font-weight:bold;padding:10px;width:10%;vertical-align:top;">Start by describing the situation that changed your life or a loved one\'s life.</td>';
					$message .= '<td style="vertical-align:top;width:90%;padding:10px;">'.$this->fields['question-1'].'</td>';			
				$message .= '</tr>';				
			}	

			// Question 2
			if($this->fields['question-2'] != '')
			{
				$message .= '<tr style="border-bottom: 1px solid #000;">';				
					$message .= '<td style="font-weight:bold;padding:10px;width:10%;vertical-align:top;">Based on your situation or story, was there a turning point that prompted the need for change or help?</td>';
					$message .= '<td style="vertical-align:top;width:90%;padding:10px;">'.$this->fields['question-2'].'</td>';			
				$message .= '</tr>';				
			}	

			// Question 3
			if($this->fields['question-3'] != '')
			{
				$message .= '<tr style="border-bottom: 1px solid #000;">';				
					$message .= '<td style="font-weight:bold;padding:10px;width:10%;vertical-align:top;">How did you or your HERO get help?</td>';
					$message .= '<td style="vertical-align:top;width:90%;padding:10px;">'.$this->fields['question-3'].'</td>';			
				$message .= '</tr>';				
			}	

			// Question 4
			if($this->fields['question-4'] != '')
			{
				$message .= '<tr style="border-bottom: 1px solid #000;">';				
					$message .= '<td style="font-weight:bold;padding:10px;width:10%;vertical-align:top;">Based on your experience, what lessons did you learn? Do you have any advice to give?</td>';
					$message .= '<td style="vertical-align:top;width:90%;padding:10px;">'.$this->fields['question-4'].'</td>';			
				$message .= '</tr>';				
			}	

			// Question 5
			if($this->fields['question-5'] != '')
			{
				$message .= '<tr style="border-bottom: 1px solid #000;">';				
					$message .= '<td style="font-weight:bold;padding:10px;width:10%;vertical-align:top;">If you or your loved one is in recovery, describe what life is like today.</td>';
					$message .= '<td style="vertical-align:top;width:90%;padding:10px;">'.$this->fields['question-5'].'</td>';			
				$message .= '</tr>';				
			}	

			// Question 6
			if($this->fields['question-6'] != '')
			{
				$message .= '<tr style="border-bottom: 1px solid #000;">';				
					$message .= '<td style="font-weight:bold;padding:10px;width:10%;vertical-align:top;">Is there anything else you\'d like to share?</td>';
					$message .= '<td style="vertical-align:top;width:90%;padding:10px;">'.$this->fields['question-6'].'</td>';			
				$message .= '</tr>';				
			}

			// Share Date

				$message .= '<tr style="border-bottom: 1px solid #000;">';				
					$message .= '<td style="font-weight:bold;padding:10px;width:10%;vertical-align:top;">Shared Date</td>';
					$message .= '<td style="vertical-align:top;width:90%;padding:10px;">'.$this->fields['share-date'].'</td>';			
				$message .= '</tr>';				
			

			if($this->fields['written-story'] != '')
			{
				$message .= '<tr style="border-bottom: 1px solid #000;">';				
					$message .= '<td style="font-weight:bold;padding:10px;width:10%;vertical-align:top;">Written Story</td>';
					$message .= '<td style="vertical-align:top;width:90%;padding:10px;">'.str_replace(chr(13), "<br />".chr(13), $this->fields['written-story']).'</td>';			
				$message .= '</tr>';				
			}	

			if($this->heroImage != '')
			{
				$message .= '<tr style="border-bottom: 1px solid #000;">';				
					$message .= '<td style="font-weight:bold;padding:10px;width:10%;vertical-align:top;">Hero Image</td>';
					$message .= '<td style="vertical-align:top;width:90%;padding:10px;">'.$this->heroImage.'</td>';			
				$message .= '</tr>';						
			}

			if($this->fields['avatar'] != '')
			{
				$message .= '<tr style="border-bottom: 1px solid #000;">';				
					$message .= '<td style="font-weight:bold;padding:10px;width:10%;vertical-align:top;">Selected Avatar</td>';
					$message .= '<td style="vertical-align:top;width:90%;padding:10px;">'.$this->fields['avatar'].'</td>';			
				$message .= '</tr>';				
			}

			$message .= '<tr style="border-bottom: 1px solid #000;">';				
				$message .= '<td style="font-weight:bold;padding:10px;width:10%;vertical-align:top;">Newsletter?</td>';
				if($this->fields['newsletter'])
				{
					$message .= '<td style="vertical-align:top;width:90%;padding:10px;">Yes</td>';			
				}					
				else
				{
					$message .= '<td style="vertical-align:top;width:90%;padding:10px;">No</td>';			
				}
			$message .= '</tr>';

			$message .= '<tr style="border-bottom: 1px solid #000;">';				
				$message .= '<td style="font-weight:bold;padding:10px;width:10%;vertical-align:top;">Agreed to terms?</td>';
				if($this->fields['terms'])
				{
					$message .= '<td style="vertical-align:top;width:90%;padding:10px;">Yes</td>';			
				}					
				else
				{
					$message .= '<td style="vertical-align:top;width:90%;padding:10px;">No</td>';			
				}
			$message .= '</tr>';							



		$message .= '</table>';

		return $message;
	}

	/**
	 * Construct Notification Email
	 *
	 * @return var the message
	 */
	public function constructConfirmationEmail()
	{
		$message = '

			<html>
			<body>
			<TABLE cellSpacing="0" cellpadding="0" bgcolor="#ffffff" border=0 align="center" width="499">

			    <TR>
			<TD bgcolor="#FFFFFF" width="499" align="center" valign="top">
			    </tr>
			    <tr>
			        <td valign="top"><div align="center"><a href="http://heroesinrecovery.com/"><img src="https://ci3.googleusercontent.com/proxy/qI4ykAd3JcWNOuoCpibIOPiGOTyPEFNsRSA67_PypAYrJlDXxrLKPE0i3MhJj65E8wHrmrP3tDYd6IDALtuDa5b3SlhNB2873tBXNMW3f3VVvzWot8QoXTD-Uwhi=s0-d-e1-ft#http://www.heroesinrecovery.com/wp-content/uploads/thank-you-heroes.gif" alt="" hspace="0" vspace="0" border="0" /></a></div></td>
			    </tr>
			    <tr>
			    <td valign="top"><div align="left" style="font-size:13px;"><FONT face="Georgia, Times New Roman, Times, serif" color="#333333">
			Thank you for submitting your story to <a href="http://www.heroesinrecovery.com" target="new" style="COLOR:#C30; TEXT-DECORATION: underline;"><strong>Heroes in Recovery</strong></a>. Your story may  be the one that encourages someone to seek the addiction and mental health help they need without feeling ashamed or isolated. Together, we are breaking the stigma one story at a time!<br />
			<br />
			    Before your story can be posted to the website, the Heroes team will need to review and make any edits if necessary.  We have  a set of rules we use to protect everyone\'s privacy, maintain the values of Heroes in Recovery and aid the reading experience. Please read our <a href="http://www.heroesinrecovery.com/wp-content/uploads/HeroesStyleGuide.pdf" target="new" style="COLOR:#C30; TEXT-DECORATION: underline;"><strong>guidelines</strong></a> to find out how or why your story may have been edited.<br /><br />
			<em>In the meantime, follow us on social media and join the conversation!</em></font>
			            <div align="center"><img src="https://ci4.googleusercontent.com/proxy/QrrBhTWbNz_N4m0uOXIFINmHC_mRjBmdZMHdLffOA55KQXCvNb1RxyjOY5-cHU-GHUBzydT2K5iUjWfoV-jiqagXXevV03BBFUvXniRAsyvCHtXYLS3A2PBPTNZeP4A=s0-d-e1-ft#http://www.heroesinrecovery.com/wp-content/uploads/social-media-icons.gif" alt="social media icons" width="270" height="88" usemap="#Map" hspace="0" vspace="0" border="0" /></div>
			</div></td>
			</tr>
			<tr>
			          <td bgcolor="#ffffff" width="499" align="center"><br />
			   <div align="center" style="font-size:12px;">
			<font face="arial" color="#333333"><strong>Heroes in Recovery</strong><br />5409 Maryland Way | Suite 320 | Brentwood, TN 37027 <br /><a href="http://www.facebook.com/HeroesinRecovery" target="new" style="COLOR:#C30; TEXT-DECORATION: underline;"><strong>Heroes Facebook</strong></a> | <a href="http://twitter.com/heroesnrecovery" target="new" style="COLOR:#C30; TEXT-DECORATION: underline;"><strong>Heroes Twitter</strong></a> | <a href="http://www.pinterest.com/heroesnrecovery/" target="new" style="COLOR:#C30; TEXT-DECORATION: underline;"><strong>Heroes Pinterest</strong></a> | <a href="http://instagram.com/heroesinrecovery#" target="new" style="COLOR:#C30; TEXT-DECORATION: underline;"><strong>Heroes Instagram</strong></a><br>
			            <br> </font>
			</div></td>
			     </tr>
			</table>

			<map name="Map" id="Map">
			 <area shape="rect" coords="8,12,69,75" href="http://twitter.com/heroesnrecovery" />
			 <area shape="rect" coords="73,15,133,75" href="http://www.pinterest.com/heroesnrecovery/" />
			 <area shape="rect" coords="138,14,199,77" href="http://www.facebook.com/HeroesinRecovery" />
			 <area shape="rect" coords="202,14,264,79" href="http://instagram.com/heroesinrecovery#" />
			</map>
			</body>
			</html>
		';

		return $message;
	}	

	/**
	 * Send Email
	 *
	 * @return bool
	 */
	public function sendEmail()
	{
		$message = Swift_Message::newInstance()

		// Subject
		->setSubject('Story Submission')

		// From Email
		->setFrom(array('DO_NOT_REPLY@corp.frn.network' => 'Heroes In Recovery'))

		// To Email
		->setTo(array($this->email))

		// Body
		->setBody($this->constructEmail(), 'text/html');

		// Transport
		$transport = Swift_SmtpTransport::newInstance('smtp.sendgrid.net', 25)
		->setUsername('apikey')
        ->setPassword('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')
        ;

		// Mailer
		$mailer = Swift_Mailer::newInstance($transport);		


		return $mailer->send($message);
	}

	/**
	 * Send Confirmation Email
	 *
	 * @return bool
	 */
	public function sendConfirmationEmail($email)
	{
		$alt = "
			Thank you for submitting your story to Heroes in Recovery! Your story may be the one that encourages someone to seek the addiction and mental health help they need without feeling ashamed or isolated. Together, we are breaking the stigma one story at a time!

			Before your story can be posted to the website, the Heroes team will need to review and make any edits if necessary. We have a set of rules we use to protect everyone's privacy, maintain the values of Heroes in Recovery and aid the reading experience. Please read our guidelines (http://www.heroesinrecovery.com/wp-content/uploads/HeroesStyleGuide.pdf) to find out how or why your story may have been edited.

			In the meantime, follow us on social media and join the conversation if you haven't already!"."

			http://twitter.com/heroesnrecovery
			http://www.pinterest.com/heroesnrecovery/
			http://www.facebook.com/HeroesinRecovery
			http://instagram.com/heroesinrecovery#


			Heroes in Recovery (www.heroesinrecovery.com)
			5409 Maryland Way | Suite 320 | Brentwood, TN 37027

		";

		$message = Swift_Message::newInstance()

		// Subject
		->setSubject('Thank You!')

		// From Email
		->setFrom(array('DO_NOT_REPLY@corp.frn.network' => 'Heroes In Recovery'))

		// To Email
		->setTo(array($email))

		// Body
		->setBody($this->constructConfirmationEmail(), 'text/html')

		// Alt Body
		->addPart($alt, 'text/plain');

		// Transport
		$transport = Swift_MailTransport::newInstance();

		// Mailer
		$mailer = Swift_Mailer::newInstance($transport);		


		return $mailer->send($message);
	}	
}
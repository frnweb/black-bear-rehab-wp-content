<?php 
/**
 * Template Name: Share
 */
require( 'includes/share-class.php' );

// If the form has been submitted create an instance of story
if( $_POST )
{
	$story = new story( $_POST, $_FILES );

	// Validate
	if( $story->isValid )
	{
		$story->sendConfirmationEmail($story->fields['email']);

		$sent = $story->sendEmail();

		// If the email has been sent, let's redirect to the thank you page
		if($sent) { wp_redirect(get_bloginfo('url') . '/thank-you', 200); }

	}
	else
	{
		$errors = true;
	}
}



get_header();
?>

<?php get_template_part('_page-top'); ?>

<!--
	// Quick Tips
-->
<div class="row quick-tips-row">
	<div class="col-sm-6 col-sm-offset-3">
		<div class="row">
			<img src="<?php echo get_bloginfo('template_url'); ?>/img/bulb.png" alt="Light Bulb">
			<h3>Quick Tips</h3>
			<div class="col-sm-4 quick-tips-col" id="quick-tips-col-1">
				<?php echo get_field('quick_tip_1'); ?>
			</div><!-- /.quick-tips-col-1 -->
			<div class="col-sm-4 quick-tips-col" id="quick-tips-col-2">
				<?php echo get_field('quick_tip_2'); ?>
			</div><!-- /.quick-tips-col-2 -->
			<div class="col-sm-4 quick-tips-col" id="quick-tips-col-3">
				<?php echo get_field('quick_tip_3'); ?>
			</div><!-- /.quick-tips-col-3 -->
		</div><!-- /.row -->
	</div><!-- /.col-sm-10 -->
</div><!-- /.quick-tips-row -->
<?php if( $errors ): ?>
<div class="row errors-row">
	<div class="col-sm-8 col-sm-offset-2 errors-wrap">
		<h3>There Were Errors With Your Submission</h3>
		<ul class="errors-list">
			<?php foreach( $story->errors as $error ): ?>
				<li><?php echo $error; ?></li>
			<?php endforeach; ?>
		</ul>
	</div>
</div>
<?php endif; ?>

<!--
	// Let's Get Started
-->
<div class="row share-row">
	<div class="col-sm-8 col-sm-offset-2 share-step" id="step-0">
		<div class="step-instructions">
			<h2>Let's Get Started</h2>
			<p>There are two ways to share your story with the Heroes in Recovery community.</p>	
		</div><!-- /.step-instructions -->
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
				<div class="row">
					<div class="col-sm-6">
						<div class="faux-btn<?php echo $errors && $story->fields['story-type'] == 'qa'  ? ' step-after-active' : ''; ?>"><button class="hir-btn-button hir-btn-full-width select-step-link<?php echo $errors && $story->fields['story-type'] == 'qa'  ? ' step-active' : ''; ?>" data-show="qa">Q&amp;A</button></div>
						<small>Follow our easy, step-by-step, question and answer process to get to the heart of your story. We’ll guide you.</small>
					</div><!-- /.col-sm-6 -->
					<div class="col-sm-6">
						<div class="faux-btn<?php echo $errors && $story->fields['story-type'] == 'written'  ? ' step-after-active' : ''; ?>"><button class="hir-btn-button hir-btn-full-width select-step-link<?php echo $errors && $story->fields['story-type'] == 'written'  ? ' step-active' : ''; ?>" data-show="written">Write Your Own</button></div>
						<small>Feel more comfortable writing it out? Simply copy/paste your story and submit, and we’ll review it for grammar and styling.</small>
					</div><!-- /.col-sm-6 -->
				</div>
			</div>			
		</div><!-- /.row -->
	</div><!-- /.share-step -->
</div><!-- /.share-row -->

<div id="share-form-wrap" <?php echo !$errors ? 'class="is-hidden"' : ''; ?>>
	<form action="<?php the_permalink(); ?>" method="post" enctype="multipart/form-data" id="share-form">
		<input type="hidden" name="submitted" value="1">
		<input type="hidden" name="story-type" value="<?php echo $errors && $story->fields['story-type'] ? $story->fields['story-type'] : ''; ?>" id="share-story-type">
	<!--
		// The Basics
	-->
	<div class="row share-row">
		<div class="col-sm-8 col-sm-offset-2 share-step" id="step-1">
			<div class="step-instructions">
				<h2><span class="step-number">1</span>The Basics</h2>
			</div><!-- /.step-instructions -->
			<div class="step-fields">
				<div class="field-wrap">
					<label for="share-name">Name<span class="required">*</span></label>
					<input type="text" name="person_name" id="share-name" value="<?php echo $story->fields['person_name'] ? $story->fields['person_name'] : ''; ?>">
					<small>You can use your first name only, your full name, initials or even use “anonymous”. Whatever you are comfortable with.</small>
				</div><!-- /.field-wrap -->
				<div class="field-wrap">
					<label for="share-name">Email<span class="required">*</span></label>
					<input type="email" name="email" id="share-email" value="<?php echo $story->fields['email'] ? $story->fields['email'] : ''; ?>">
					<small>Unless you let us know you want to publish your email as part of your story to allow others to contact you, it will remain private and Heroes in Recovery will only use it to contact you when your story has been published.</small>
				</div><!-- /.field-wrap -->
				<div class="field-wrap">
					<label for="share-name">Phone</label>
					<input type="phone" name="phone" id="share-phone" value="<?php echo $story->fields['phone'] ? $story->fields['phone'] : ''; ?>">
					<small>Your phone number will only be used by us internally to contact you regarding this story if needed. It won't be used for any other purpose.</small>
					<small><span class="required">*</span><em>Denotes required field</em></small>
				</div><!-- /.field-wrap -->
			</div><!-- /.step-fields -->
		</div><!-- /.share-step -->
	</div><!-- /.share-row -->

	<!--
		// Photo Upload
	-->
	<div class="row share-row">
		<div class="col-sm-8 col-sm-offset-2 share-step" id="step-2">
			<div class="step-instructions">
				<h2><span class="step-number">2</span>upload a photo. It’s way more than a profile pic.<span class="required">*</span></h2>
				<p>We believe that having a photo along with your submission makes your story all the more real and powerful. But we also respect the choice for anonymity. Simply upload a photo of your choice, OR, please select an avatar that you feel represents you and your story.</p>
			</div><!-- /.step-instructions -->
			<div class="row">
				<div class="col-sm-8 col-sm-offset-2">
					<div class="row">
						<div class="col-sm-6">
							<div class="faux-btn<?php echo $story->heroImage ? ' green' : ''; ?>"><button href="#upload" class="hir-btn-button hir-btn-full-width pop-up<?php echo $story->heroImage ? ' selected-option' : ''; ?>" id="upload-btn">Upload a Photo</button></div>
							<small>Click here to upload a photo from your computer. HINT: We love big, happy smiles. Have fun and get creative!</small>
						</div><!-- /.col-sm-6 -->
						<div class="col-sm-6">
							<div class="faux-btn<?php echo $story->fields['avatar'] ? ' green' : ''; ?>"><button href="#avatar" class="hir-btn-button hir-btn-full-width pop-up<?php echo $story->fields['avatar'] ? ' selected-option' : ''; ?>" id="avatar-btn">Choose an Avatar</button></div>
							<small>If you would rather not post a photo of yourself, we respect that. You can choose a profile pic that relates to your story.</small>
						</div><!-- /.col-sm-6 -->
					</div>
				</div>			
			</div><!-- /.row -->
		</div><!-- /.share-step -->
	</div><!-- /.share-row -->

	<!--
		// Category
	-->
	<div class="row share-row">
		<div class="col-sm-8 col-sm-offset-2 share-step" id="step-3">
			<div class="step-instructions">
				<h2><span class="step-number">3</span>categorize your story<span class="required">*</span></h2>
				<p>Select all the categories that apply to your story. This will help our community members and readers find the stories that will be the most encouraging to them.</p>
			</div><!-- /.step-instructions -->
			<div class="row">
				<div class="col-sm-6 col-sm-offset-3 mobile-full share-cat-wrap">
					<div class="row">
						<div class="col-sm-6">
							<div class="faux-checkbox"><input type="checkbox" name="story_category[]" value="Friends &amp; Family"<?php echo $errors ? $story->is_checked('story_category', 'Friends & Family') : ''; ?>></div> Friends &amp; Family
						</div><!-- /.col-sm-6 -->
						<div class="col-sm-6">
							<div class="faux-checkbox"><input type="checkbox" name="story_category[]" value="Drug Addiction"<?php echo $errors ? $story->is_checked('story_category', 'Drug Addiction') : ''; ?>></div> Drug Addiction
						</div><!-- /.col-sm-6 -->
						<div class="col-sm-6">
							<div class="faux-checkbox"><input type="checkbox" name="story_category[]" value="Faith"<?php echo $errors ? $story->is_checked('story_category', 'Faith') : ''; ?>></div> Faith
						</div><!-- /.col-sm-6 -->
						<div class="col-sm-6">
							<div class="faux-checkbox"><input type="checkbox" name="story_category[]" value="Alcohol Addiction"<?php echo $errors ? $story->is_checked('story_category', 'Alcohol Addiction') : ''; ?>></div> Alcohol Addiction
						</div><!-- /.col-sm-6 -->
						<div class="col-sm-6">
							<div class="faux-checkbox"><input type="checkbox" name="story_category[]" value="Mental Health"<?php echo $errors ? $story->is_checked('story_category', 'Mental Health') : ''; ?>></div> Mental Health
						</div><!-- /.col-sm-6 -->
						<div class="col-sm-6">
							<div class="faux-checkbox"><input type="checkbox" name="story_category[]" value="Other Addictions"<?php echo $errors ? $story->is_checked('story_category', 'Other Addictions') : ''; ?>></div> Other Addictions
						</div><!-- /.col-sm-6 -->
					</div>
				</div>			
			</div><!-- /.row -->
		</div><!-- /.share-step -->
	</div><!-- /.share-row -->

	<!--
		// The Share
	-->
	<div class="row share-row">
		<div class="col-sm-8 col-sm-offset-2 share-step" id="step-4">
			<div class="step-4-select<?php echo $errors ? ' is-hidden' : ''; ?>">
				<div class="step-instructions">
					<h2><span class="step-number">4</span>Select An Option</h2>
					<p>Would you like to do the <a href="#" class="select-step-link" data-show="qa">Q&amp;A</a> or just <a href="#" class="select-step-link" data-show="written">write your own</a>?</p>
				</div><!-- /.step-instructions -->
			</div><!-- /.step-4-select -->
			<div class="written-content <?php echo $errors && $story->fields['story-type'] == 'written'  ? '' : 'is-hidden'; ?>" data-step="written">
				<div class="step-instructions">
					<h2><span class="step-number">4</span>Write Your Own<span class="required">*</span></h2>
					<p>Share your story in your own words. We recommend pasting in content from a text editor on your computer (e.g. Word, Pages, etc.).</p>
				</div><!-- /.step-instructions -->
				<div class="step-fields">
					<div class="field-wrap">
						<label for="share-written-story">Write Below<span class="required">*</span></label>
						<textarea name="written-story" id="share-written-story"><?php echo $story->fields['written-story'] && $errors ? $story->fields['written-story'] : ''; ?></textarea>
					</div><!-- /.field-wrap -->
				</div><!-- /.step-fields -->
			</div><!-- /written -->
			<div class="qa-content <?php echo $errors && $story->fields['story-type'] == 'qa'  ? '' : 'is-hidden'; ?>" data-step="qa">
				<div class="step-instructions">
					<h2><span class="step-number">4</span>SHARE YOUR ANSWERS<span class="required">*</span></h2>
					<p>Here are six questions that relate to recovery. Your story will be posted in a Q &amp; A style. Take all the time you need, and skipping a question is just fine.</p>
				</div><!-- /.step-instructions -->
				<div class="step-fields">
					<div class="field-wrap">
						<label for="share-title">Title<span class="required">*</span></label>
						<input type="text" name="title" id="share-title" value="<?php echo $story->fields['title'] && $errors ? $story->fields['title'] : ''; ?>">
						<small>If your journey had a name, what would it be?</small>
					</div><!-- /.field-wrap -->
					<div class="field-wrap">
						<label for="share-question-1">Start by describing the situation that changed your life or a loved one's life.<span class="required">*</span></label>
						<textarea name="question-1" id="share-question-1"><?php echo $story->fields['question-1'] && $errors ? $story->fields['question-1'] : ''; ?></textarea>
					</div><!-- /.field-wrap -->
					<div class="field-wrap">
						<label for="share-question-2">Based on your situation or story, was there a turning point that prompted the need for change or help?</label>
						<textarea name="question-2" id="share-question-2"><?php echo $story->fields['question-2'] && $errors ? $story->fields['question-2'] : ''; ?></textarea>
					</div><!-- /.field-wrap -->	
					<div class="field-wrap">
						<label for="share-question-3">How did you or your HERO get help?</label>
						<textarea name="question-3" id="share-question-3"><?php echo $story->fields['question-3'] && $errors ? $story->fields['question-3'] : ''; ?></textarea>
					</div><!-- /.field-wrap -->
					<div class="field-wrap">
						<label for="share-question-4">Based on your experience, what lessons did you learn? Do you have any advice to give?</label>
						<textarea name="question-4" id="share-question-4"><?php echo $story->fields['question-4'] && $errors ? $story->fields['question-4'] : ''; ?></textarea>
					</div><!-- /.field-wrap -->
					<div class="field-wrap">
						<label for="share-question-5">If you or your loved one is in recovery, describe what life is like today.</label>
						<textarea name="question-5" id="share-question-5"><?php echo $story->fields['question-5'] && $errors ? $story->fields['question-5'] : ''; ?></textarea>
					</div><!-- /.field-wrap -->
					<div class="field-wrap">
						<label for="share-question-6">Is there anything else you'd like to share?</label>
						<textarea name="question-6" id="share-question-6"><?php echo $story->fields['question-6'] && $errors ? $story->fields['question-6'] : ''; ?></textarea>
					</div><!-- /.field-wrap -->	
				</div><!-- /.step-fields -->
			</div><!-- /qa -->
		</div><!-- /.share-step -->
	</div><!-- /.share-row -->


	<!--
		// The Date
	-->
	<div class="row share-row">
		<div class="col-sm-8 col-sm-offset-2 share-step" id="step-5">
			<div class="step-5-select<?php echo $errors ? ' is-hidden' : ''; ?>">
				<div class="step-instructions">
					<h2><span class="step-number">5</span>Select An Option</h2>
					<p>Would you like to do the <a href="#" class="select-step-link" data-show="qa">Q&amp;A</a> or just <a href="#" class="select-step-link" data-show="written">write your own</a>?</p>
				</div><!-- /.step-instructions -->
			</div><!-- /.step-5-select -->
				<div class="step-instructions">
					<h2><span class="step-number">5</span>Share Your Recovery Date With Us<span class="required">*</span></h2>
					<p>Share Your Important Date Here (e.g. Recovery Date, Sober Birthday, Meaningful Date, Date You Made A Change, etc.).</p>
				</div><!-- /.step-instructions -->
				<div class="step-fields">
					<div class="field-wrap">
						<div class="center">
						<label for="datetime">Write Below<span class="required">*</span></label>
							<input type="text" name="share-date" id="datetime" value="<?php echo $story->fields['share-date'] && $errors ? $story->fields['share-date'] : ''; ?>">
						</div>
					</div><!-- /.field-wrap -->
				</div><!-- /.step-fields -->
			<div class="field-wrap last-checkboxes">
				<div class="faux-checkbox"><input type="checkbox" name="newsletter" value="true" checked<?php echo $errors ? $story->is_checked('newsletter', 'true') : ''; ?>></div> <span>Sign up for HEROES newsletter</span><br>
				<div class="faux-checkbox"><input type="checkbox" name="terms" value="true"<?php echo $errors ? $story->is_checked('terms', 'true') : ''; ?>></div> <span>Confirm you have read and agreed to our <a href="#terms" class="pop-up">Terms of Use</a>, <a href="#privacy-policy" class="pop-up">Privacy Policy</a>, <a href="#privacy-guide" class="pop-up">Privacy Guide</a> and <a href="#publication-release" class="pop-up">Publication Release Form</a></span>
			</div><!-- /.field-wrap -->		
			<div class="field-wrap share-submit-wrap">
				<div class="faux-btn"><input type="submit" id="share-submit-btn" class="hir-btn-button" value="Submit Your Story"></div>
			</div><!-- /.field-wrap -->		
		</div><!-- /.share-step -->
	</div><!-- /.share-row -->

	<div id="upload" class="is-hidden">		
		<h3>Upload</h3>
		<p>Please select a file to upload below:</p>
		<?php if($story->heroImage): ?>
		<p><img src="<?php echo $story->heroImage; ?>" style="width:200px;height:auto;"></p>
		<?php else: ?>
		<p><?php echo $story->heroImage ? 'class="is-hidden"' : ''; ?>><input type="file" name="hero_image" id="share-hero-img"></p>
		<?php endif; ?>
		<a class="select-upload hir-btn slim" data-button="upload-btn">Continue</a>
	</div><!-- /#upload -->

	<div id="avatar" class="is-hidden">
		<div id="avatar-wrap">
			<ul id="avatar-list">
				<li>
					<a href="#">
						<img class="attachment-thumbnail" width="150" height="150" alt="SERENITY" src="<?php echo get_bloginfo('url'); ?>/wp-content/uploads/SERENITY-150x150.jpg">
					</a>
				</li>
				<li>
					<a href="#">
						<img class="attachment-thumbnail" width="150" height="150" alt="RENEW" src="<?php echo get_bloginfo('url'); ?>/wp-content/uploads/RENEW-150x150.jpg">
					</a>
				</li>
				<li>
					<a href="#">
						<img class="attachment-thumbnail" width="150" height="150" alt="OVERCOME" src="<?php echo get_bloginfo('url'); ?>/wp-content/uploads/OVERCOME-150x150.jpg">
					</a>
				</li>
				<li>
					<a href="#">
						<img class="attachment-thumbnail" width="150" height="150" alt="LIVE" src="<?php echo get_bloginfo('url'); ?>/wp-content/uploads/LIVE-150x150.jpg">
					</a>
				</li>
				<li>
					<a href="#">
						<img class="attachment-thumbnail" width="150" height="150" alt="INSPIRE" src="<?php echo get_bloginfo('url'); ?>/wp-content/uploads/INSPIRE-150x150.jpg">
					</a>
				</li>
				<li>
					<a href="#">
						<img class="attachment-thumbnail" width="150" height="150" alt="HOPE" src="<?php echo get_bloginfo('url'); ?>/wp-content/uploads/HOPE-150x150.jpg">
					</a>
				</li>
				<li>
					<a href="#">
						<img class="attachment-thumbnail" width="150" height="150" alt="HONESTY" src="<?php echo get_bloginfo('url'); ?>/wp-content/uploads/HONESTY-150x150.jpg">
					</a>
				</li>
				<li>
					<a href="#">
						<img class="attachment-thumbnail" width="150" height="150" alt="HEAL" src="<?php echo get_bloginfo('url'); ?>/wp-content/uploads/HEAL-150x150.jpg">
					</a>
				</li>
				<li>
					<a href="#">
						<img class="attachment-thumbnail" width="150" height="150" alt="GROW" src="<?php echo get_bloginfo('url'); ?>/wp-content/uploads/GROW-150x150.jpg">
					</a>
				</li>
				<li>
					<a href="#">
						<img class="attachment-thumbnail" width="150" height="150" alt="FREE" src="<?php echo get_bloginfo('url'); ?>/wp-content/uploads/FREE-150x150.jpg">
					</a>
				</li>
				<li>
					<a href="#">
						<img class="attachment-thumbnail" width="150" height="150" alt="FORGIVE" src="<?php echo get_bloginfo('url'); ?>/wp-content/uploads/FORGIVE-150x150.jpg">
					</a>
				</li>
				<li>
					<a href="#">
						<img class="attachment-thumbnail" width="150" height="150" alt="FAITH" src="<?php echo get_bloginfo('url'); ?>/wp-content/uploads/FAITH-150x150.jpg">
					</a>
				</li>
				<li>
					<a href="#">
						<img class="attachment-thumbnail" width="150" height="150" alt="EXPLORE" src="<?php echo get_bloginfo('url'); ?>/wp-content/uploads/EXPLORE-150x150.jpg">
					</a>
				</li>
				<li>
					<a href="#">
						<img class="attachment-thumbnail" width="150" height="150" alt="EVOLVE" src="<?php echo get_bloginfo('url'); ?>/wp-content/uploads/EVOLVE-150x150.jpg">
					</a>
				</li>
				<li>
					<a href="#">
						<img class="attachment-thumbnail" width="150" height="150" alt="EMPOWER" src="<?php echo get_bloginfo('url'); ?>/wp-content/uploads/EMPOWER-150x150.jpg">
					</a>
				</li>
				<li>
					<a href="#">
						<img class="attachment-thumbnail" width="150" height="150" alt="COURAGE" src="<?php echo get_bloginfo('url'); ?>/wp-content/uploads/COURAGE-150x150.jpg">
					</a>
				</li>
				<li>
					<a href="#">
						<img class="attachment-thumbnail" width="150" height="150" alt="BELIEVE" src="<?php echo get_bloginfo('url'); ?>/wp-content/uploads/BELIEVE-150x150.jpg">
					</a>
				</li>			
			</ul>
			<div class="clearfix"></div>
			<a class="select-upload hir-btn slim" data-button="avatar-btn">Continue</a>			
			<input type="hidden" name="avatar" id="share-avatar" value="<?php echo $story->fields['avatar'] ? $story->fields['avatar'] : ''; ?>">
		</div><!-- /#avatar-wrap -->
	</div><!-- /#avatar -->
	<div id="publication-release" class="is-hidden">
		<h1 style="text-align:center;">Publication Release Form</h1>
		<p>I hereby grant to Foundations Recovery Network the right and license to use my name, comments, and photos in Foundations Recovery Network's materials for internal and external audiences. These materials include but are not limited to advertisements, brochures, news releases, magazines, newspapers, newsletters, videos and websites.</p>
	</div>
	<div id="privacy-policy" class="is-hidden">
		<h1 style="text-align:center;">Privacy Policy</h1>
		<p style="text-align: left;"><strong>Notice of Privacy Practices</strong></p>
		<p style="text-align: left;">THIS NOTICE DESCRIBES HOW MEDICAL INFORMATION ABOUT YOU MAY BE USED AND DISCLOSED AND HOW YOU CAN GET ACCESS TO THIS INFORMATION. PLEASE REVIEW IT CAREFULLY.</p>
		<p style="text-align: left;">This Notice Of Privacy Practices is provided to you under the Health Insurance Portability and Accountability Act and its implementing regulations (HIPAA) and applies to all records received and created about your physical and mental condition and treatment, and about billing and payment for such treatment (together “PHI”), that may be maintained, used, and/or disclosed by Foundations Recovery Network and/or its Affiliated Covered Entities (listed on the last page of this Notice and collectively referenced hereafter as “FRN”), and all FRN workforce members, volunteers, medical staff, and contractors. Let’s discuss Organized Health Care Arrangements.</p>
		<p style="text-align: left;"><strong>EFFECTIVE DATE</strong></p>
		<p style="text-align: left;">This Notice is effective as of September 23, 2013. FRN reserves the right to revise this Notice. If revisions are material, we will promptly revise and distribute a revised Notice by mail, e-mail (if you have agreed to electronic notice), hand delivery, or by posting on our website, as required by law. A copy of the current Notice will be made available to you when you initially register with FRN for treatment or services, upon your request, and on subsequent visits if the Notice has been revised. In addition, the Notice will be posted at the registration desk.</p>
		<p style="text-align: left;"><strong>FRN’S COMMITMENT TO PRIVACY</strong></p>
		<p style="text-align: left;">The privacy protections described in this Notice reflect FRN’s commitment to protecting your privacy and to complying with HIPAA and related federal and state privacy and security laws (collectively hereafter “Privacy Laws”), which require FRN to maintain the privacy and security of your PHI; to provide you with this Notice; to notify you of any unauthorized disclosure, use or other breach of unsecured PHI; and to abide by the terms of this Notice.</p>
		<p style="text-align: left;"><strong>PERMITTED USES AND DISCLOSURES</strong></p>
		<p style="text-align: left;">The following describes and provides examples of how FRN may use and disclose your PHI without your authorization. Any use or disclosure that does not fall within one of the following categories requires your written authorization, and your authorization may be revoked by you at any time. State and/or federal laws may also place restrictions on the manner in which specific types of PHI may be used and/or to whom such medical information may be disclosed, such as certain drug and alcohol information, HIV information, alcohol and substance abuse treatment, mental health treatment, and genetic information. In those instances where the use and/or disclosure of this PHI is specifically restricted, we will seek appropriate authorization from you, your legal representative or a valid court order before using or disclosing this information, unless required in a medical emergency or, in the case of drug or alcohol abuse programs, the disclosure is authorized by applicable state and federal laws and regulations governing drug or alcohol abuse. If a use or disclosure of health information described in this Notice is prohibited or materially limited by state law, it is our intent to meet the requirements of the more stringent law.</p>
		<p style="text-align: left;"><strong>Treatment.</strong> FRN may receive PHI from and share PHI with health care providers involved in your treatment before, during, and after your stay with FRN. For example, FRN may provide physicians and therapists access to your medical records in connection with providing you with care, or to a pharmacist in connection with requesting a prescription to identify potential interactions or allergies. In the event of your incapacity or an emergency, FRN may also disclose your medical information based on our professional judgment of whether the disclosure would be in your best interests.</p>
		<p style="text-align: left;"><strong>Payment.</strong> FRN will use your PHI for purposes of obtaining payment for your care. For example, FRN will provide information about the services that will be or were provided to you so that your insurance company or health plan may pay us or reimburse you. FRN may also provide information regarding sources of payment to practitioners outside of FRN who are 2 involved in your care to enable them to obtain payment.</p>
		<p style="text-align: left;"><strong>Health Care Operations. </strong>FRN may use or disclose PHI in connection with managing and operating the organization. For example, FRN may use and/or share your PHI in connection with providing you with appointment reminders; evaluating FRN’s performance and the quality of care provided; averting a serious threat to health or safety; legal services and audit functions, including fraud and abuse detection, compliance programs, and due diligence activities; licensing and accreditation; business planning and development; in determining what additional services we should offer, what services are no longer needed, and whether certain new treatments are effective; and in certain circumstances where you have not otherwise objected, in making reports to public or private entities authorized by law or charter to assist in disaster relief efforts (such as the Red Cross) to notify a family member or personal representative of your location or general condition. We may also disclose your health information to business associates with whom we contract to provide services where such business associates agree to appropriately safeguard your PHI.</p>
		<p style="text-align: left;"><strong>Research.</strong> FRN may use and share PHI for research projects, if approved by a special process that balances the research needs with patients’ need for privacy of their PHI. For example, research could include comparing the health and recovery of all patients who have the same condition, but were treated with different medications. However, in preparing to start a research project, FRN may share PHI with researchers without authorization or approval, as long as the PHI does not leave the FRN. For example, PHI may be shared with researchers at the FRN to identify patients who may want to participate in a research study.</p>
		<p style="text-align: left;"><strong>REQUIRED AND OTHER PERMITTED USES AND DISCLOSURES</strong></p>
		<p style="text-align: left;">FRN may make certain disclosures of your PHI as and when required or otherwise authorized by law, and will limit the use or disclosure to the amount of PHI necessary to comply with and/or serve the purposes of the relevant federal, state, or local laws or ordinances, or the legitimate needs of responsible, authorized agencies in fulfilling their purposes, including, for example:</p>

		<ul style="text-align: left;">
			<li>to the United States Department of Health and Human Services as part of an investigation or determination of compliance with relevant laws;</li>
			<li>to a state agency for activities such as audits and inspections;</li>
			<li>to law enforcement as part of an investigation or to a government authority authorized by law to receive reports of abuse, neglect, or domestic violence;</li>
			<li>to a court or administrative law judge or other tribunal for judicial or administrative proceedings and/or as required by court or administrative orders, subpoenas, and/or other lawful process unless the state has more restrictive laws;1</li>
			<li>to a public health authority which is permitted by law to collect or receive such information for the purpose of preventing or controlling disease, injury, vital events such as death, child abuse or neglect; of conducting public health surveillance, investigation and/or intervention; and reporting adverse reactions to medications or problems with regulated products;</li>
			<li>to a health oversight agency for oversight activities authorized by law, such as audits, investigations, and inspections.</li>
			<li>to a law enforcement official for a law enforcement/emergency purpose as required by law, in compliance with a court order from a court of competent jurisdiction granted after application showing good cause for the issuance of the order, or to investigate a crime occurring on our premises.</li>
			<li>to coroners, medical examiners or funeral directors consistent with applicable law to carry out their duties.</li>
			<li>to organ or tissue procurement organizations to facilitate the donation of organs, eyes or tissues after your death; and</li>
			<li>for specialized governmental functions, such as national security, intelligence activities, and for the provision of protective services to the President to the extent required by Federal and State laws.</li>
			<li>to you or your legal representative. Some state laws concerning minors permit or require disclosure of PHI to parents, guardians, and persons acting in a similar legal status. FRN will act consistently with the law of the state where the treatment is provided and will make disclosures in accordance with such laws.</li>
		</ul>
		<p style="text-align: left;">Under the laws of the state of Georgia, records are produced in response to a court order issued by a court of competent jurisdiction pursuant to a full and fair show cause hearing.</p>
		<p style="text-align: left;"><strong>USES AND DISCLOSURES TO WHICH YOU MAY AGREE OR OBJECT</strong></p>
		<p style="text-align: left;">FRN will inform you in advance of certain uses and disclosures and if you agree or express no objection, may disclose your PHI, for example:</p>

		<ul style="text-align: left;">
			<li>relevant PHI may be disclosed to a family member, friend or any other person you identify for that person to be involved in or support your health care or payment related to your health care or to notify a family member, your personal representative, or other person responsible for your care of your location, general condition, or death unless doing so is inconsistent with any prior expressed preference you make to us.</li>
			<li>FRN may send PHI via email, text message or through a reasonably requested method or medium to you, other persons you designate, and to those involved in the delivery of your health care. You should know that if PHI is transmitted outside of FRN by e-mail or text message, there is some level of risk that the information in the email/text could be read by a third party.</li>
		</ul>
		<p style="text-align: left;"><strong>USES AND DISCLOSURES TO WHICH YOU MUST AGREE IN WRITING</strong></p>
		<p style="text-align: left;"><strong>Marketing and Sale of PHI.</strong> We will not use your PHI for marketing purposes and will not sell your PHI without your written authorization unless permitted or required by state and federal law. Marketing includes communications with you about someone else’s product or service about which we are paid to communicate with you other than refill reminders, face to face communications, and promotional gifts of nominal value. We will not sell your PHI unless permitted by law. For example, if we sold a facility to someone else regulated by HIPAA, that new operator may receive medical records.</p>
		<p style="text-align: left;"><strong>Psychotherapy Notes.</strong> In recognition of the special confidentiality of psychotherapy notes recorded by a mental health provider in a counseling session, HIPAA permits us to separate these notes from the rest of your medical record. When we do, we will not use or disclose your psychotherapy notes without your written authorization except for use by the originator for treatment, to defend ourselves in a legal action or proceeding you bring, to someone reasonably able to prevent or lessen a serious and imminent threat to the health or safety of a person or the public consistent with legal and professional standards if we believe it is necessary to prevent or lessen that serious and imminent threat, for our own supervised mental health training programs, or as otherwise permitted or required by state and federal law. Psychotherapy notes do not include medication prescription and monitoring, counseling session start and stop times, the modalities and frequencies of treatment furnished, results of clinical tests, or any summary of diagnosis, functional status, treatment plan, symptoms, prognosis or progress to date.</p>
		<p style="text-align: left;"><strong>Uses and Disclosure of Your PHI Not Covered By This Notice Or By Law.</strong> Uses and disclosures of your PHI not covered by this Notice or applicable law may be made only with your written authorization, which you may revoke as described below.</p>
		<p style="text-align: left;"><strong>YOUR RIGHTS REGARDING PHI.</strong></p>
		<p style="text-align: left;">As an FRN patient, you have the following rights with regard to your PHI. Right to Request Restrictions. You have the right to request limits on the use or disclosure of your PHI for treatment, payment, and/or health care operations. You also have the right to request a limit on PHI we disclose to someone who is involved in your care or the payment of your care, such as a family member or friend. For example, you may ask that we not disclose information about a treatment you have received. To request restrictions, the request must be made in writing to the Privacy Officer as set forth below. In your request you must tell us (1) what information you want restricted; (2) whether you want to restrict our use, disclosure, or both; (3) to whom you want the restriction to apply; and (4) the expiration date of the restriction(s). We are not required to agree to your request except in limited circumstances where you, or someone on your behalf, paid out of pocket and in full for the items or services and have requested that we not disclose your PHI to a health plan unless the disclosure is required by law. If we do agree, we will comply with your restrictions unless the information is needed to provide emergency treatment.</p>
		<p style="text-align: left;"><strong>Right to Make Requests Regarding Method Or Means of Communicating PHI.</strong> You have the right to request that we communicate with you about medical matters in a certain way or at a certain location. For example, you may ask that we only contact you at work or by mail. Your request must specify how or where you wish to be contacted. We will accommodate reasonable requests made in writing to the Privacy Officer as set forth below.</p>
		<p style="text-align: left;"><strong>Right to Inspect and Copy PHI.</strong> You have the right to inspect and/or receive a copy of PHI contained in a “designated record set” for as long as we maintain it, except for psychotherapy notes, information compiled in reasonable anticipation of, or for use in, a civil, criminal or administrative action or proceeding, or PHI that may not be disclosed under the Clinical Laboratory Improvements Amendments of 1988. A designated record set contains medical and billing records and any other records that FRN uses to make decisions about your care or payment for your care. While HIPAA does not require us to provide you with access to psychotherapy notes, we may allow you such access upon written request if FRN decides, based on a clinical assessment, that doing so may not be harmful to you. We do not disclose actual test questions or raw data of psychological tests that are protected by copyright laws. You have the right to receive an electronic copy of any of your designated record set that is maintained in an electronic format (known as an electronic medical record or an electronic health record), and to request that the copy be given to you or transmitted to another individual or entity. We may charge a reasonable, cost-based fee in accordance with applicable state and federal law for copying and mailing your records, including portable media such as a CD or DVD if you so request. We may deny your request in certain limited circumstances. If your request is denied, you may request that your denial be reviewed. Such reviews will be performed by an independent licensed healthcare professional chosen by our Privacy Officer. We will comply with the outcome of the review.</p>
		<p style="text-align: left;"><strong>Right to Amend.</strong> If you believe that the information we have about you is incorrect or incomplete, you may request an amendment to your PHI in a designated record set. You may submit a request for amendment in writing to the Privacy Officer, with a reason you wish to make the amendment. While we accept requests for amendment, we are not required to agree to them. We may deny your request if you ask us to amend information that was not created by us, is not part of your designated record set, or if the information is determined to be accurate and complete as it is. If we deny your request, we will provide you with a written denial and you will be given the opportunity to submit a written statement disagreeing with the denial. We will include this information in your medical record. If we grant your request, we will inform you in a timely fashion, make the amendment, and provide appropriate notification.</p>
		<p style="text-align: left;"><strong>Right to Revoke your Authorization.</strong> If you provide us with an authorization to use or disclose your PHI, you may revoke that authorization, in writing, at any time, and we will honor your request(s), except as required, prohibited, or permitted by law. Such revocation will not apply to any action that FRN took in reliance on your authorization prior to the revocation’s receipt.</p>
		<p style="text-align: left;"><strong>Right to Breach Notification.</strong> In certain instances, you have the right to be notified if we, or one of our Business Associates, discover an inappropriate use or disclosure of your PHI. Notice of any such use or disclosure will be made in accordance with state and federal requirements.</p>
		<p style="text-align: left;"><strong>Right to Accounting of Disclosures.</strong> You have the right to request an "accounting of disclosures." This is a list of disclosures that we have made of your PHI. We are not required to list certain disclosures, including (1) disclosures made for treatment, payment, and health care operations purposes, (2) disclosures made with your authorization, (3) disclosures made to create a limited data set, (4) disclosures made directly to you, (5) disclosures permitted or required by the Federal HIPAA Privacy Rule, and/or (6) disclosures occurring prior to April 14, 2003. You must submit your request in writing to our Privacy Officer. Your request must state a time period which may not be longer than 6 years before your request. Your request should indicate in what form you would like the accounting (for example, on paper or by e-mail). The first accounting you request within any 12-month period will be free. For additional requests, we may charge you for the reasonable costs of providing the accounting. We will notify you of the costs involved and you may choose to withdraw or modify your request before any costs are incurred.</p>
		<p style="text-align: left;"><strong>Right to a Paper Copy of this Notice.</strong> You have a right to a paper copy of this Notice, even if you agreed to receive it electronically. Please contact us as directed below to obtain this Notice in written form.</p>
		<p style="text-align: left;"><strong>Right to Foreign Language Version.</strong> If you have difficulty reading or understanding English, you may request a copy of this Notice in another language.</p>
		<p style="text-align: left;"><strong>CONTACT INFORMATION FOR PRIVACY OFFICER</strong>
		If you would like more information about our privacy practices or have questions or concerns about this Notice, please contact the following:</p>
		<p style="text-align: left;"><strong>Trudi Lovinski, Compliance/ Privacy Officer Foundations Recovery Network</strong>
		5409 Maryland Way, Suite 320
		Brentwood, TN 37027
		615-345-3205</p>
		<p style="text-align: left;">If you believe your privacy rights have been violated, you may file a complaint, in writing, to either of the above or you may contact the U.S. Department of Health and Human Services (HHS). You will not be penalized or retaliated against in any way for making a complaint.</p>
		<p style="text-align: left;"><strong>FOUNDATIONS RECOVERY NETWORK’S AFFILIATED COVERED ENTITIES</strong>
		This Notice applies to the privacy practices of the following Foundations Recovery Network’s Affiliated Covered Entities who, for the purposes of HIPAA, designate themselves as a Single Affiliated Covered Entity:</p>
		<p style="text-align: left;"><span style="text-decoration: underline;">Inpatient Centers</span>
		The Canyon at Peace Park, Malibu, CA Michael’s House, Palm Springs, CA La Paloma, Memphis, TN</p>
		<p style="text-align: left;"><span style="text-decoration: underline;">Outpatient Centers</span>
		Foundations Atlanta, Roswell, GA Foundations Nashville, Nashville, TN Michael’s House, Palm Springs, CA La Paloma Outpatient Center, Memphis, TN The Canyon, Santa Monica, CA</p>
	</div>
	<div id="privacy-guide" class="is-hidden">
		<h1 style="text-align:center;">Privacy Guide</h1>
		<p style="text-align: left;"><strong>ANONYMITY </strong></p>
		<p style="text-align: left;">You may tell your story of recovery and use your name and photograph without revealing your membership in Alcoholics Anonymous (AA), NA, CA, SA, or any other 12-Step groups. Personal anonymity for members of AA is a cherished gift granted by the Twelve Traditions of AA. Please refer to www.aa.org and the pamphlet “Understanding Anonymity” for more information on anonymity.</p>
		<p style="text-align: left;"><strong>PHOTOGRAPHS </strong></p>
		<p style="text-align: left;">Please consider your use of a photograph carefully. Photographs are OPTIONAL. If you use a photograph, you may use a personal photograph, but you may also use the photograph of an object, place, or symbol that relates to your story. Heroes in Recovery strongly discourages the use of a personal photograph if you have less than one year of sobriety.</p>
		<p style="text-align: left;"><strong>NAMES </strong></p>
		<p style="text-align: left;">Please consider the use of your full name carefully. Full names are OPTIONAL. You may choose to use a first name and last initial or you may decide to use a pseudonym. Heroes in Recovery strongly discourages the use your full name if you have less than one year of sobriety.</p>
		<p style="text-align: left;"><strong>TREATMENT CENTER OR PROGRAM NAMES </strong></p>
		<p style="text-align: left;">Heroes in Recovery reserves the right to edit treatment center or program names. We review all stories. The focus of the story should be on personal recovery and not an advertisement for treatment at any particular center or program.</p>
		<p style="text-align: left;"><strong>MORE ON ANONYMITY AND 12-STEP GROUPS </strong></p>
		<p style="text-align: left;"><em>The principle of anonymity within Alcoholics Anonymous and other 12-Step groups was established to keep the primary focus on helping alcoholics and addicts and to ensure a safe space for people to recover. </em></p>
		<p style="text-align: left;">Anonymity with the media is a cornerstone principle of many 12-Step groups and recovery programs. It is an essential element of success because it gives the recovering person the protection he or she needs from outside scrutiny.</p>
		<p style="text-align: left;">Anonymity also plays a crucial role in establishing personal humility, which is a cornerstone of the spiritual foundation of recovery.</p>
		<p style="text-align: left;">Here are the traditions that lay out the principle of anonymity as it applies to many 12- Step groups and programs.</p>
		<p style="text-align: left;"><strong>Tradition 6 </strong></p>
		<p style="text-align: left;">“A [12-Step group] ought never endorse, finance or lend the [12-Step group] name to any related facility or outside enterprise, lest problems of money, property and prestige divert us from our primary purpose.”</p>
		<p style="text-align: left;"><strong>Tradition 10 </strong></p>
		<p style="text-align: left;">“The [12-Step group] has no opinion on outside issues; hence, the [12-Step group] ought never to be drawn into any public controversy.”</p>
		<p style="text-align: left;"><strong>Tradition 11 </strong></p>
		<p style="text-align: left;">“Our public relations policy is based on attraction rather than promotion; we need always maintain personal anonymity at the level of press, radio, and films.” You can share your story about recovery and advocate for the rights of others, as long as you do not involve the 12-Step group or AA by name.</p>
		<p style="text-align: left;">Thank you for sharing your story with us and adding to our collective voice at Heroes in Recovery!</p>
	</div>
	<div id="terms" class="is-hidden">
		<h1 style="text-align:center;">Terms of Use</h1>
		<p>By using HeroesinRecovery.com you acknowledge that you have read and understand <span style="line-height: 1.5;">the HeroesinRecovery.com Terms of Service (TOS).</span></p>
		<p><strong>Opinions expressed on this site belong to the individual poster.</strong></p>
		<p>Our website which provides an opportunity for participants to express comments and opinions about recovery and treatment. These postings do not necessarily reflect the opinion of anyone at Heroes in Recovery, and are not endorsed by Heroes in Recovery. While we will do review and moderate the comments in order to remove objectionable material, we may not always get to it immediately.</p>
		<p><strong>No medical or psychiatric advice</strong></p>
		<p>Heroes in Recovery does not provide medical advice or offer medical or psychiatric opinions online. We do post articles regarding recovery and treatment for the public benefit. While we will monitor content as time allows, we are not responsible for the opinions, advice, recommendations, advertising for suggestions of any writer of these articles. Anyone who has a medical problem should contact a physician. Anyone with a serious addiction problem should get medical or therapeutic help. Anyone who feels at risk for suicide should call 911 immediately. The function of this site is to provide information regarding the Heroes movement and post relevant blog posts and stories which we think might be helpful to those seeking treatment.</p>
		<p>Trust in any information found on HeroesinRecovery.com, any of its licensors, or any affiliates, rests in the user at the users own risk. HeroesinRecovery.com may contain information that you may find explicit or otherwise offensive. Heroes in Recovery, its affiliates and licensors are not responsible for such content. The user is responsible for complying with applicable laws of the region/country of residence.</p>
		<p><strong>No offensive material</strong></p>
		<p>Anyone who leaves a comment or story on this website is expected to follow certain content rules, including the prohibition of any content which is illegal or otherwise prohibited. Illegal or prohibited content would include content which holds itself out as medical or legal advice, which invites other participants to engage in a criminal activity, which offers other participants an opportunity to participate in a financial scheme, pyramid, chain letter, or otherwise invites participants to send money to the poster, or which contains sexually suggestive, racially offensive, threatening or abusive language. We do not provide treatment for individuals under age 18, nor do we knowingly collect their contact information. The right to determine what material offends the online community will be the sole decision of the moderator.</p>
		<p><strong>Links to other websites</strong></p>
		<p>This website will contain links to Third Party sites which will contain content over which HeroesinRecovery.com has no control. Bear in mind when you follow a link to another website that HeroesinRecovery.com has no responsibility for what you find there, and does not review, moderate or maintain those sites. We do not necessarily endorse or recommend their products. Follow those links, and use those websites at your own risk.</p>
		<p><strong>Agreement</strong></p>
		<p>By using this website, sending a request for information or posting a comment online, I understand and acknowledge all of the above terms, and in particular I understand that from time to time HeroesinRecovery.com will make changes regarding their website, their policies and their rules.</p>
	</div>
	</form>
</div><!-- /#share-form-wrap -->
<?php get_footer(); ?>
<?php
/**
 * The Loop - where the magic happens...
 */
?>
<?php if (have_posts()): while (have_posts()) : the_post(); ?>
	<article id="post-<?php the_ID(); ?>" class="blog-post-row row">
		<div class="blog-post-loop-img col-sm-2"><div class="blog-post-loop-img-wrap">			
			<?php echo get_avatar( get_the_author_meta( 'email' ) ); ?>
		</div></div><!-- /.blog-post-loop-img -->
		<div class="blog-post-loop-content col-sm-10">
			<h3><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
			<div class="blog-post-loop-meta">
				<?php the_date(); ?> by <a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><?php the_author_meta( 'display_name' ); ?></a>
			</div><!-- /.blog-post-loop-meta -->
			<?php the_excerpt(); ?>
			<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">Continue Reading</a>
		</div><!-- /.blog-post-loop-content -->
	</article><!-- /.row -->
<?php endwhile; ?>

<?php else: ?>
	<article id="post-<?php the_ID(); ?>" class="blog-post-row row">
		<strong>No Posts Found</strong>
	</article>
<?php endif; ?>
<?php
/**
 * Template Name: Virtual 6k Race
 */
get_header();
?>
<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/css/reveal.css"/>
<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/css/virtual-race.css"/>

<!-- Subnav -->
	<div class="tab-nav-row row nu-6k-tab">
		<nav class="tab-nav col-sm-10 col-sm-offset-1">
			<ul>
				<li><a href="#levels" title="About" class="tab-link active" data-target="about">Levels</a></li>
				<li><a href="#charity" title="Charity" class="tab-link" data-target="charity">Charity</a></li>
				<li><a href="#social" title="Volunteer" class="tab-link" data-target="get-social">Get Social</a></li>
				<li><a href="#faq" title="Course" class="tab-link" data-target="faq">FAQ</a></li>
			</ul>
		</nav><!-- /.tab-nav -->
	</div><!-- /.row -->

<!-- Content -->
	<div class="page-row row virtual-background" itemscope itemtype="http://schema.org/Event">
		<article class="page-content-wrap col-sm-10 col-sm-offset-1">
			<div class="tabbed-content">
				<div id="tab-1" class="tab-wrap" data-tab="about">
					<div class="row">
						<p style="text-align: center; color: #fff;">Virtual participation means running or walking anytime, anywhere and we'll send you some pretty cool gear to help you spread the Heroes in Recovery message. Still have questions? Check out our FAQ or Contact Us!</p>
						
						<!-- Level 6 -->
							<div class="level level--6 col-sm-12 col-md-6 col-lg-4">
								<div class="virtual-card first">
									<h2>The Ultimate Heroes Package</h2>
									<div class="virtual-wrapper">
										<p>Run or walk <strong>six 6Ks</strong> any time, anywhere.</p>
										<h4 class="virtual-price first"><small>$</small>110</h4>
										<hr>
										<div class="row virtual-list">
											<ul class="col-sm-6">
												<li>Medal</li>
												<li>Bib</li>
												<li>Resuable Bag</li>
												<li>Magnet</li>
												<li>Race Shirt</li>
											</ul>
											<ul class="col-sm-6">
												<li>Performance socks</li>
												<li>Branded tumbler</li>
												<li>Hat</li>
												<li>Reversible headband</li>
												<li>Limited edition tee</li>
											</ul>
										</div><!-- /.row virtual-list-->
									</div><!-- /.virtual wrapper -->
									<h6 class="virtual-sweeptakes">Entry into Heroes Sweepstakes!</h6>
									<div class="virtual-register first">
										<a href="https://endurancecui.active.com/new/events/50850153/select-race"><button>Register Now</button></a>
										<a data-reveal-id="level-6" data-animation="fade"><button>View Kit</button></a>
									</div><!-- /.virtual register first -->

								</div><!-- /.virtual card first -->
							</div>

						<!-- Level 5 -->
							<div class="level level--5 col-sm-12 col-md-6 col-lg-4">
								<div class="virtual-card">
									<h2>Level 5</h2>
									<div class="virtual-wrapper">
										<p>Run or walk <strong>five 6Ks</strong> any time, anywhere.</p>
										<h4 class="virtual-price"><small>$</small>100</h4>
										<hr>
										<div class="row virtual-list">
											<ul class="col-sm-6">
												<li>Medal</li>
												<li>Bib</li>
												<li>Resuable Bag</li>
												<li>Magnet</li>
												<li>Race Shirt</li>
											</ul>
											<ul class="col-sm-6">
												<li>Performance socks</li>
												<li>Branded tumbler</li>
												<li>Hat</li>
												<li>Reversible headband</li>
												<li class="not-available">Limited edition tee</li>
											</ul>
										</div><!-- /.row virtual-list -->
									</div><!-- /.virtual wrapper -->
									<div class="virtual-register">
										<a href="https://endurancecui.active.com/new/events/50850153/select-race"><button>Register Now</button></a>
										<a data-reveal-id="level-5" data-animation="fade"><button>View Kit</button></a>
									</div><!-- /.virtual register -->
								</div><!-- /.virtual card -->
							</div>
	  					<div class="clearfix visible-md"></div>
						
						<!-- Level 4 -->
							<div class="level level--4 col-sm-12 col-md-6 col-lg-4">
								<div class="virtual-card">
									<h2>Level 4</h2>
									<div class="virtual-wrapper">
										<p>Run or walk <strong>four 6Ks</strong> any time, anywhere.</p>
										<h4 class="virtual-price"><small>$</small>70</h4>
										<hr>
										<div class="row virtual-list">
											<ul class="col-sm-6">
												<li>Medal</li>
												<li>Bib</li>
												<li>Resuable Bag</li>
												<li>Magnet</li>
												<li>Race Shirt</li>
											</ul>
											<ul class="col-sm-6">
												<li>Performance socks</li>
												<li>Branded tumbler</li>
												<li class="not-available">Hat</li>
												<li class="not-available">Reversible headband</li>
												<li class="not-available">Limited edition tee</li>
											</ul>
										</div><!-- /.row virtual-list -->
									</div><!-- /.virtual wrapper -->
									<div class="virtual-register">
										<a href="https://endurancecui.active.com/new/events/50850153/select-race"><button>Register Now</button></a>
										<a data-reveal-id="level-4" data-animation="fade"><button>View Kit</button></a>
									</div><!-- /.virtual register -->
								</div><!-- /.virtual card -->
							</div>
	  					<div class="clearfix visible-lg"></div>

	  				<!-- Level 3 -->
							<div class="level level--3 col-sm-12 col-md-6 col-lg-4">
								<div class="virtual-card">
									<h2>Level 3</h2>
									<div class="virtual-wrapper">
										<p>Run or walk <strong>three 6Ks</strong> any time, anywhere.</p>
										<h4 class="virtual-price"><small>$</small>50</h4>
										<hr>
										<div class="row virtual-list">
											<ul class="col-sm-6">
												<li>Medal</li>
												<li>Bib</li>
												<li>Resuable Bag</li>
												<li>Magnet</li>
												<li>Race Shirt</li>
											</ul>
											<ul class="col-sm-6">
												<li>Performance socks</li>
												<li class="not-available">Branded tumbler</li>
												<li class="not-available">Hat</li>
												<li class="not-available">Reversible headband</li>
												<li class="not-available">Limited edition tee</li>
											</ul>
										</div><!-- /.row virtual-list -->
									</div><!-- /.virtual wrapper -->
									<div class="virtual-register">
										<a href="https://endurancecui.active.com/new/events/50850153/select-race"><button>Register Now</button></a>
										<a data-reveal-id="level-3" data-animation="fade"><button>View Kit</button></a>
									</div><!-- /.virtual register -->
								</div><!-- /.virtual card -->
							</div>

						<!-- Level 2 -->
							<div class="level level--2 col-sm-12 col-md-6 col-lg-4">
								<div class="virtual-card">
									<h2>Level 2</h2>
									<div class="virtual-wrapper">
										<p>Run or walk <strong>two 6Ks</strong> any time, anywhere.</p>
										<h4 class="virtual-price"><small>$</small>35</h4>
										<hr>
										<div class="row virtual-list">
											<ul class="col-sm-6">
												<li>Medal</li>
												<li>Bib</li>
												<li>Resuable Bag</li>
												<li>Magnet</li>
												<li>Race Shirt</li>
											</ul>
											<ul class="col-sm-6">
												<li class="not-available">Performance socks</li>
												<li class="not-available">Branded tumbler</li>
												<li class="not-available">Hat</li>
												<li class="not-available">Reversible headband</li>
												<li class="not-available">Limited edition tee</li>
											</ul>
										</div><!-- /.row virtual-list -->
									</div><!-- /.virtual wrapper -->
									<div class="virtual-register">
										<a href="https://endurancecui.active.com/new/events/50850153/select-race"><button>Register Now</button></a>
										<a data-reveal-id="level-2" data-animation="fade"><button>View Kit</button></a>
									</div><!-- /.virtual register -->
								</div><!-- /.virtual card -->
							</div>

						<!-- Level 1 -->
							<div class="level level--1 col-sm-12 col-md-6 col-lg-4">
								<div class="virtual-card">
									<h2>Level 1</h2>
									<div class="virtual-wrapper">
										<p>Run or walk <strong>one 6K</strong> any time, anywhere.</p>
										<h4 class="virtual-price"><small>$</small>20</h4>
										<hr>
										<div class="row virtual-list">
											<ul class="col-sm-6">
												<li>Medal</li>
												<li>Bib</li>
												<li>Resuable Bag</li>
												<li class="not-available">Magnet</li>
												<li class="not-available">Race Shirt</li>
											</ul>
											<ul class="col-sm-6">
												<li class="not-available">Performance socks</li>
												<li class="not-available">Branded tumbler</li>
												<li class="not-available">Hat</li>
												<li class="not-available">Reversible headband</li>
												<li class="not-available">Limited edition tee</li>
											</ul>
										</div><!-- /.row virtual-list -->
									</div><!-- /.virtual wrapper -->
									<div class="virtual-register">
										<a href="https://endurancecui.active.com/new/events/50850153/select-race"><button>Register Now</button></a>
										<a data-reveal-id="level-1" data-animation="fade"><button>View Kit</button></a>
									</div><!-- /.virtual register -->
								</div><!-- /.virtual card -->
							</div>	


					</div><!-- /row -->
				</div><!-- /#tab-1 -->
			</div><!-- /#tabbed content -->
		
				<div id="tab-2" class="tab-wrap" data-tab="charity">
					<?php echo get_field('charity_copy'); ?>
					<div class="page-row row partners-row">
						<article class="page-content-wrap col-sm-10 col-sm-offset-1">
							<div class="row page-content-row" align="center">
					<?php if(get_field('all_charity_logos')): ?>

											<?php while(has_sub_field('all_charity_logos')): ?>
												<div class="partners_logo">
												<div class="size_protector">
												<a href="<?php the_sub_field('charity_link'); ?>">
												<img class="author_image" src="<?php the_sub_field('charity_logos'); ?>">
												</a>
												</div>
												</div>

											<?php endwhile; ?>

										<?php endif; ?>
											</div><!-- /row page content row -->
					</div><!-- / partners row -->
				</div><!-- /#tab-2 -->			
				<!--
					// Get Social
				-->
				<div id="tab-3" class="tab-wrap" data-tab="get-social">
					<?php echo get_field('get_social_copy'); ?>
				</div><!-- /#tab-3 -->	

				<!--
					// FAQ
				-->
				<div id="tab-4" class="tab-wrap" data-tab="faq">
					<?php echo get_field('faq_copy'); ?>
				</div><!-- /#tab-4 -->			
			
			</div><!-- /.partner row -->
		</article>

		


	</div><!-- /.row -->

<?php get_footer(); ?>
<!-- MODALS -->
			<div id="level-1" class="reveal-modal">
				<a class="close-reveal-modal">×</a>
				<img class="product-img" src="<?php echo get_template_directory_uri(); ?>/img/products/level-1.jpg" />
			</div>
			<div id="level-2" class="reveal-modal">
				<img class="product-img" src="<?php echo get_template_directory_uri(); ?>/img/products/level-2.jpg" />
			</div>
			<div id="level-3" class="reveal-modal">
				<img class="product-img" src="<?php echo get_template_directory_uri(); ?>/img/products/level-3.jpg" />
			</div>
			<div id="level-4" class="reveal-modal">
				<img class="product-img" src="<?php echo get_template_directory_uri(); ?>/img/products/level-4.jpg" />
			</div>
			<div id="level-5" class="reveal-modal">
				<img class="product-img" src="<?php echo get_template_directory_uri(); ?>/img/products/level-5.jpg" />
			</div>
			<div id="level-6" class="reveal-modal">
				<img class="product-img" src="<?php echo get_template_directory_uri(); ?>/img/products/level-6.jpg" />
			</div>
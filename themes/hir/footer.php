<?php 
    // Site Settings
    $options = get_option('settings_options');
?>
        <div class="floating-stories bottom-screen">
            <div class="floating-stories-number">
                <?php 
                    $number = wp_count_posts('stories'); 
                    echo '<span class="number">' . $number->publish . '</span> Stories'; 
                ?>
            </div>
            <div class="floating-stories-link">
                <a href="<?php echo get_bloginfo('url'); ?>/share">
                    <span class="no-mobile">&gt;</span>Share Yours
                    <span class="no-mobile">&lt;</span>
                </a>
            </div>
        </div><!-- /#floating-stories -->
        
        <div id="footer-social-row" class="row">
            <div class="col-sm-10 col-sm-offset-1">
                <div class="row">
                    <div id="footer-social-left" class="col-sm-6">
                        <h4>Stay Connected</h4>
                    </div><!-- /#footer-social-left -->
                    <div id="footer-social-right" class="col-sm-6">
                        <nav id="footer-social-nav" itemscope itemtype="http://schema.org/SiteNavigationElement">
                            
                           <?php 
								global $frn_mobile;
								if(!$frn_mobile) $target='target="_blank"';
								else $target="";
                                
								if (is_page_template( array( 'page-event.php', 'page-events-landing.php', 'page-race-landing.php', 'page-new-race-landing.php', 'page-event-virtual.php' ) ) ) {

                                        echo '<ul>
                                <li>
                                    <a href="https://www.facebook.com/HeroesinRecovery" title="Facebook" '.$target.' itemprop="url">
                                        <span class="sr-only" itemprop="name">Facebook</span><i class="fa fa-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.youtube.com/user/heroesinrecovery" title="YouTube" '.$target.' itemprop="url">
                                        <span class="sr-only" itemprop="name">YouTube</span><i class="fa fa-youtube"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://instagram.com/heroes6k/" title="instagram" '.$target.' itemprop="url">
                                        <span class="sr-only" itemprop="name">instagram</span><i class="fa fa-instagram"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://twitter.com/Heroes6K/" title="Twitter" '.$target.' itemprop="url">
                                        <span class="sr-only" itemprop="name">Twitter</span><i class="fa fa-twitter"></i>
                                    </a>
                                </li>
                            </ul>';
                                }
                                else 
                                    echo'<ul>
                                <li>
                                    <a href="https://www.facebook.com/HeroesinRecovery" title="Facebook" '.$target.' itemprop="url">
                                        <span class="sr-only" itemprop="name">Facebook</span><i class="fa fa-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.youtube.com/user/heroesinrecovery" title="YouTube" '.$target.' itemprop="url">
                                        <span class="sr-only" itemprop="name">YouTube</span><i class="fa fa-youtube"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://instagram.com/heroesinrecovery/" title="instagram" '.$target.' itemprop="url">
                                        <span class="sr-only" itemprop="name">instagram</span><i class="fa fa-instagram"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://twitter.com/HeroesNRecovery" title="Twitter" '.$target.' itemprop="url">
                                        <span class="sr-only" itemprop="name">Twitter</span><i class="fa fa-twitter"></i>
                                    </a>
                                </li>
                            </ul>'
                                
                           ?>

                            
                        </nav><!-- /#login-social-nav --> 
                    </div><!-- /#footer-social-right -->
                </div><!-- /.row -->
            </div><!-- /.col-sm-10 -->
        </div><!-- /.row -->      
        
        <div id="footer-row" class="row">                    
            <footer id="footer" class="col-sm-10 col-sm-offset-1" itemscope itemtype="http://schema.org/WPFooter">
                <div class="row">
                    <div id="footer-left" class="col-sm-3">
                        <h4><a href="<?php echo get_bloginfo('url'); ?>/about/#news">Latest News</h4>
                        <article id="footer-latest-news-wrap" itemscope itemtype="http://schema.org/Article">
                            <?php

                            $args = array(
                                'post_type'              => 'news',
                                'post_status'            => 'publish',
                                'posts_per_page'         => 1,
                                'order'                  => 'DESC',
                                'paged'                  => $paged,
                            );

                            // The Query
                            $query = new WP_Query( $args );

                            // The Loop
                            if ( $query->have_posts() ) {
                                
                                while ( $query->have_posts() ) {
                                    $query->the_post();
                             ?>
                            <h5 itemprop="name"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h5>
                            <?php the_excerpt(); ?>
                            <a href="<?php the_permalink(); ?>" title="Read More" itemprop="url" class="read-more">Read More</a>
                            <?php
                                }
                            } else {
                                echo 'No News Found';
                            }        

                            // Restore original Post Data
                            wp_reset_postdata(); 
                            ?>                            
                        </article><!-- /#footer-latest-news-wrap -->
                    </div>
                    <div id="footer-right" class="col-sm-9">
                        <div class="row">
                            <div id="footer-middle" class="col-sm-5" itemscope itemtype="http://schema.org/Organization">
                                <p>
                                    <strong itemprop="name">Heroes In Recovery</strong><br>
                                    <span itemprop="streetAddress">
                                        <?php echo nl2br($options['address_input']); ?>
                                    </span><br>
                                    <span itemprop="addressLocality"><?php echo $options['city_input']; ?></span>,
                                    <span itemprop="addressRegion"><?php echo $options['state_input']; ?></span>
                                    <span itemprop="postalCode"><?php echo $options['zip_input']; ?></span>
                                </p>
                                <p>
                                    <strong>T</strong> <span itemprop="telephone"><?php echo do_shortcode('[frn_phone number="'.$options['phone_number_input'].'" ga_phone_location="Phone Clicks in Footer"]'); ?></span><!--<a href="tel:<?php //echo $options['phone_number_input']; ?>" itemprop="telephone"><?php //echo $options['phone_number_input']; ?></a>--><br>
                                    <strong>E</strong> <a href="mailto:<?php echo $options['email_input']; ?>?subject=[Via%20HeroesInRecovery.com]%20I%27d%20Like%20Some%20Information" itemprop="email"><?php echo $options['email_input']; ?></a>
                                </p>

                            </div><!-- /#footer-middle -->
                            <div id="footer-list" class="col-sm-7">
                                                 
                            
                                <div class="footer__logo--frn">
                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/frn-logo-white.svg" class="img-responsive" />
                                </div>
                                
                                &copy;<?php echo date( 'Y' ); ?> Heroes In Recovery. All Rights Reserved. <a href="http://www.proofbranding.com" title="Finely Crafted By Proof" <?=$target;?> >Finely Crafted By Proof.</a>
                            

                            </div><!-- /#footer-right -->
                        </div><!-- /.row -->
                        <div class="row">
                            <nav id="footer-middle-nav" class="col-sm-5" itemscope itemtype="http://schema.org/SiteNavigationElement">
                                <?php
                                    // Display footer menu
                                    if( has_nav_menu( 'footer-menu' ) ) {
                                        wp_nav_menu( array( 'theme_location' => 'footer-menu' ) );
                                    }
                                ?>
                            </nav><!-- /#footer-middle-nav -->                             
                        </div><!-- /.row -->
                    </div><!-- /#footer-right -->



                </div><!-- /.row -->
            </footer>
        </div><!-- /.row -->
    </div><!-- /.container -->
<?php wp_footer(); ?>

<!-- Google Code for Remarketing Tag -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1008062797;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1008062797/?guid=ON&amp;script=0"/>
</div>
</noscript>
</body>
</html>